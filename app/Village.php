<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    //
     protected $fillable = [
        'statname','villename','villename_en',
    ];
}
