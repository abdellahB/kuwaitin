<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catcompany extends Model
{
    //
    //
    //use Notifiable;
    protected $table = 'catcompanies';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'catname','catflag','status','catcolor','title','catname_en',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    public function company(){
        return $this->belongsTo('App\Company');
    }
}
