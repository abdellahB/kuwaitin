<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classelectro extends Model
{
    //
    protected $fillable=[
        "upelc_video",
        "upelc_title",
        "upelc_desc" ,
        "upelc_price" ,
        "upelc_fstphone",
        "upelc_secphone" ,
        "upelc_brand" ,
        "upelc_gov" ,
        "upelc_city",
        "upelc_adress" ,
        "upelc_model",
        "upelc_country",
        "upelc_year" ,
        "upelc_status" ,
        "upelc_guar" ,
        "upelccategory",
        "upelc_acc",
        "user_id" 
    ];
}
