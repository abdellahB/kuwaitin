<?php

namespace App\Providers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        
        Schema::defaultStringLength(191);

        view()->composer(['layouts.app','includes.sliderads','includes.sidebar','includes.feature','includes.body','companyprofile'], function ($view) {
            $view->with('homeads',\App\Ads::get());
            $view->with('setup',\App\Setup::first());

            $view->with('featureclasscars',DB::table('users')->join('classcars','users.id', '=', 'classcars.user_id')->where('classcars.statys',2)->get());
            $view->with('featureclassbuilds',DB::table('users')->join('classbuilds','users.id', '=', 'classbuilds.user_id')->where('classbuilds.status',2)->get());
            $view->with('featureclasselectros',DB::table('users')->join('classelectros','users.id', '=', 'classelectros.user_id')->where('classelectros.status',2)->get());
            $view->with('featureclassjobs',DB::table('users')->join('classjobs','users.id', '=', 'classjobs.user_id')->where('classjobs.status',2)->get());
        });

        view()->composer(['control.layouts.navbar'], function ($view) {

            //count class cars ..
            $view->with('classcars1',DB::table('users')->join('classcars','users.id', '=', 'classcars.user_id')
                ->join('cars','cars.id','=','classcars.upcar_category')->where('classcars.statys',0)->count());
           
            $view->with('classcars2',DB::table('users')->join('classcars','users.id', '=', 'classcars.user_id')
                ->join('cars','cars.id','=','classcars.upcar_category')->where('classcars.statys',1)->count());

             $view->with('classcars3',DB::table('users')->join('classcars','users.id', '=', 'classcars.user_id')
                ->join('cars','cars.id','=','classcars.upcar_category')->where('classcars.statys',2)->count());
            
             $view->with('classcars4',DB::table('users')->join('classcars','users.id', '=', 'classcars.user_id')
                ->join('cars','cars.id','=','classcars.upcar_category')->where('classcars.statys',-3)->count());
        });
            
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
