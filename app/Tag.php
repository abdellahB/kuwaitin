<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //

    protected $fillable = [
        'tagname','tagname_en',
    ];


    public function company(){
        return $this->belongsToMany('App\Company');
    }
}
