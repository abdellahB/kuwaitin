<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    //

    protected $fillable = [
        'catname','catname_en','title','catflag','catcolor','catphoto','cathome'
    ];


    public function getCatsCars(){
        return $this->belongsToMany('App\Classcar');
    }
    
}
