<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    //
    protected $fillable = [
        'catname','catname_en','title','catflag','catcolor','catphoto','cathome'
    ];
}
