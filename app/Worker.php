<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    
    protected $fillable = [
        'empname','empname_en','emprole','emprole_en','empphoto','company_id',
    ];

    public function companie(){
        return $this->belongsTo('App\Company','company_id');
    }

   
}
