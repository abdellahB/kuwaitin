<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classbuild extends Model
{
    //
    protected $fillable = [
        "upbuild_video","upbuild_title","upbuild_desc","upbuild_price","upbuild_fstphon",
        "upbuild_secphone","upbuild_floor","upbuild_gov","upbuild_city","upbuild_adress",
        "upbuild_size" ,"upbuild_room","upbuild_bath" ,"upbuild_status" ,"upbuild_kind",
        "upbuild_pos", "bluidcategory" ,"upbuild_cars","upbuild_upstairs","user_id","status"
    ];
}
