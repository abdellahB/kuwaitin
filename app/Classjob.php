<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classjob extends Model
{
    //
    protected $fillable=[
        "upjob_title",
        "upjob_desc",
        "upjob_salary" ,
        "upjob_fstphone",
        "upjob_secphone" ,
        "upjob_comp" ,
        "upjob_gov" ,
        "upjob_city" ,
        "upjob_adress",
        "upjob_exp" ,
        "upjob_Wholiday" ,
        "upjob_Aholiday" ,
        "upjob_Jnum" ,
        "upjob_Jkind" ,
        "upjob_gen" ,
        "upjob_nation" ,
        "upjobcategory" ,
        "upjob_age" ,
        "upjob_edu" ,
        "upjob_spec" ,
        "upjob_fromTime",
        "upjob_toTime",
        "upbuild-pos" ,
        "user_id" ,
        "upjob_video"
    ];
}
