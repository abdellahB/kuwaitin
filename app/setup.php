<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setup extends Model
{
    //

    protected $fillable = [
        'sitename','sitename_en','description','description_en','sitelogo','site_facebook','site_instagram',
        'site_twitter','site_youtube','site_email','site_phone',
    ];
}
