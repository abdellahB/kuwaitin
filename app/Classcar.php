<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classcar extends Model
{
    //
    protected $fillable = [
        "upcar_video","upcar_title","upcar_desc" , "upcar_price", "upcar_fstphone" , "upcar_secphone",
        "car_makes", "upcar_size" , "upcar_dist" ,"upcar_gov", "upcar_city" , "upcar_adress" , "car_models",
        "car_years" , "upcar_outcolor", "upcar_incolor" ,  "upcar_gear", "upcar_gear_type" ,
        "cq_doors" , "upcar_fuel" , "upcar_wheel" , "upcar_category" , "upcar_check" ,
        "upcar_size" ,"upcar_guar" , "upcar_end" ,"upcar_spec" , "upcar_status" ,
        "camal" , "camal1" , "camal2" ,  "camal3" , "camal4" , "camal5" ,
        "camal6" ,  "camal7",   "camal8" ,  "camal9" ,  "camal10" ,
        "camal11" ,  "camal12" , "camal13" ,  "camal14", "camal15" ,
        "camal16" ,"camal17" , "camal18","user_id"
    ];


    public function getuser(){
        return $this->belongsTo('App\User','user_id');
    }

    public function getCategory(){
        return $this->belongsTo('App\Car','upcar_category');
    }

    public function ImageCars(){
        return $this->hasMany('App\Carimage');
    }


}
