<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carimage extends Model
{
    //


    protected $fillable = [
        'classcar_id','upcar_img'
    ];


    public function getImageCars(){
        return $this->belongsTo('App\Classcar');
    }
}
