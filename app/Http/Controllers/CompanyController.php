<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Company;
class CompanyController extends Controller
{
    //
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function employer(){
        return view('employer.create');
    }
    public function compprofile(){
        $user = Auth::user();
        $companies = Company::join('users', 'users.id', '=', 'companies.user_id')
            ->join ('workers','workers.company_id','=','companies.id')
            ->select('*')->where('companies.user_id','=',$user->id)->first();
        return view('compProfile',compact('companies'));
    }
    public function editemployer(){
        return view('employer.edit');
    }
}
