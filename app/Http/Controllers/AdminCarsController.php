<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;

class AdminCarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
      $this->middleware('admin');
    }
    public function index()
    {
        //
        $cats = Car::where('status',0)->get();
        return view('control.category.cars.index',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('control.category.cars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
       if($file = $request->file('catphoto')){
            $data['catphoto'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img');
            $file->move($destinationPath, $data['catphoto']);
       }
       $title = str_random(5).'-'.str_slug($request->input('catname_en'), "-");
       $data['title']= $title;
       Car::create($data);
       return back()->with('success','تمت اضافة القسم بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cats = Car::findOrfail($id);
        return view('control.category.cars.edit',compact('cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $cats = Car::findOrfail($id);
        $data = $request->all();
       if($file = $request->file('catphoto')){
            $data['catphoto'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img');
            $file->move($destinationPath, $data['catphoto']);
       }
       $title = str_random(5).'-'.str_slug($request->input('catname_en'), "-");
       $data['title']= $title;
       $cats->fill($data)->save();
       return back()->with('success','تم تحديث القسم بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
