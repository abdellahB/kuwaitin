<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Company;
use App\Worker;
class AdminEmployerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
      $this->middleware('admin');
    }
    public function index()
    {
        //
        $workers  = Worker::where('status',0)->get();
        return view('control.employer.index',compact('workers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $company = Company::where('status',0)->pluck('compname','id');
        return view('control.employer.create',compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $validator = Validator::make($data, [
            'empname' => 'required',
            'emprole' => 'required',
            'empname_en' => 'required',
            'emprole_en' => 'required',
            'company_id' => 'required',
            'empphoto' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with('error',$validator);
        }

        $file = $request->file('empphoto');
        $data['empphoto'] = time().'-'.$file->getClientOriginalName();
        $destinationPath = public_path('/assets/img');
        $file->move($destinationPath, $data['empphoto']);

        $save = Worker::create($data);

        return back()->with('success','تمت اضافة الموظف');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $workers  = Worker::where('company_id',$id)->get();
        return view('control.employer.show',compact('workers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $company = Company::where('status',0)->pluck('compname','id');
        $work = Worker::findOrFail($id);
        return view('control.employer.edit',compact('work','company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $work = Worker::findOrFail($id);
        $data = $request->all();
        $validator = Validator::make($data, [
            'empname' => 'required',
            'emprole' => 'required',
            'empname_en' => 'required',
            'emprole_en' => 'required',
            'company_id' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('error',$validator);
        }

        if($file = $request->file('empphoto')){
            $data['empphoto'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img');
            $file->move($destinationPath, $data['empphoto']);
        }
        $work->fill($data)->save();

        return back()->with('success','تم تحديث ملف الموظف');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
