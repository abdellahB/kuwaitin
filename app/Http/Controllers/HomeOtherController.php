<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Scrap;
use App\House;
use App\Lance;
use App\Other;
use Auth;
class HomeOtherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo "hhhh";
    }

    public function scrap(){
        $scarps = Scrap::where('status',0)->get();
        return view('category.scrap',compact('scarps'));
    }
    public function house(){
        $houses = House::where('status',0)->get();
        return view('category.house',compact('houses'));
    } 
    public function other(){
        $others = Other::where('status',0)->get();
        return view('category.house',compact('others'));
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $ct = $request->upcargov;
        if($ct){
             $mycity = Village ::where('statname',$ct)->get();
             return view('includes.city', compact('mycity'));
        }
        $user = Auth::user();
        $scraps = Scrap::where('status',0)->get();
        $houses = House::where('status',0)->get();
        $lances = Lance::where('status',0)->get();
        $others = Other::where('status',0)->get();
        return view('ads.add-other',compact('user','scraps','houses','lances','others'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        dd($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         return view ('show.viewothers');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
