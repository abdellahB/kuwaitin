<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Page;
use App\Offer;
use App\Company;
use App\Catcompany;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $currentUser;

    public function __construct()
    {
       //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classcars = DB::table('classcars')->where('classcars.statys',1)->orWhere('classcars.statys',2)->get();
        $classbuilds = DB::table('users')->join('classbuilds','users.id', '=', 'classbuilds.user_id')->get();
        $classelectros = DB::table('users')->join('classelectros','users.id', '=', 'classelectros.user_id')->get();
        $classjobs = DB::table('users')->join('classjobs','users.id', '=', 'classjobs.user_id')->get();

        $classothers = DB::table('users')->join('classothers','users.id', '=', 'classothers.user_id')->get();
       // $classrunts = DB::table('users')->join('classrunts','users.id', '=', 'classrunts.user_id')->get();

       return view('home',compact('classcars','classbuilds','classelectros','classjobs'));
    }
    public function contact(){
        return view('pages.contact');
    }
    public function terms(){
         $terms = Page::get();
        return view('pages.terms',compact('terms'));
    }
    public function privacy(){
         $privacy = Page::get();
        return view('pages.privacy',compact('privacy'));
    }
    public function about(){
         $about = Page::get();
        return view('pages.about',compact('about'));
    }
    public function offers(){
        $offers=Offer::where('status',0)->get();
        return view('offers',compact('offers'));
    }

    //company
    public function company(){
        $cats=Catcompany::where('status',0)->get();
        return view('companylist',compact('cats'));
    }
    public function companytype($cat){
        $cats= Catcompany::where('title',$cat)->first();
        $companies = Company::join('users', 'users.id', '=', 'companies.user_id')
            ->join('catcompanies', 'catcompanies.id', '=', 'companies.catcompanie_id')
            ->select('*')->where('catcompanies.title','=',$cat)->get();
        return view('company',compact('companies','cats'));
    }
    public function companyprofile($slug){
        $companies = Company::where('compslug','=',$slug)->first();
        return view('companyprofile',compact('companies'));
    }


    
}
