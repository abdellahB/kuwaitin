<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Catcompany;
use App\User;
use App\Tag;
class AdminCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
      $this->middleware('admin');
    }
    public function index()
    {
        //
        $company=Company::get();
        return view('control.company.index',compact('company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tags=Tag::pluck('tagname','id');
        $cats = Catcompany::where('status',0)->pluck('catname','id');
        return view('control.company.create',compact('cats','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data=$request->all();
        //dd($data);
        $file = $request->file('complogo');
        $data['complogo'] = time().'-'.$file->getClientOriginalName();
        $destinationPath = public_path('/assets/img');
        $file->move($destinationPath, $data['complogo']);
        $data['password']=bcrypt($request->input('password'));
        $data['compslug'] = str_slug($request->input('compname_en'), "-");
        //creat account and send info login
        $data['phone']     = $request->input('comphone1');
        $data['username']  = $request->input('compname');
        $data['usertype']  = "company";
        $data['emailuser'] = $request->input('compemail');
        $data['photo']     = $data['complogo'];
        $data['sexe']      = "2";

       $user = User::create($data);
       $data['user_id']=$user->id;
        //save company
       $company = Company::create($data);
        
        //save tags
        $company->tags()->sync($request->tagname,false);
        //return messages success
        return back()->with('success','تمت اضافة الشركة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $company=Company::findOrfail($id);
        $cats = Catcompany::where('status',0)->pluck('catname','id');
        $tags=Tag::pluck('tagname','id');
       // $tags=$company->tags->pluck('tagname','id');
        return view('control.company.edit',compact('company','cats','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $company=Company::findOrfail($id);
        $data=$request->all();
        //dd($data);
        if($file = $request->file('complogo')){
            $data['complogo'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img');
            $file->move($destinationPath, $data['complogo']);
        }
        if($request->input('password')!=" "){
             $data['password']=bcrypt($request->input('password'));
        }
        

        $company->fill($data)->save();
        return back()->with('success','تم تحديث الملف');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
