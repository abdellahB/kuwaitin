<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;
class AdsController extends Controller
{
    //
    public function __construct()
    {
      //$this->middleware('auth',['except' => ['addcar', 'addbuild','addelectronic','addjobs','addother','car4rent']]);
    }
    public function index(){
        return view('addads');
    }
    //must be secure
    public function profile(){
        $user = Auth::user();
        $classcars =  DB::table('classcars')->where([['classcars.user_id','=',Auth::user()->id],['classcars.statys','=',1]])->orWhere([['classcars.user_id','=',Auth::user()->id],['classcars.statys','=',2]])->get();
       return view('profile',compact('user','classcars'));
    }
    public function updateprofile(Request $request){
      dd($request);
    }
    public function myads(){
        $user = Auth::user();
       $classcars =  DB::table('classcars')->where([['classcars.user_id','=',Auth::user()->id],['classcars.statys','=',1]])->orWhere([['classcars.user_id','=',Auth::user()->id],['classcars.statys','=',2]])->get();
        return view('myads',compact('user','classcars'));
    }
}
