<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;
class AdminOffersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
      $this->middleware('admin');
    }
    public function index()
    {
        //
        $offers=Offer::get();
        return view('control.offers.index',compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('control.offers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'offername'   => 'required',
            'offerurl' => 'required',
            'offerenddate'   => 'required',
            'offerimage' => 'required|mimes:jpeg,bmp,png,jpg'
        ]);

        $data=$request->all();
        $file = $request->file('offerimage');
        $data['offerimage'] = time().'-'.$file->getClientOriginalName();
        $destinationPath = public_path('/assets/img');
        $file->move($destinationPath, $data['offerimage']);
        
        Offer::create($data);
        return back()->with('success','تم نشر العرض بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $offers=Offer::findOrfail($id);
        return view('control.offers.edit',compact('offers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $offers=Offer::findOrfail($id);
        $data=$request->all();
        if ($file = $request->file('offerimage')) {
            $data['offerimage'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img');
            $file->move($destinationPath, $data['offerimage']);
        }
        $offers->fill($data)->save();
        return back()->with('success','تم تحديث العرض بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
