<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Classcar;
use App\User;
use App\Village;
use App\Car;
class AdminAdsCarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
      $this->middleware('admin');
    }
    //ads cars 
    public function newadsCars(){
        $classcars =Classcar::where('classcars.statys',0)->get();
        return view('control.adsStep.cars.new',compact('classcars'));
    }
    public function publishedCars(){
       $classcars =Classcar::where('classcars.statys',1)->get();
        return view('control.adsStep.cars.published',compact('classcars'));
    }
    public function featureCars(){
       $classcars =Classcar::where('classcars.statys',2)->get();
        return view('control.adsStep.cars.feature',compact('classcars'));
    }
    public function trashedCars(){
        $classcars =Classcar::where('classcars.statys',-3)->get();
        return view('control.adsStep.cars.trashed',compact('classcars'));
    }
    
    public function create(Request $request)
    {
        $ct = $request->upcargov;
        if($ct){
             $mycity = Village ::where('statname',$ct)->get();
             return view('includes.city', compact('mycity'));
        }

        $cats = Car::where('status',0)->get();
        $users = User::where('status',0)->get();

        return view('control.adsStep.cars.create',compact('cats','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->All();
        if($file = $request->file('upcar_video')){
            $data['upcar_video'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img/video');
            $file->move($destinationPath, $data['upcar_video']);
        }
       Classcar::create($data);
       return back()->with('success','تم نشر الاعلان بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $carads = Classcar::findorfail($id);
        return view('control.adsStep.cars.edit',compact('carads'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAjax(Request $request){
        $id = (int) $request->idads;
        $statusAds =  $request->status;
        if($id && $statusAds){
            if($statusAds == "publierads"){

                Classcar::where('id', $id)->update(['statys' => 1]);
                $data['status'] ="success";

            }elseif($statusAds == "featureads"){

                Classcar::where('id', $id)->update(['statys' => 2]);
                $data['status'] ="success";

            }elseif($statusAds == "archiveads"){

                Classcar::where('id', $id)->update(['statys' => -3]);
                $data['status'] ="success";

            }elseif($statusAds == "deleteads"){
            
                $classcar = Classcar::where('id', $id)->first();

                Classcar::where('id', $id)->update(['statys' => 5]);

                User::where('id', $classcar->user_id)->update(['status' => 5]);

                $data['status'] ="success";

            }elseif($statusAds == "retoreAds"){
                Classcar::where('id', $id)->update(['statys' => 0]);
                $data['status'] ="success";
            }
           
            return response()->json($data);
        }
       
         
    }
}
