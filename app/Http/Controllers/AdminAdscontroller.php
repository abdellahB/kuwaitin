<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ads;

class AdminAdscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
      $this->middleware('admin');
    }
    public function index()
    {
        //
        $homeads = Ads::get();
        return view('control.ads.index',compact('homeads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('control.ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data=$request->all();
        if ($file = $request->file('image')) {
            $data['image'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img');
            $file->move($destinationPath, $data['image']);
        }
        Ads::create($data);
        return back()->with('success','تمت اضافة الاعلان بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         //return view('control.ads.edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $ads=Ads::findOrfail($id);
        return view('control.ads.edit',compact('ads'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $ads=Ads::findOrfail($id);
        $data=$request->all();
        if ($file = $request->file('image')) {
            $data['image'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img');
            $file->move($destinationPath, $data['image']);
        }
        $ads->fill($data)->save();
        return back()->with('success','تم تحديث الاعلان بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //$ads=Ads::findOrfail($id);
        dd($id);
        /*$update = Ads::where('id', $ads)->update(['status' => 1]);
        return redirect('control/ads/index');*/
    }
}
