<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
class AdminRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $admins = Admin::get();
        return view('control.role.index',compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('control.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $this->validate($request, [
            'username' => 'required',
            'emailadmin' => 'required|unique:admins',
            'password'  => 'required|min:6',
            'role'  => 'required',
        ]);
       
        $file = $request->file('photo');
        $data['photo'] = time().'-'.$file->getClientOriginalName();
        $destinationPath = public_path('/assets/img');
        $file->move($destinationPath, $data['photo']);
        $data['password'] = bcrypt($request->input('password'));

       Admin::create($data);

       return back()->with('success','تم اضافة مستخدم جديد ');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $admins = Admin::findOrfail($id);
        return view('control.role.edit',compact('admins'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $admin = Admin::findorfail($id);
        $data = $request->all();
        
        if($file = $request->file('photo')){
            $data['photo'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img');
            $file->move($destinationPath, $data['photo']);
        }
        if($request->input('password') !=" "){
            $data['password'] = bcrypt($request->input('password'));
        }

         $admin->update($data);

       return back()->with('success','تم تحديث الحساب ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request){
        $id = (int) $request->idadmin;
        $action =  $request->admins;
        if($id && $action){
            if($action =="removeadmins"){
                $admin = Admin::find($id)->update(['status'=>1]);
            }else{
                $admin = Admin::find($id)->update(['status'=>0]);
            }
            $data['status'] ="success";
            return response()->json($data);
        }
       
         
    }
    public function destroy($id)
    {
        //
        
    }
}
