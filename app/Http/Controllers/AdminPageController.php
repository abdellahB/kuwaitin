<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Village;
use App\Setup;
class AdminPageController extends Controller
{
    
    public function __construct()
    {
         $this->middleware('admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showprivacy()
    {
        $privacy = Page::get();
        return view('control.pages.privacy',compact('privacy'));
    }

    public function saveprivacy(Request $request){
        $id=$request->input('update');
        $data=$request->input('privacy');
        Page::where('id', $id)
            ->update(['privacy' => $data]);

        return redirect('/control/page/privacy');
    }

    public function showterms(){
         $terms = Page::get();
         return view('control.pages.terms',compact('terms'));
    }
    public function saveterms(Request $request){
        $id=$request->input('update');
        $data=$request->input('terms');
        Page::where('id', $id)
            ->update(['terms' => $data]);

        return redirect('/control/page/terms');
    }

    public function showabout(){
         $about = Page::get();
         return view('control.pages.about',compact('about'));
    }
    public function saveabout(Request $request){
        $id=$request->input('update');
        $data=$request->input('about');
        Page::where('id', $id)
            ->update(['about' => $data]);

        return redirect('/control/page/about');
    }

    //city
    public function showcity(){
        $villages = Village::where('status',0)->get();
        return view('control.city.index',compact('villages'));
    }
    public function savecity(Request $request){
        $data = $request->all();
        Village::create($data);
        return redirect('/control/city');

    }

    //settings
    public function showsettings(){
         $settings = Setup::first();
         return view('control.settings.create',compact('settings'));
    }
    public function savesettings(Request $request){
        $id   = $request->input('settingsid');
        $data = request()->except(['_token','settingsid']);

        if($file = $request->file('sitelogo')) {
            $data['sitelogo'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img');
            $file->move($destinationPath, $data['sitelogo']);
        }
        Setup::where('id', $id)
            ->update($data);

        return back()->with('success','تم تحديث المعلومات');
    }

}
