<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use Auth;
use App\Village;
use App\Classcar;
use App\Carimage;
use Image; 
class HomeCarscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::where('status',0)->get();
        return view('category.cars',compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $ct = $request->upcargov;
        if($ct){
             $mycity = Village ::where('statname',$ct)->get();
             return view('includes.city', compact('mycity'));
        }
        $user = Auth::user();
        $cats = Car::where('status',0)->get();
        return view('ads.add-car',compact('user','cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->All();
        if($file = $request->file('upcar_video')){
            $data['upcar_video'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img/video');
            $file->move($destinationPath, $data['upcar_video']);
        }
        $data['user_id'] = Auth::user()->id;

        $classcar = Classcar::create($data);

        
        $images = array();
        $images[] = $request->file('upcar_img');


        foreach($images as $image){

            $name = time().'-'.$image->getClientOriginalName();
            $destinationPath = public_path('/assets/img');
            $image->move($destinationPath, $name);

            Carimage::insert([
                'classcar_id' =>  $classcar->id,
                'upcar_img'   => $name,
            ]);
        }
        

        return back()->with('success','تم اضافة الاعلان بنجاح و في انتظار المراجعة من طرف الادارة');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $classcar = Classcar::findorfail($id);
        return view ('show.viewcars',compact("classcar"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
