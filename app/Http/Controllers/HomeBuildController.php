<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input as Input;
use Illuminate\Http\Request;
use Auth;
use App\Build;
use App\Classbuild;
use App\Village;
class HomeBuildController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $builds = Build::where('status',0)->get();
         return view('category.build',compact('builds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $ct = $request->upcargov;
        if($ct){
             $mycity = Village ::where('statname',$ct)->get();
             return view('includes.city', compact('mycity'));
        }
        $user = Auth::user();
         $cats = Build::where('status',0)->get();
        return view('ads.add-build',compact('user','cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        //
        $data   = $request->all();
        if($file = $request->file('upbuild_video')){
            $data['upbuild_video'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/upload');
            $file->move($destinationPath, $data['upbuild_video']);
       }
       /*$title = str_random(5).'-'.str_slug($request->input('catname_en'), "-");
       $data['title']= $title;*/
       $data['status'] = 0;
       Classbuild::create($data);
       return back()->with('success','تمت اضافة القسم بنجاح');

    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         return view ('show.viewbuild');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
