<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Electro;
use Auth;
use App\Village;
use App\Classelectro;
class HomeElectroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $electros = Electro::where('status',0)->get();
        return view('category.electro',compact('electros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $ct = $request->upcargov;
        if($ct){
             $mycity = Village ::where('statname',$ct)->get();
             return view('includes.city', compact('mycity'));
        }
        $user = Auth::user();
        $cats = Electro::where('status',0)->get();
        return view('ads.add-el',compact('cats','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->All();
        if($file = $request->file('upelc_video')){
            $data['upelc_video'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img/video');
            $file->move($destinationPath, $data['upelc-video']);
        }
        $data['user_id'] = Auth::user()->id;
       Classelectro::create($data);
       return back()->with('success','تمت اضافة الاعلان بنجاح و في انتظار مراجعة الادارة');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         return view ('show.viewele');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
