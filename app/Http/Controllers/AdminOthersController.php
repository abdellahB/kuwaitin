<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Other;
class AdminOthersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cats = Other::where('status',0)->get();
        return view('control.category.others.index',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('control.category.others.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
       if($file = $request->file('catphoto')){
            $data['catphoto'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img');
            $file->move($destinationPath, $data['catphoto']);
       }
       $title = str_random(5).'-'.str_slug($request->input('catname_en'), "-");
       $data['title']= $title;
       Other::create($data);
       return back()->with('success','تمت اضافة القسم بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $cats = Other::findOrfail($id);
        return view('control.category.others.edit',compact('cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $cats = Other::findOrfail($id);
        $data = $request->all();
       if($file = $request->file('catphoto')){
            $data['catphoto'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img');
            $file->move($destinationPath, $data['catphoto']);
       }
       $title = str_random(5).'-'.str_slug($request->input('catname_en'), "-");
       $data['title']= $title;
       $cats->fill($data)->save();
       return back()->with('success','تم تحديث القسم بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
