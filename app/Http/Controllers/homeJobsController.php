<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use App\Village;
use App\Classjob;
use Auth;
class homeJobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $jobs = Job::where('status',0)->get();
        return view('category.jobs',compact('jobs'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $ct = $request->upcargov;
        if($ct){
             $mycity = Village ::where('statname',$ct)->get();
             return view('includes.city', compact('mycity'));
        }
        $user = Auth::user();
        $cats = Job::where('status',0)->get();
        return view('ads.add-job',compact('cats','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request->all());
        
        $data = $request->All();
        if($file = $request->file('upelc_video')){
            $data['upelc_video'] = time().'-'.$file->getClientOriginalName();
            $destinationPath = public_path('/assets/img/video');
            $file->move($destinationPath, $data['upelc-video']);
        }
        $data['user_id'] = Auth::user()->id;
        Classjob::create($data);
        return back()->with('success','تمت اضافة الاعلان بنجاح و في انتظار مراجعة الادارة');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         return view ('show.viewjobs');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         return view ('show.viewbuild');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
