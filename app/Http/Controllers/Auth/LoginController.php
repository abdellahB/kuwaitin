<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout', 'userLogout']]);

    }
    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'phone'   => 'required',
        'password' => 'required|min:6'
      ]);

      // Attempt to log the user in
      if(Auth::attempt(['phone' => $request->phone, 'password' => $request->password,'status'=>0], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect('/');
      }else{
        return back()->with('error','معلومات الدخول غير أو حسابك موقف ...');
      }

      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('phone', 'remember'));
    }
    public function userLogout()
    {
        Auth::guard('web')->logout();
        return redirect('/');
    }
}
