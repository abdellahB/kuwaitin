<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //

     protected $fillable = [
        'privacy','terms','about',
    ];

    //
    protected $hidden = [
        'remember_token',
    ];
}
