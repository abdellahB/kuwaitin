<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classified extends Model
{
    //

    public function cars(){
        return $this->belongstoMany('App/Classcar');
    }
    public function builds(){
        return $this->belongstoMany('App/Classbuild');
    }
    public function electros(){
        return $this->belongstoMany('App/Classelectro');
    }
    public function jobs(){
        return $this->belongstoMany('App/Classjob');
    }
    public function others(){
        return $this->belongstoMany('App/Classother');
    }
    public function runts(){
        return $this->belongstoMany('App/Classrunt');
    }
}
