<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $table = 'companies';

    protected $fillable = [
        'catcompanie_id','user_id','compname','compname_en','comptitle','comptitle_en','compworking',
        'compworking_en','complogo','compabout','compabout_en','compwebsite','comphone1','comphone2',
        'compemail','compfacebook','comptwitter','compyoutube','compinstagram','compgoogleplus','complace',
        'compslug',
    ];

    //
    public function cat(){
        return $this->belongsTo('App\Catcompany','catcompanie_id');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag');
    }

    public function workers(){
        return $this->hasMany('App\Worker');
    }

}
