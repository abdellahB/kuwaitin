const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');*/
mix.styles([
    'public/assets/css/okayNav.min.css',
    'public/assets/css/lightslider.min.css',
    'public/assets/css/semantic.min.css',
    'public/assets/css/lightgallery.min.css',
    'public/assets/css/default.css',
    'public/assets/css/default.time.css',
    'public/assets/css/default.date.css',
    'public/assets/css/main-css.css'
],  'public/css/all.css'); 


mix.scripts([
    'public/assets/js/jquery-1.12.3.min.js',
    'public/assets/js/bootstrap.min.js',
    'public/assets/js/semantic.min.js',
    'public/assets/js/jquery.smooth-scroll.js',
    'public/assets/js/lightslider.min.js',
    'public/assets/js/lightgallery.min.js',
    'public/assets/js/lg-video.min.js',
    'public/assets/js/picker.js',
    'public/assets/js/picker.time.js',
    'public/assets/js/picker.date.js',
    'public/assets/js/main-js.js',
    'public/assets/js/javascript.js'
], 'public/js/all.js');

//admin style and js
mix.styles([
    'public/assets/admin/css/application.min.css',
    'public/assets/admin/css/jquery-ui-1.10.3.custom.min.css',
    'public/assets/admin/css/selectize.css'
],  'public/assets/admin/css/admin.css'); 


mix.scripts([
    'public/assets/admin/js/vendor.min.js',
    'public/assets/admin/js/elephant.min.js',
    'public/assets/admin/js/application.min.js',
    'public/assets/admin/js/demo.min.js',
    'public/assets/admin/js/jquery-ui-1.10.3.custom.min.js',
    'public/assets/admin/js/selectize.min.js',
    'public/assets/admin/js/javascript.js'
], 'public/js/admin.js');