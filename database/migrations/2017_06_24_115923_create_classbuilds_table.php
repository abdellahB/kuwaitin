<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassbuildsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classbuilds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('upbuild_video')->nullable();
            $table->string('upbuild_title')->nullable();
            $table->string('upbuild_desc')->nullable();
            $table->float('upbuild_price')->nullable();
            $table->string('upbuild_fstphon')->nullable();
            $table->string('upbuild_secphone')->nullable();
            $table->string('upbuild_floor')->nullable();
            $table->string('upbuild_gov')->nullable();
            $table->string('upbuild_city')->nullable();
            $table->string('upbuild_adress')->nullable();
            $table->string('upbuild_size')->nullable();
            $table->string('upbuild_room')->nullable();
            $table->string('upbuild_bath')->nullable();
            $table->string('upbuild_status')->nullable();
            $table->string('upbuild_kind')->nullable();
            $table->string('upbuild_pos')->nullable();
            $table->integer('bluidcategory')->nullable();
            $table->integer('upbuild_cars')->nullable();
            $table->integer('upbuild_upstairs')->nullable();
            $table->string('desposit')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classbuilds');
    }
}
