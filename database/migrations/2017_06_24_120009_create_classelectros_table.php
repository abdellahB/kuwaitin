<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClasselectrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classelectros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('upelc_video')->nullable();
            $table->string('upelc_title')->nullable();
            $table->string('upelc_desc')->nullable();
            $table->float('upelc_price')->nullable();
            $table->string('upelc_fstphone')->nullable();
            $table->string('upelc_secphone')->nullable();
            $table->string('upelc_brand')->nullable();
            $table->string('upelc_gov')->nullable();
            $table->string('upelc_city')->nullable();
            $table->string('upelc_adress')->nullable();
            $table->string('upelc_model')->nullable();
            $table->string('upelc_country')->nullable();
            $table->string('upelc_year')->nullable();
            $table->string('upelc_status')->nullable();
            $table->string('upelc_guar')->nullable();
            $table->integer('upelccategory')->nullable();
            $table->string('upelc_acc')->nullable();
            $table->string('desposit')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classelectros');
    }
}
