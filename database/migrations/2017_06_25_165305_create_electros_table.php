<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('electros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('catname');
            $table->string('catname_en');
            $table->string('title')->unique();
            $table->string('catflag')->nullable();
            $table->string('catcolor');
            $table->string('catphoto')->nullable();
            $table->integer('cathome')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electros');
    }
}
