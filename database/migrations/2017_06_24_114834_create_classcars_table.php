<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClasscarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classcars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('upcar_video')->nullable();
            $table->string('upcar_title')->nullable();
            $table->string('upcar_desc')->nullable();
            $table->string('upcar_price')->nullable();
            $table->string('upcar_fstphone')->nullable();
            $table->string('upcar_secphone')->nullable();
            $table->string('car_makes')->nullable();
            $table->string('upcar_dist')->nullable();
            $table->string('upcar_gov')->nullable();
            $table->string('upcar_city')->nullable();
            $table->string('upcar_adress')->nullable();
            $table->string('car_models')->nullable();
            $table->string('car_years')->nullable();
            $table->string('upcar_outcolor')->nullable();
            $table->string('upcar_incolor')->nullable();
            $table->string('upcar_gear')->nullable();
            $table->string('upcar_gear_type')->nullable();
            $table->integer('cq_doors')->nullable();
            $table->string('upcar_fuel')->nullable();
            $table->string('upcar_wheel')->nullable();
            $table->string('upcar_category')->nullable();
            $table->string('upcar_check')->nullable();
            $table->string('upcar_size')->nullable();
            $table->string('upcar_guar')->nullable();
            $table->string('upcar_end')->nullable();
            $table->string('upcar_spec')->nullable();
            $table->string('upcar_status')->nullable();
            $table->string('camal')->nullable();
            $table->string('camal1')->nullable();
            $table->string('camal2')->nullable();
            $table->string('camal3')->nullable();
            $table->string('camal4')->nullable();
            $table->string('camal5')->nullable();
            $table->string('camal6')->nullable();
            $table->string('camal7')->nullable();
            $table->string('camal8')->nullable();
            $table->string('camal9')->nullable();
            $table->string('camal10')->nullable();
            $table->string('camal11')->nullable();
            $table->string('camal12')->nullable();
            $table->string('camal13')->nullable();
            $table->string('camal14')->nullable();
            $table->string('camal15')->nullable();
            $table->string('camal16')->nullable();
            $table->string('camal17')->nullable();
            $table->string('camal18')->nullable();
            $table->string('desposit')->nullable();
            $table->integer('statys')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classcars');
    }
}
