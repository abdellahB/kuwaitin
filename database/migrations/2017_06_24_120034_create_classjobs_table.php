<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassjobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classjobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('upjob_video')->nullable();
            $table->string('upjob_title')->nullable();
            $table->text('upjob_desc')->nullable();
            $table->float('upjob_salary')->nullable();
            $table->string('upjob_fstphone')->nullable();
            $table->string('upjob_secphone')->nullable();
            $table->string('upjob_comp')->nullable();
            $table->string('upjob_gov')->nullable();
            $table->string('upjob_city')->nullable();
            $table->string('upjob_adress')->nullable();
            $table->string('upjob_exp')->nullable();
            $table->string('upjob_Wholiday')->nullable();
            $table->string('upjob_Aholiday')->nullable();
            $table->string('upjob_Jnum')->nullable();
            $table->string('upjob_Jkind')->nullable();
            $table->string('upjob_gen')->nullable();
            $table->string('upjob_nation')->nullable();
            $table->integer('upjob_age')->nullable();
            $table->string('upjob_edu')->nullable();
            $table->string('upjob_spec')->nullable();
            $table->string('upjob_fromTime')->nullable();
            $table->string('upjob_toTime')->nullable();
            $table->string('upbuild-pos')->nullable();
            $table->integer('upjobcategory')->nullable();
            $table->string('desposit')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classjobs');
    }
}
