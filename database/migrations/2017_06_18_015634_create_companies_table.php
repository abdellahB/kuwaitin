<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('catcompanie_id')->unsigned();
            $table->foreign('catcompanie_id')->references('id')->on('catcompanies');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('compname');
            $table->string('complace');
            $table->string('compname_en')->nullable();
            $table->string('comptitle');
            $table->string('comptitle_en')->nullable();
            $table->string('compworking');
            $table->string('compworking_en')->nullable();
            $table->string('compslug');
            $table->string('compfeature')->default(1);
            $table->string('complogo');
            $table->string('compabout');
            $table->string('compabout_en')->nullable();
            $table->string('compwebsite');
            $table->string('comphone1');
            $table->string('comphone2');
            $table->string('compemail');
            $table->string('compfacebook');
            $table->string('comptwitter');
            $table->string('compyoutube');
            $table->string('compinstagram');
            $table->string('compgoogleplus');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
