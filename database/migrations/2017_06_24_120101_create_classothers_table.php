<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassothersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classothers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('upoth_video')->nullable();
            $table->string('upoth_title')->nullable();
            $table->integer('supcategory')->nullable();
            $table->integer('childcategory')->nullable();
            $table->string('upoth_desc')->nullable();
            $table->float('upoth_price')->nullable();
            $table->string('upoth_fstphone')->nullable();
            $table->string('upoth_secphone')->nullable();
            $table->string('upoth_gov')->nullable();
            $table->string('upoth_city')->nullable();
            $table->string('upoth_adress')->nullable();
            $table->string('desposit')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classothers');
    }
}
