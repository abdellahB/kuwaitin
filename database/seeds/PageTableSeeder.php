<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'privacy' => 'test test test',
            'terms' => 'test test test',
            'about'=> 'test test test',
        ]);
    }
}
