<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('setups')->insert([
            'sitename' =>"test",
            'sitename_en'=>"test",
            'description' =>"test",
            'description_en' =>"test",
            'sitelogo' =>"logo.png",
            'site_facebook' =>"test",
            'site_instagram' =>"test",
            'site_twitter' =>"test",
            'site_youtube' =>"test",
            'site_email' =>"test",
            'site_phone' =>"test",
        ]);
    }
}
