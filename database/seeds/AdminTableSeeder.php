<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'username' => 'abdellahB',
            'emailadmin' => 'starfilmy@gmail.com',
            'role'=> 'admin',
            'photo'=> '-',
            'password' => bcrypt('123456'),
        ]);
    }
}
