@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">المستخدمين</span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{url('control/role/create')}}" class="btn btn-success">مستخدم جديد</a>
            </br>
            <table id="demo-dynamic-tables-2" class="table table-middle">
                <thead>
                    <tr>
                    <th>
                        <label class="custom-control custom-control-primary custom-checkbox">
                        <input id="checkAll" class="custom-control-input" type="checkbox">
                        <span class="custom-control-indicator"></span>
                        </label>
                    </th>
                    <th>صورة</th>
                    <th>الاسم الكامل</th>
                    <th>الايميل</th>
                    <th>رتبة</th>
                    <th>تاريخ الاضافة</th>
                    <th>الحالة</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($admins as $offer)
                        <tr>
                            <td></td>
                            <td>
                                <img class="rounded" width="40" height="40" src="{{url('public/assets/img/')}}/{{$offer->photo}}" alt=""/>
                            </td>
                            <td>{{$offer->username}}</td>
                            <td>{{$offer->emailadmin}}</td>
                            <td>
                                @if($offer->role=="admin") 
                                  مدير عام
                                @elseif($offer->role=="miniadmin")
                                   مدير
                                @elseif($offer->role=="employer")
                                    موظف
                                @endif
                            </td>
                            <td>{{$offer->created_at}}</td>
                            <td>
                            @if($offer->status ==0 )
                                <label class="label label-success label-pill">نشيط</label>
                            @else
                                <label class="label label-primary label-pill">تم ايقافه</label>
                            @endif
                            </td>
                            <td>
                                <div class="btn-group pull-right dropdown">
                                <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                    <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('role.edit',$offer->id)}}"  class="btn-link goldorders" >تعديل الحساب</a></li>
                                    @if($offer->status ==0 )
                                    <li><a href="#"  id="{{$offer->id}}" class="btn-link removeadmins" >ايقاف الموظف</a></li>
                                   @else
                                    <li><a href="#"  id="{{$offer->id}}" class="btn-link activeadmins" >تفعيل الحساب</a></li>
                                    @endif
                                </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
