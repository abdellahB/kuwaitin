@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">اضافة مستخدم جديد</span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{URL::to('control/role')}}" class="btn btn-success pull-right">كل المستخدمين</a>
            </br>
             @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            {!! Form::open(array('url' => 'control/role', 'class' => 'form','data-toggle' => 'validator','files' => true)) !!}
                <div class="form-group">
                    {!! Form::label('اسم المستخدم ') !!}
                    {!! Form::text('username', null, 
                        array('required', 
                            'class'=>'form-control', 
                            'placeholder'=>'اسم المستخدم')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('البريد الالكتروني ') !!}
                    {!! Form::email('emailadmin', null, 
                        array('required', 
                            'class'=>'form-control', 
                            'placeholder'=>'البريد الالكتروني')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('رتبة المستخدم') !!}
                    {!! Form::select('role', ['admin' => 'مدير عام', 'miniadmin' => 'مدير','employer'=>'موظف'],1, ['required','class'=>'form-control']); !!}
                </div>
                <div class="form-group">
                    {!! Form::label('كلمة المرور ') !!}
                    {!! Form::password('password', 
                        array('required', 
                            'class'=>'form-control', 
                            'placeholder'=>'كلمة المرور')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('صورة المستخدم') !!}
                    {!! Form::file('photo', 
                        array('required','class'=>'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('اضافة المستخدم', 
                    array('class'=>'btn btn-primary btn-block')) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection