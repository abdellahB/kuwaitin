@extends('control.layouts.admin')
@section('content')
<div class="login">
    <div class="login-body">
        <a class="login-brand" href="index.html">
          <!--<img class="img-responsive" src="img/logo.svg" alt="Elephant Theme">-->
        </a>
        <h3 class="login-heading">صفحة تسجيل الدخول</h3>
        <div class="login-form">
          <form data-toggle="md-validator" method="POST" action="{{route('control.auth.login')}}">
            <div class="md-form-group md-label-floating">
              <input class="md-form-control useremail" type="email" name="emailadmin" spellcheck="false" autocomplete="off" data-msg-required="يرجي إدخال بريد الكتروني صالح" required>
              <label class="md-control-label">البريد الالكتروني </label>
            </div>
            {{ csrf_field() }}
            <div class="md-form-group md-label-floating userpasswords">
              <input class="md-form-control userpassword" type="password" name="password" minlength="6" data-msg-minlength="Password must be 6 characters or more." data-msg-required="من فضلك ادخل كلمة المرور" required>
              <label class="md-control-label">كلمة المرور</label>
            </div>
            <div class="md-form-group md-custom-controls">
              <label class="custom-control custom-control-primary custom-checkbox">
                <input class="custom-control-input" type="checkbox" checked>
                <span class="custom-control-indicator"></span>
                <span class="custom-control-label">تدكرني على الجهاز</span>
              </label>
              <span aria-hidden="true"> · </span>
              <a href="#">نسيت كلمة المرور ؟</a>
            </div>
            <button class="btn btn-primary btn-block submit">تسجيل الدخول</button>
          </form>
        </div>
    </div>
</div>
@endsection
