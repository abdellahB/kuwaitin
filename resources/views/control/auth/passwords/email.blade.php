@extends('layouts.app')
@section('content')
<div class="login">
    <div class="login-body">
        <a class="login-brand" href="{{url('/control')}}"></a>
        <div class="login-form">
          <form data-toggle="md-validator" action="{{ route('control.password.email') }}" method="POST">
            <div class="md-form-group md-label-floating">
              <input class="md-form-control" type="email" name="email" spellcheck="false" autocomplete="off" data-msg-required="يرجي ادخال بريد الكتروني صحيح" required>
              <label class="md-control-label">البريد الالكتروني</label>
              <span class="md-help-block">سوف يتم ارسال كلمة مرور جديدة الى بريدك</span>
            </div>
            {{ csrf_field() }}
            <button class="btn btn-primary btn-block" type="submit">تغيير كلمة المرور</button>
          </form>
        </div>
    </div>
</div>
@endsection
