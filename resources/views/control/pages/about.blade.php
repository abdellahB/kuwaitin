@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">من نحن  </span>
            </h1>
        </div>
        <div class="row gutter-xs">
           @foreach($about as $about)	
			<form data-toggle="md-validator" method="POST" action="{{url('control/page/about')}}">	
				<p>
				<textarea name="about" class="pages" id="about">{{$about->about}}</textarea>
				</p>
				 {{ csrf_field() }}
				<button type="submit" name="update" value="{{$about->id}}" class="btn btn-info btn-block">تحديث الصفحة </button>
			</form>
			@endforeach
        </div> 
    </div>
</div>
@endsection