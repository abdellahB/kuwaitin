@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
	<div class="layout-content-body">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">سياسة الخصوصية</h2>
			</div>
			<!-- /.col-lg-12 -->
		</div>
			@foreach($privacy as $privacy)	
			<form data-toggle="md-validator" method="POST" action="{{url('control/page/privacy')}}">	
				<p>
				<textarea name="privacy" class="pages" id="privacy">{{$privacy->privacy}}</textarea>
				</p>
				 {{ csrf_field() }}
				<button type="submit" name="update" value="{{$privacy->id}}" class="btn btn-info btn-block">تحديث الصفحة </button>
			</form>
			@endforeach
	</div>
</div>
@endsection