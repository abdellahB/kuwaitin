@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">اتفاقية الاستخدام </span>
            </h1>
        </div>
        <div class="row gutter-xs">
            @foreach($terms as $terms)	
			<form data-toggle="md-validator" method="POST" action="{{url('control/page/terms')}}">	
				<p>
				<textarea name="terms" class="pages" id="terms">{{$terms->terms}}</textarea>
				</p>
				 {{ csrf_field() }}
				<button type="submit" name="update" value="{{$terms->id}}" class="btn btn-info btn-block">تحديث الصفحة </button>
			</form>
			@endforeach
        </div> 
    </div>
</div>
@endsection