@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">كل الاعلانات </span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{url('control/ads/create')}}" class="btn btn-success">اضافة اعلان جديد</a>
            </br>
            <table id="demo-dynamic-tables-2" class="table table-middle">
                <thead>
                    <tr>
                    <th>
                        <label class="custom-control custom-control-primary custom-checkbox">
                        <input id="checkAll" class="custom-control-input" type="checkbox">
                        <span class="custom-control-indicator"></span>
                        </label>
                    </th>
                    <th>معاينة</th>
                    <th>اسم الاعلان</th>
                    <th>النوع</th>
                    <th>طببعة الاعلان</th>
                    <th>رابط</th>
                    <th>المكان</th>
                    <th>تاريخ الانتهاء</th>
                    <th>الحالة</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($homeads as $ads)
                    <tr>
                        <th></th>
                        <td>
                        @if($ads->image)
                        <img class="rounded" width="40" height="40" src="{{url('public/assets/img/')}}/{{$ads->image}}" alt=""/>
                        @else
                        <img class="rounded" width="40" height="40" src="{{url('public/assets/img/300%20ads.jpg')}}" alt=""/>
                        @endif
                        </td>
                        <td>{{$ads->adsname}}</td>
                        <td>
                            @if($ads->adstype==1) 
                                صورة
                            @elseif($ads->adstype==2)
                                فيديو 
                            @elseif($ads->adstype==3)
                                 أدسنس
                            @endif
                        </td>
                        <td>
                            @if($ads->adsfeature==1) 
                                اعلان مميز
                            @elseif($ads->adsfeature==2)
                                اعلان عادي 
                            @endif
                        </td>
                        <td><a href="{{$ads->adsurl}}">معاينة</a></td>
                        <td>
                            @if($ads->adsplace==1) 
                                السلايدر الأفقي
                            @elseif($ads->adsplace==2)
                                اعلان جانبي 
                            @elseif($ads->adsplace==3)
                                اعلان سفلي
                            @elseif($ads->adsplace==4)
                                 شركات مميزة         
                            @endif
                        </td>
                         <td>{{$ads->enddate}}</td>
                        <td>
                            <?php 
                                $dt = new DateTime();
								$date = $dt->format('m/d/Y');
                            ?>
                            @if($ads->status ==0 && $ads->enddate >= $date )
                                <label class="label label-success label-pill">اعلان صالح</label>
                            @else
                                <label class="label label-primary label-pill">منتهي</label>
                            @endif
                        </td>
                        <td>
                            <div class="btn-group pull-right dropdown">
                            <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{route('ads.edit',$ads->id)}}"  class="btn-link goldorders" >تعديل الاعلان</a></li>
                                <li><a href="{{route('ads.destroy',$ads->id)}}"  class="btn-link goldorders" >ايقاف الاعلان</a></li>
                            </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection