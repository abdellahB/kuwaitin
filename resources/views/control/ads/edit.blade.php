@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">تعديل الاعلان </span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{URL::to('control/ads')}}" class="btn btn-success pull-right">كل الاعلانات</a>
            </br>
             @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
             {!! Form::model($ads, [
                    'method' => 'PATCH',
                    'route' => ['ads.update', $ads->id],
                    'class' => 'form','data-toggle' => 'validator','files' => true
                    ]) !!}
                <div class="form-group">
                    {!! Form::label('اسم اللوحة الاشهارية') !!}
                    {!! Form::text('adsname', null, 
                        array('required', 
                            'class'=>'form-control', 
                            'placeholder'=>'ضع اسم مميز لتمييز نوع الاعلان')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('نوع الأعلان') !!}
                    {!! Form::select('adstype', [1 => 'صورة', 2 => 'يوتوب',3=>'ادسنس'],$ads->adstype, ['required','class'=>'form-control adstype']); !!}
                </div>
                <div class="form-group">
                    {!! Form::label('مكان عرض الإعلان ') !!}
                    {!! Form::select('adsplace', [1 => 'إعلانات السلايدر الفوقي', 2 => 'إعلانات جانبية',3=>' إعلانات سفلية',4=>'شركات مميزة'],$ads->adsplace, ['required','class'=>'form-control adsplace']); !!}
                </div>
                <div class="form-group adsfeature" style="display:none">
                    {!! Form::label('طبيعة الاعلان') !!}
                    {!! Form::select('adsfeature', [1 => 'اعلان مميز', 2 => 'اعلان عادي'],$ads->adsfeature, ['required','class'=>'form-control']); !!}
                </div>
                <div class="form-group image">
                    {!! Form::label('رابط الاعلان') !!}
                    {!! Form::text('adsurl', null, 
                        array('required','class'=>'form-control','placeholder'=>'رابط الاعلان')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('تاريخ الانتهاء') !!}
                    {!! Form::date('enddate', null, 
                        array('required','class'=>'form-control','placeholder'=>'تاريخ نهاية الاعلان')) !!}
                </div>
                <div class="form-group image">
                    {!! Form::label('رفع الصورة') !!}
                    {!! Form::file('image', null, 
                        array('required','class'=>'form-control')) !!}
                </div>
                <div class="form-group youtube" style="display:none">
                    {!! Form::label('رابط فيديو يوتوب') !!}
                    {!! Form::text('youtube', null, 
                        array('required','class'=>'form-control', 
                            'placeholder'=>'ضع كود رابط الفيديو مثلا wGgy4yWQneU')) !!}
                </div>
                <div class="form-group adsense" style="display:none">
                    {!! Form::label('كود أدسنس') !!}
                    {!! Form::textarea('adsense', null, 
                        array('required','class'=>'form-control', 
                            'placeholder'=>'ضع كود ادسنس هنا 300*250')) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('نشر الاعلان', 
                    array('class'=>'btn btn-primary btn-block')) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection