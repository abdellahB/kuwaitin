@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">قائمة العملاء</span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{url('control/client/create')}}" class="btn btn-success">عميل جديد</a>
            <a href="#" class="btn btn-info pull-right">تصدير</a>
            </br></br>
            <table id="demo-dynamic-tables-2" class="table table-middle">
                <thead>
                    <tr>
                    <th>
                        <label class="custom-control custom-control-primary custom-checkbox">
                        <input id="checkAll" class="custom-control-input" type="checkbox">
                        <span class="custom-control-indicator"></span>
                        </label>
                    </th>
                    <th>صورة</th>
                    <th>اسم العميل</th>
                    <th>الجنس </th>
                    <th>رقم الهاتف</th>
                    <th>الايميل </th>
                    <th>نوع </th>
                    <th>تاريخ التسجيل</th>
                    <th>الحالة</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($user as $client)
                    <td></td>
                    <td>
                        <img src="" alt="users">
                    </td>
                    <td>{{$client->username}}</td>
                    <td>{{$client->sexe}}</td>
                    <td>{{$client->phone}}</td>
                    <td>{{$client->emailuser}}</td>
                    <td>{{$client->usertype}}</td>
                    <td>{{$client->created_at}}</td>
                    <td>{{$client->status}}</td>
                    <td>
                        <div class="btn-group pull-right dropdown">
                        <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                            <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="{{route('client.edit',$client->id)}}"  class="btn-link goldorders" >تعديل الحساب</a></li>
                            <li><a href="{{route('client.destroy',$client->id)}}"  class="btn-link goldorders" ايقاف الحساب</a></li>
                        </ul>
                        </div>
                    </td>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection