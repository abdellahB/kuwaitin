@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">المناطق و المحافظات</span>
            </h1>
        </div>
         <div class="row">
            <div class="col-lg-12">
              <div class="demo-form-wrapper">
                <form class="form form-inline" method="post" action="{{URL('control/city')}}">
                    {{ csrf_field() }}                  
                    <div class="form-group">
                        <select name="statname" class="form-control">
                           <option value="الكويت">الكويت</option>
                            <option value="حولي">حولي</option>
                            <option value="الفروانية">الفروانية</option>
                            <option value="مبارك الكبير">مبارك الكبير</option>
                            <option value="الجهراء">الجهراء</option>
                            <option value="الأحمدي">الأحمدي</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="villename" type="text" placeholder="المنطقة بالعربية" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="villename_en" type="text" placeholder="المنطقة بالانجليوية" required>
                    </div>
                        <button class="btn btn-primary" type="submit">اضافة المنظقة </button>
                </form>
              </div>
            </div>
        </div>
        <div class="row gutter-xs">
            <table id="demo-dynamic-tables-2" class="table table-middle">
                <thead>
                    <tr>
                   
                    <th>المحافظة</th>
                    <th>المنظقة</th>
                    <th>تاريخ الاضافة</th>
                    <th>الحالة</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($villages as $offer)
                        <tr>
                           
                            <td>{{$offer->statname}}</td>
                            <td>{{$offer->villename}}</td>
                            <td>{{$offer->created_at}}</td>
                            <td>
                                @if($offer->status==0)
                                    <label class="label label-success label-pill">منشور</label>
                                @else
                                    <label class="label label-primary label-pill">تم ايقافه</label>
                                @endif
                            </td>
                            <td>
                                <div class="btn-group pull-right dropdown">
                                <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                    <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href=""  class="btn-link goldorders" >ازالة المنطقة</a></li>
                                </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection