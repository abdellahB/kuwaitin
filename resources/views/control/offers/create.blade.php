@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">إضافة عرض جديد  </span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{URL::to('control/offer')}}" class="btn btn-success pull-right">كل العروض</a>
            </br>
             @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            {!! Form::open(array('url' => 'control/offer', 'class' => 'form','data-toggle' => 'validator','files' => true)) !!}
                <div class="form-group">
                    {!! Form::label('اسم العرض') !!}
                    {!! Form::text('offername', null, 
                        array('required', 
                            'class'=>'form-control', 
                            'placeholder'=>'ضع اسم مميز للعرض')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('طبيعة الاعلان') !!}
                    {!! Form::select('offertype', [1 => 'عرض عادي', 2 => 'عرض مميز'],1, ['required','class'=>'form-control']); !!}
                </div>
                <div class="form-group">
                    {!! Form::label('رابط الاعلان') !!}
                    {!! Form::text('offerurl', null, 
                        array('required','class'=>'form-control','placeholder'=>'رابط الاعلان')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('تاريخ الانتهاء') !!}
                    {!! Form::text('offerenddate', null, 
                        array('required','data-provide'=>'datepicker','class'=>'form-control','placeholder'=>'تاربخ نهاية العرض')) !!}
    
               </div>
                <div class="form-group">
                    {!! Form::label('رفع الصورة') !!}
                    {!! Form::file('offerimage', null, 
                        array('required','class'=>'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::submit('نشر العرض', 
                    array('class'=>'btn btn-primary btn-block')) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection