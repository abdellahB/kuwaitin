@extends('control.layouts.admin')
@section('content')
<div class="layout-header">
      <div class="navbar navbar-default">
        <div class="navbar-header">
          <a class="navbar-brand navbar-brand-center" href="{{url('/control')}}">
            <!--<img class="navbar-brand-logo" src="images/logotest23.png" alt="shopandshiptome">-->
	          <b style="color:#fff">kuwaitin</b>
          </a>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
            <span class="sr-only">Toggle navigation</span>
            <span class="bars">
              <span class="bar-line bar-line-1 out"></span>
              <span class="bar-line bar-line-2 out"></span>
              <span class="bar-line bar-line-3 out"></span>
            </span>
            <span class="bars bars-x">
              <span class="bar-line bar-line-4"></span>
              <span class="bar-line bar-line-5"></span>
            </span>
          </button>
          <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="arrow-up"></span>
            <span class="ellipsis ellipsis-vertical">
              <img class="ellipsis-object" width="32" height="32" src="img/0180441436.jpg" alt="Teddy Wilson">
            </span>
          </button>
        </div>
        <div class="navbar-toggleable">
          <nav id="navbar" class="navbar-collapse collapse">
            <button class="sidenav-toggler hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="bars">
                <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                <span class="bar-line bar-line-4 in"></span>
                <span class="bar-line bar-line-5 in"></span>
                <span class="bar-line bar-line-6 in"></span>
              </span>
            </button>
            <ul class="nav navbar-nav navbar-right">
              <li class="visible-xs-block">
                <h4 class="navbar-text text-center">مرحبا :</h4>
              </li>
              <li class="hidden-xs hidden-sm">
                <form class="navbar-search navbar-search-collapsed">
                  <div class="navbar-search-group">
                    <input class="navbar-search-input" type="text" placeholder="البحث عن العملاء">
                    <button class="navbar-search-toggler" title="Expand search form ( S )" aria-expanded="false" type="submit">
                      <span class="icon icon-search icon-lg"></span>
                    </button>
                    <button class="navbar-search-adv-btn" type="button"></button>
                  </div>
                </form>
              </li>
              <li class="dropdown">
              </li>
              <li class="dropdown">
                <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true">
                  <span class="icon-with-child hidden-xs">
                    <span class="icon icon-bell-o icon-lg"></span>
                    <span class="badge badge-danger badge-above right">0</span>
                  </span>
                  <span class="visible-xs-block">
                    <span class="icon icon-bell icon-lg icon-fw"></span>
                    <span class="badge badge-danger pull-right">0<span>
                    الاشعارات
                  </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                  <div class="dropdown-header">
                    <a class="dropdown-link" href="#"></a>
                    <h5 class="dropdown-heading">اخر الاشعارات</h5>
                  </div>
                  <div class="dropdown-body">
                    <div class="list-group list-group-divided custom-scrollbar">
                        <div class="notification">
                          <div class="notification-media">
                            <span class="icon icon-exclamation-triangle bg-warning rounded sq-40"></span>
                          </div>
                        <div class="notification-content">
                            <small class="notification-timestamp">6min</small>
                            <h5 class="notification-heading">عنوان الاشعار</h5>
                            <p class="notification-text">
                              <small class="truncate">
                                 نص الاشعار نص الاشعار نص الاشعار  
                              </small>
                            </p>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </li>
              <li class="dropdown hidden-xs">
                <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true">
                   <img class="circle" width="36" height="36" src="{{url('public/assets/admin/images/users/user14814720400299419341.jpg')}}" alt=""> 
                   {{Auth::guard('admin')->user()->username}}
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right">
                  <!--<li><a href="profile">الملف الشخصي</a></li>-->
                  <li><a href="{{url('control/logout')}}">تسجيل الخروج</a></li>
                </ul>
              </li>
             <!-- <li class="visible-xs-block">
                <a href="profile">
                  <span class="icon icon-user icon-lg icon-fw"></span>
                  الملف الشخصي
                </a>
              </li>-->
              <li class="visible-xs-block">
                <a href="{{url('control/logout')}}">
                  <span class="icon icon-power-off icon-lg icon-fw"></span>
                  تسجيل الخروج
                </a>
              </li>
            </ul>
          </nav>
        </div>
    </div>
</div>
<div class="layout-main">
    <div class="layout-sidebar">
        <div class="layout-sidebar-backdrop"></div>
        <div class="layout-sidebar-body">
            <div class="custom-scrollbar">
                <nav id="sidenav" class="sidenav-collapse collapse">
                    <ul class="sidenav">
                        <li class="sidenav-search hidden-md hidden-lg">
                            <form class="sidenav-form" action="/">
                                <div class="form-group form-group-sm">
                                    <div class="input-with-icon">
                                        <input class="form-control" type="text" placeholder="البحث ...">
                                        <span class="icon icon-search input-icon"></span>
                                    </div>
                                </div>
                            </form>
                        </li>
                        <li class="sidenav-heading">القائمة</li>
                        <li class="sidenav-item">
                          <a href="{{url('/control')}}" aria-haspopup="true">
                            <span class="sidenav-icon icon icon-home"></span>
                            <span class="sidenav-label">لوحة التحكم </span>
                          </a>
                        </li>
                        <li class="sidenav-heading">قسم الاعلانات</li>
                        <li class="sidenav-item has-subnav">
                            <a href="#" aria-haspopup="true">
                              <span class="sidenav-icon icon icon-wpforms"></span>
                               
                              <span class="sidenav-label">اعلانات سيارات</span>
                            </a>
                          <ul class="sidenav-subnav collapse">
                            <li><a href="{{url('control/adsStep/new')}}">
                            <span class="badge badge-success">{{ $classcars1 }}</span> اعلانات قيد المراجعة</a></li>
                            <li><a href="{{url('control/adsStep/published')}}">
                            <span class="badge badge-success">{{ $classcars2 }}</span> اعلانات منشورة </a></li>
                            <li><a href="{{url('control/adsStep/feature')}}">
                            <span class="badge badge-success">{{ $classcars3 }}</span> اعلانات مميزة </a></li>
                            <li><a href="{{url('control/adsStep/trashed')}}">
                            <span class="badge badge-success">{{ $classcars4 }}</span> اعلانات محدوفة </a></li>
                            <li><a href="{{route('adsStep.create')}}"> اضافة اعلان </a></li>
                          </ul>
                        </li>
                        <li class="sidenav-item has-subnav">
                            <a href="#" aria-haspopup="true">
                              <span class="sidenav-icon icon icon-wpforms"></span>
                              
                              <span class="sidenav-label">عقارات الكويت</span>
                            </a>
                          <ul class="sidenav-subnav collapse">
                            <li><a href="{{URL::to('control/company')}}">  <span class="badge badge-success">20</span> اعلانات قيد المراجعة</a></li>
                            <li><a href="{{url('control/company/create')}}">  <span class="badge badge-success">20</span> اعلانات منشورة </a></li>
                            <li><a href="{{url('control/company/create')}}">  <span class="badge badge-success">20</span> اعلانات مميزة </a></li>
                            <li><a href="{{url('control/company/create')}}">  <span class="badge badge-success">20</span> اعلانات محدوفة </a></li>
                          </ul>
                        </li>
                        <li class="sidenav-item has-subnav">
                            <a href="#" aria-haspopup="true">
                              <span class="sidenav-icon icon icon-wpforms"></span>
                               
                              <span class="sidenav-label">الكترونيات</span>
                            </a>
                          <ul class="sidenav-subnav collapse">
                            <li><a href="{{URL::to('control/company')}}">  <span class="badge badge-success">20</span> اعلانات قيد المراجعة</a></li>
                            <li><a href="{{url('control/company/create')}}">  <span class="badge badge-success">20</span> اعلانات منشورة </a></li>
                            <li><a href="{{url('control/company/create')}}">  <span class="badge badge-success">20</span> اعلانات مميزة </a></li>
                            <li><a href="{{url('control/company/create')}}">  <span class="badge badge-success">20</span> اعلانات محدوفة </a></li>
                          </ul>
                        </li>
                        <li class="sidenav-item has-subnav">
                            <a href="#" aria-haspopup="true">
                              <span class="sidenav-icon icon icon-wpforms"></span>
                               
                              <span class="sidenav-label">وظائف الكويت</span>
                            </a>
                          <ul class="sidenav-subnav collapse">
                           <li><a href="{{URL::to('control/company')}}">  <span class="badge badge-success">20</span> اعلانات قيد المراجعة</a></li>
                            <li><a href="{{url('control/company/create')}}">  <span class="badge badge-success">20</span> اعلانات منشورة </a></li>
                            <li><a href="{{url('control/company/create')}}">  <span class="badge badge-success">20</span> اعلانات مميزة </a></li>
                            <li><a href="{{url('control/company/create')}}"> <span class="badge badge-success">20</span> اعلانات محدوفة </a></li>
                          </ul>
                        </li>
                        <li class="sidenav-heading">قسم الشركات</li>
                        <li class="sidenav-item has-subnav">
                            <a href="#" aria-haspopup="true">
                              <span class="sidenav-icon icon icon-wpforms"></span>
                              <span class="sidenav-label">الشركات</span>
                            </a>
                          <ul class="sidenav-subnav collapse">
                            <li><a href="{{URL::to('control/company')}}">كل الشركات</a></li>
                            <li><a href="{{url('control/company/create')}}">اضافة شركة </a></li>
                          </ul>
                        </li>
                        <li class="sidenav-item has-subnav">
                            <a href="#" aria-haspopup="true">
                              <span class="sidenav-icon icon icon-wpforms"></span>
                              <span class="sidenav-label">الموظفين</span>
                            </a>
                          <ul class="sidenav-subnav collapse">
                            <li><a href="{{URL::to('control/employer')}}">كل الموظفين</a></li>
                            <li><a href="{{url('control/employer/create')}}">اضافة موظف جديد </a></li>
                          </ul>
                        </li>
                        <li class="sidenav-item has-subnav">
                            <a href="#" aria-haspopup="true">
                              <span class="sidenav-icon icon icon-wpforms"></span>
                              <span class="sidenav-label">أقسام دليل الشركات</span>
                            </a>
                          <ul class="sidenav-subnav collapse">
                            <li><a href="{{URL::to('control/cat')}}">كل الأقسام</a></li>
                            <li><a href="{{url('control/cat/create')}}">قسم جديد </a></li>
                            <li><a href="{{url('control/tags')}}">وسوم أو خدمات </a></li>
                          </ul>
                        </li>
                        <li class="sidenav-heading"> </li>
                        <li class="sidenav-item has-subnav">
                            <a href="#" aria-haspopup="true">
                              <span class="sidenav-icon icon icon-wpforms"></span>
                              <span class="sidenav-label">العروض</span>
                            </a>
                          <ul class="sidenav-subnav collapse">
                            <li><a href="{{URL::to('control/offer')}}">كل العروض</a></li>
                            <li><a href="{{url('control/offer/create')}}">اضافة عرض </a></li>
                          </ul>
                        </li>
                        <li class="sidenav-item has-subnav">
                            <a href="#" aria-haspopup="true">
                              <span class="sidenav-icon icon icon-wpforms"></span>
                              <span class="sidenav-label">الإعلانات المدفوعة</span>
                            </a>
                          <ul class="sidenav-subnav collapse">
                            <li><a href="{{URL::to('control/ads')}}">كل الاعلانات </a></li>
                            <li><a href="{{url('control/ads/create')}}">اضافة اعلان </a></li>
                          </ul>
                        </li>
                        <li class="sidenav-heading">التصنيقات </li>
                        <li class="sidenav-item has-subnav">
                            <a href="#" aria-haspopup="true">
                              <span class="sidenav-icon icon icon-cogs"></span>
                              <span class="sidenav-label">الأقسام</span>
                            </a>
                            <ul class="sidenav-subnav collapse">	
                                <li><a href="{{url('control/category/cars')}}">حراج السيارات</a></li>
                                <li><a href="{{url('control/category/builds')}}">عقارات الكويت</a></li>
                                <li><a href="{{url('control/category/electro')}}">الكترونيات</a></li>
                                <li><a href="{{url('control/category/jobs')}}">وظائف الكويت</a></li>
                                <li><a href="{{url('control/category/scrap')}}">خدمات سكراب</a></li>
                                <li><a href="{{url('control/category/homes')}}">خدمات منزلية</a></li>
                                <li><a href="{{url('control/category/lance')}}">اعمال حرة</a></li>
                                <li><a href="{{url('control/category/others')}}">أخرى</a></li>
                                <li><a href="{{url('control/category/runt')}}">تاجير السيارات</a></li>
                            </ul>
                        </li> 
                        <li class="sidenav-heading">الأعدادات</li>
                        <li class="sidenav-item has-subnav">
                            <a href="#" aria-haspopup="true">
                              <span class="sidenav-icon icon icon-cogs"></span>
                              <span class="sidenav-label">الأعدادات</span>
                            </a>
                            <ul class="sidenav-subnav collapse">	
                                <li><a href="{{url('control/settings')}}">الاعدادت العامة</a></li>
                                <li><a href="{{url('control/client')}}">حسابات العملاء</a></li>
                                <li><a href="{{url('control/role')}}">المستخدمين</a></li>
                                <li><a href="{{url('control/city')}}">المناطق</a></li>
                                <li><a href="{{url('control/menu')}}">القوائم</a></li>
                            </ul>
                        </li> 
                        <li class="sidenav-item has-subnav">
                            <a href="#" aria-haspopup="true">
                              <span class="sidenav-icon icon icon-wpforms"></span>
                              <span class="sidenav-label">الصفحات</span>
                            </a>
                          <ul class="sidenav-subnav collapse">
                            <li><a href="{{url('control/page/terms')}}">اتفافية الاستخدام </a></li>
                            <li><a href="{{url('control/page/privacy')}}">سياسة الخصوصية </a></li>
                            <li><a href="{{url('control/page/about')}}">من نحن  </a></li>
                          </ul>
                        </li>
                        <li class="sidenav-heading"></li>
                      </ul>
                </nav>  
            </div>
        </div>
    </div>

    @yield('navbar')
    	
</div>
@endsection