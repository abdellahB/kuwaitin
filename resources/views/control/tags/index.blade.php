@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">الوسوم او الخدمات </span>
            </h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
              <div class="demo-form-wrapper">
                <form class="form form-inline" method="post" action="{{URL('control/tags')}}">
                 {{ csrf_field() }}                  
                  <div class="form-group">
                    <div class="input-with-icon">
                      <input class="form-control" name="tagname" type="text" placeholder="الوسم بالعربية " required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-with-icon">
                      <input class="form-control" name="tagname_en" type="text" placeholder="الوسم بالاتنجليزية" required>
                    </div>
                  </div>
                  <button class="btn btn-primary" type="submit">اضافة الوسم </button>
                </form>
              </div>
            </div>
        </div>
        <div class="row gutter-xs">
            <table id="demo-dynamic-tables-2" class="table table-middle">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>اسم الوسم ع</th>
                    <th>اسم الوسم </th>
                    <th>تاريخ الاضافة</th>
                    <th>تاريخ التعديل  </th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                     @foreach($tags as $tag)
                        <tr>    
                            <td>{{$tag->id}}</td>
                            <td>{{$tag->tagname}}</td>
                            <td>{{$tag->tagname_en}}</td>
                            <td>{{ Carbon\Carbon::parse($tag->created_at)->format('d-m-Y ') }}</td>
                            <td>{{ Carbon\Carbon::parse($tag->updated_at)->format('d-m-Y ') }}</td>
                            <td>
                                <div class="btn-group pull-right dropdown">
                                <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                    <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('tags.edit',$tag->id)}}"  class="btn-link goldorders" >تعديل الوسم</a></li>
                                    <li><a href="{{route('tags.destroy',$tag->id)}}"  class="btn-link goldorders" >حدف الوسم</a></li>
                                </ul>
                                </div>
                            </td>
                        </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection