@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">اضافة قسم جديد  </span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{url('control/category/electro')}}" class="btn btn-success pull-right">كل الاقسام</a>
            </br>
             @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
             {!! Form::open(array('url' => 'control/category/electro', 'class' => 'form','data-toggle' => 'validator','files' => true)) !!}
                <div class="form-group">
                    {!! Form::label('اسم القسم') !!}
                    {!! Form::text('catname', null, 
                        array('required', 
                            'class'=>'form-control', 
                            'placeholder'=>'اكتب اسم القسم')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('اسم القسم بالانجليزية') !!}
                    {!! Form::text('catname_en', null, 
                        array('required', 
                            'class'=>'form-control', 
                            'placeholder'=>'اسم القسم بالانجليزية')) !!}
                </div>
                <div class="form-group">
                        {!! Form::label('لون الشعار ') !!}
                        {!! Form::text('catcolor', null, 
                            array('required','class'=>'form-control','placeholder'=>'اكثت لون الشعار مثلا green ')) !!}
                </div>
                <div class="form-group catflag">
                    {!! Form::label('شعار القسم http://fontawesome.io/icons/') !!}
                    {!! Form::text('catflag', null, 
                        array('class'=>'form-control','placeholder'=>'ضع شعار القسم مثلا fa fa-car ')) !!}
                </div>
                <div class="form-group catphoto" style="display:none">
                    {!! Form::label('رفع شعار القسم') !!}
                    {!! Form::file('catphoto', null, 
                        array('class'=>'form-control')) !!}
                </div>
                <div class="form-group">
                   <label class="custom-control custom-control-primary custom-checkbox">
                        <input class="custom-control-input isAgeSelected" type="checkbox" name="mode" value="1">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-label">رفع صورة للقسم</span>
                    </label>
                </div>
                <div class="form-group">
                   <label class="custom-control custom-control-primary custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="cathome" value="1">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-label">اظهار القسم في الصفحة الرئسية</span>
                    </label>
                </div>
                <div class="form-group">
                    {!! Form::submit('نشر القسم', 
                    array('class'=>'btn btn-primary btn-block')) !!}
                </div>
            {!! Form::close() !!}
    </div>
</div>
@endsection