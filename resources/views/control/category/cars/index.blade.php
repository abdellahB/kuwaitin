@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">حراج السيارات  </span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{url('control/category/cars/create')}}" class="btn btn-success">قسم جديد</a>
            </br>
            <table id="demo-dynamic-tables-2" class="table table-middle">
                <thead>
                    <tr>
                    <th>
                        <label class="custom-control custom-control-primary custom-checkbox">
                        <input id="checkAll" class="custom-control-input" type="checkbox">
                        <span class="custom-control-indicator"></span>
                        </label>
                    </th>
                    <th>الشعار</th>
                    <th>لون الشعار</th>
                    <th>اسم القسم</th>
                    <th>اسم القسم بالانجليزية</th>
                    <th>تاريخ الاضافة</th>
                    <th>الحالة</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($cats as $cat)
                        <tr>
                            <td></td>
                            <td>
                            @if($cat->catphoto)
                               <img class="rounded" width="40" height="40" src="{{url('public/assets/img/')}}/{{$cat->catphoto}}" alt=""/>
                            @else
                                <i class="{{$cat->catflag}} fa-2x"></i>
                            @endif
                            </td>
                            <td>{{$cat->catcolor}}</td>
                            <td>{{$cat->catname}}</td>
                            <td>{{$cat->catname_en}}</td>
                            <td>{{ Carbon\Carbon::parse($cat->created_at)->format('d-m-Y ') }}</td>
                            <td>
                            @if($cat->status==0)
                                 <label class="label label-success label-pill">منشور</label>
                            @else
                                 <label class="label label-primary label-pill">تم ايقافه</label>
                            @endif
                            </td>
                            <td>
                                <div class="btn-group pull-right dropdown">
                                <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                    <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('cars.edit',$cat->id)}}"  class="btn-link" >تعديل القسم</a></li>
                                    <li><a href="{{route('cat.destroy',$cat->id)}}"  class="btn-link" >ايقاف القسم</a></li>
                                </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection