@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">المعلومات العامة </span>
            </h1>
        </div>
        <div class="row gutter-xs">
             @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p> 
                </div>
            @endif
            {!! Form::open(array('url' => 'control/settings', 'class' => 'form','data-toggle' => 'validator','files' => true)) !!}
                
                <div class="col-lg-6">
                    <div class="form-group">
                        {!! Form::label('اسم الموقع بالعربية') !!}
                        {!! Form::text('sitename',$settings->sitename, 
                            array('required', 
                                'class'=>'form-control', 
                                'placeholder'=>'اسم الموقع بالعربية')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::hidden('settingsid',$settings->id, 
                            array('required')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('حساب فيس بوك') !!}
                        {!! Form::text('site_facebook',$settings->site_facebook, 
                            array('required', 
                            'class'=>'form-control','placeholder'=>'حساب فيس بوك')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('حساب توتير') !!}
                        {!! Form::text('site_twitter',$settings->site_twitter, 
                            array('required', 
                            'class'=>'form-control','placeholder'=>'حساب توتير')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('حساب يوبوب') !!}
                        {!! Form::text('site_youtube',$settings->site_youtube, 
                            array('required', 
                            'class'=>'form-control','placeholder'=>'حساب يوتوب')) !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        {!! Form::label('اسم الموقع بالانجليزية') !!}
                        {!! Form::text('sitename_en',$settings->sitename_en, 
                            array('required', 
                            'class'=>'form-control','placeholder'=>'اسم الموقع بالانجليزية')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('حساب انستغرام') !!}
                        {!! Form::text('site_instagram',$settings->site_instagram, 
                            array('required', 
                            'class'=>'form-control','placeholder'=>'حساب انستغرام')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('رقم الهاتف') !!}
                        {!! Form::text('site_phone',$settings->site_phone, 
                            array('required', 
                            'class'=>'form-control','placeholder'=>'رقم الهاتف')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('البريد الالكتروني') !!}
                        {!! Form::text('site_email',$settings->site_email, 
                            array('required', 
                            'class'=>'form-control','placeholder'=>'البريد الالكتروني')) !!}
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group">
                        {!! Form::label('شعار الموقع') !!}
                        {!! Form::file('sitelogo',
                            array('class'=>'form-control')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('وصف الموقع بالعربية') !!}
                        {!! Form::textarea('description',$settings->description, 
                            array('required', 
                            'class'=>'form-control')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('وصف الموفع بالانجليزية') !!}
                        {!! Form::textarea('description_en',$settings->description_en, 
                            array('required', 
                            'class'=>'form-control')) !!}
                    </div>
                </div>
                
                <div class="form-group">
                    {!! Form::submit('تحديث المعلومات ', 
                    array('class'=>'btn btn-primary btn-block')) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection