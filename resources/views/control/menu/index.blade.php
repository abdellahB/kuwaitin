@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">كل العروض </span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{url('control/offer/create')}}" class="btn btn-success">اضافة عرض جديد</a>
            </br>
            <table id="demo-dynamic-tables-2" class="table table-middle">
                <thead>
                    <tr>
                    <th>
                        <label class="custom-control custom-control-primary custom-checkbox">
                        <input id="checkAll" class="custom-control-input" type="checkbox">
                        <span class="custom-control-indicator"></span>
                        </label>
                    </th>
                    <th>معاينة</th>
                    <th>اسم العرض</th>
                    <th>طبيعة الاعلان</th>
                    <th>رابط الاعلان</th>
                    <th>تاريخ الانتهاء</th>
                    <th>الحالة</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($offers as $offer)
                        <tr>
                            <td>
                                <img class="rounded" width="40" height="40" src="{{url('public/assets/img/')}}/{{$offer->offerimage}}" alt=""/>
                            </td>
                            <td>{{$offer->offername}}</td>
                            <td>
                                @if($offer->offertype==1) 
                                   عرض عادي
                                @elseif($offer->offertype==2)
                                    عرض مميز
                                @endif
                            </td>
                            <td>{{$offer->offerurl}}</td>
                            <td>{{$offer->offerenddate}}</td>
                            <td>
                            @if($offer->status ==0 )
                                <label class="label label-success label-pill">اعلان صالح</label>
                            @else
                                <label class="label label-primary label-pill">منتهي</label>
                            @endif
                            </td>
                            <td>
                                <div class="btn-group pull-right dropdown">
                                <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                    <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('offer.edit',$offer->id)}}"  class="btn-link goldorders" >تعديل العرض</a></li>
                                    <li><a href="{{route('offer.destroy',$offer->id)}}"  class="btn-link goldorders" >ايقاف العرض</a></li>
                                </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection