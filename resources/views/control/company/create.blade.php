@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">اضافة شركة  </span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{URL::to('control/company')}}" class="btn btn-success pull-right">كل الشركات</a>
            </br>
             @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#arabic"> <i class="flag-icon flag-icon-sa"></i> العربية</a></li>
                <li><a data-toggle="tab" href="#english"> <i class="flag-icon flag-icon-us"></i> الانجليزية</a></li>
            </ul>
            {!! Form::open(array('url' => 'control/company', 'class' => 'form','data-toggle' => 'validator','files' => true)) !!}
            <div class="tab-content">
                <div id="arabic" class="tab-pane fade in active">
                    <h3></h3>
                   <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('اسم الشركة') !!}
                            {!! Form::text('compname', null, 
                                array('required', 
                                    'class'=>'form-control', 
                                    'placeholder'=>'ضع اسم الشركة')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('عنوان الشركة') !!}
                            {!! Form::text('comptitle', null, 
                                array('required','class'=>'form-control','placeholder'=>'ضع عنوان للشركة')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('مكان الشركة') !!}
                            {!! Form::text('complace', null, 
                                array('required','class'=>'form-control','placeholder'=>'مكان تواجد الشركة')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('قسم الشركة') !!}
                             {!! Form::select('catcompanie_id',$cats,1, ['required','class'=>'form-control']); !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label('اختصاص الشركة ') !!}
                                {!! Form::text('compworking', null, 
                                    array('required','class'=>'form-control','placeholder'=>'ضع اختصاص الشركة ')) !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label('الهاتف 1 ') !!}
                                {!! Form::text('comphone1', null, 
                                    array('required','class'=>'form-control','placeholder'=>'ضع اختصاص الشركة ')) !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label('فيس بوك') !!}
                                {!! Form::text('compfacebook', null, 
                                    array('required','class'=>'form-control','placeholder'=>'فيس بوك ')) !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label(' تويتر ') !!}
                                {!! Form::text('comptwitter', null, 
                                    array('required','class'=>'form-control','placeholder'=>'تويتر ')) !!}
                        </div>
                   </div>
                   <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('الموقع الالكتروني') !!}
                            {!! Form::text('compwebsite', null, 
                                array('required', 
                                    'class'=>'form-control', 
                                    'placeholder'=>'الموقع الالكتروني')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('البريد الالكتروني') !!}
                            {!! Form::text('compemail', null, 
                                array('required','class'=>'form-control','placeholder'=>'البريد الالكتروني')) !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label('الهاتف 2 ') !!}
                                {!! Form::text('comphone2', null, 
                                    array('required','class'=>'form-control','placeholder'=>'ضع اختصاص الشركة ')) !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label(' يوتوب') !!}
                                {!! Form::text('compyoutube', null, 
                                    array('required','class'=>'form-control','placeholder'=>'يوتوب  ')) !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label(' انستغرام  ') !!}
                                {!! Form::text('compinstagram', null, 
                                    array('required','class'=>'form-control','placeholder'=>'انستغرام  ')) !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label(' جوجل بلوس  ') !!}
                                {!! Form::text('compgoogleplus', null, 
                                    array('required','class'=>'form-control','placeholder'=>'جوجل بلوس  ')) !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label('كلمة المرور ') !!}
                                {!! Form::text('password', null, 
                                    array('required','class'=>'form-control','placeholder'=>'كلمة مرور لحساب الشركة ')) !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label(' صورة الشركة ') !!}
                                {!! Form::file('complogo', null, 
                                    array('required','class'=>'form-control','placeholder'=>'صورة الشركة ')) !!}
                        </div>
                   </div>  
                    <div class="col-lg-12">
                        <div class="form-group">
                             {!! Form::label('خدمات الشركة بينها فاصلة') !!}
                             {!! Form::select('tagname[]',$tags,null, ['multiple'=>'multiple','class'=>'selectstate demo-default']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('عن الشركة ') !!}
                            {!! Form::textarea('compabout', null, 
                                array('required','rows'=>6,'class'=>'form-control','placeholder'=>'عن الشركة ')) !!}
                        </div> 
                    </div>
                </div>
                <div id="english" class="tab-pane fade">
                     <h3></h3>
                    <div class="form-group">
                        {!! Form::label('اسم الشركة') !!}
                        {!! Form::text('compname_en', null, 
                            array('required', 
                                'class'=>'form-control', 
                                'placeholder'=>'ضع اسم الشركة')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('عنوان الشركة') !!}
                        {!! Form::text('comptitle_en', null, 
                            array('required','class'=>'form-control','placeholder'=>'ضع عنوان للشركة')) !!}
                    </div>
                     <div class="form-group">
                            {!! Form::label('اختصاص الشركة ') !!}
                            {!! Form::text('compworking_en', null, 
                                array('required','class'=>'form-control','placeholder'=>'ضع اختصاص الشركة ')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('عن الشركة ') !!}
                        {!! Form::textarea('compabout_en', null, 
                            array('required','rows'=>6,'class'=>'form-control','placeholder'=>'عن الشركة ')) !!}
                    </div> 
                    <div class="form-group">
                        {!! Form::submit('انشاء حساب الشركة ', 
                        array('class'=>'btn btn-primary btn-block')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection