@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">كل الشركات  </span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{url('control/company/create')}}" class="btn btn-success">اضافة شركة جديدة</a>
            </br>
            <table id="demo-dynamic-tables-2" class="table table-middle">
                <thead>
                    <tr>
                    <th>شعار</th>
                    <th>اسم الشركة</th>
                    <th>قسم الشركة</th>
                    <th>مجال الشركة</th>
                    <th>الهاتف 1</th>
                    <th>الأيميل  </th>
                    <th>المكان</th>
                    <th>الحالة</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($company as $comp)
                        <tr>
                            <td>
                             <img class="rounded" width="40" height="40" src="{{url('public/assets/img/')}}/{{$comp->complogo}}" alt=""/>
                            </td>
                            <td>{{ $comp->compname}}</td>
                            <td>{{ $comp->cat->catname }}</td>
                            <td>{{ $comp->compworking}}</td>
                            <td>{{ $comp->comphone1}}</td>
                            <td>{{ $comp->compemail}}</td>
                            <td>{{ $comp->complace}}</td>
                            <td>
                                @if($comp->status==0)
                                    <label class="label label-success label-pill">منشور</label>
                                @else
                                    <label class="label label-primary label-pill">تم ايقافه</label>
                                 @endif
                            </td>
                            <td>
                                <div class="btn-group pull-right dropdown">
                                <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                    <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('company.edit',$comp->id)}}"  class="btn-link" >تعديل معلومات الشركة</a></li>
                                    <li><a href="#"  class="btn-link" >تعديل معلومات الحساب</a></li>
                                    <li><a href="{{route('employer.show',$comp->id)}}"  class="btn-link" >موظفي الشركة</a></li>
                                    <li><a href="#"  class="btn-link" >اعلانات الشركة</a></li>
                                    <li><a href="#"  class="btn-link" >الدخول للحساب</a></li>
                                    <li><a href="#"  class="btn-link">ايقاف حساب الشركة</a></li>
                                </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection