@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">اضافة اعلان السيارات</span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#arabic"> <i class="flag-icon flag-icon-sa"></i> العربية</a></li>
                <li><a data-toggle="tab" href="#english"> <i class="flag-icon flag-icon-us"></i> الانجليزية</a></li>
            </ul>
            {!! Form::open(array('route' => 'adsStep.store', 'class' => 'form','data-toggle' => 'validator','files' => true)) !!}
            <div class="tab-content">
                <div id="arabic" class="tab-pane fade in active">
                    <h3></h3>
                    @if ($message = Session::get('success'))
                        <div class="alert-success success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                   <div class="col-lg-6">
                        <div class="form-group">
                            <label>صورة الاعلان</label>
                            <input name="upcar_video" type="file" class="form-control car-upvid" />
                        </div>
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>عنوان الاعلان</label>
                            <input name="upcar_title" type="text" class="form-control" placeholder="عنوان الاعلان" required />
                        </div>
                        <div class="form-group">
                            <label>السعر</label>
                            <input name="upcar_price" type="text" class="form-control" placeholder="السعر" required/>
                        </div>
                        <div class="form-group">
                            <label>رقم الهاتف 1</label>
                            <input name="upcar_fstphone" type="number" class="form-control" placeholder="رقم الهاتف 1" required/>
                        </div>
                        <div class="form-group">
                            <label>رقم الهاتف 2</label>
                            <input name="upcar_secphone" type="number" class="form-control" placeholder="رقم الهاتف 2" />
                        </div>
                        <div class="form-group">
                            <label>حجم  الرنقات</label>
                            <input name="upcar_size" type="text" class="form-control" placeholder="حجم  الرنقات"/>
                        </div>
                        <div class="form-group">
                            <label>المسافة المقطوعة</label>
                            <input name="upcar_dist" type="text" class="form-control" placeholder="المسافة المقطوعة"/>
                        </div>
                        <div class="form-group">
                            <label>المحافظة </label>
                            <select name="upcar_gov" class="form-control upcargov">
                            <option value="">اختر المحافظة</option>
                            <option value="الكويت">الكويت</option>
                            <option value="حولي">حولي</option>
                            <option value="الفروانية">الفروانية</option>
                            <option value="مبارك الكبير">مبارك الكبير</option>
                            <option value="الجهراء">الجهراء</option>
                            <option value="الأحمدي">الأحمدي</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>المنطقة </label>
                            <select name="upcar_city" class="form-control domains_table" required>
                                @include('../../includes.city')
                            </select>
                        </div>
                        <div class="form-group">
                            <label>اكمال العنوان </label>
                            <input name="upcar_adress" type="text" class="form-control" placeholder="اكتب تفاصيل العنوان : القطعة , الشارع , رقم المنزل / البناية , الدور , رقم الشقة / المكتب , " required/>
                        </div>
                        <div class="form-group">
                            <label>السنة</label>
                            <select name="car_years" id="car-years" class="form-control" >
                            </select>
                        </div>
                        <div class="form-group">
                            <label>الماركة </label>
                            <select name="car_makes" id="car-makes" class="form-control" >
                            </select>
                        </div>
                        <div class="form-group">
                            <label>الموديل</label>
                            <select name="car_models" id="car-models" class="form-control" >
                            </select>
                        </div>
                        <div class="form-group">
                            <label>اللون الخارجي </label>
                            <input type="text" name="upcar_outcolor" class="form-control" placeholder="اللون الخارجي" />
                        </div>
                        <div class="form-group">
                            <label>الوصف</label>
                            <textarea class="form-control" name="upcar_desc" placeholder="وصف الاعلان " required></textarea>
                        </div>
                        <p class="h3 ad-title form-group">
                            الكماليات 
                        </p>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="فتحة" name="camal" />
                            فتحة
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="شاشة" name="camal1" />
                            شاشة
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="سي دي" name="camal2" />
                            سي دي
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="كاميرا" name="camal3" />
                            كاميرا
                        </div>
                        <div class="clearfix visible-xs "></div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="زنون" name="camal4" />
                            زنون
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="نفجيشن" name="camal5" />
                            نفجيشن
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="تكييف" name="camal6" />
                            تكييف
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="بانوراما" name="camal7" />
                            بانوراما
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="جناح" name="camal8" />
                            جناح
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value=" شبك حماية خارجي" name="camal17" />
                            شبك حماية خارجي
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value=" تشغيل بدون مفتاح" name="camal18" />
                            تشغيل بدون مفتاح
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>اللون الداخلي </label>
                            <input type="text" name="upcar_incolor" class="form-control"  placeholder="اللون الداخلي" />
                        </div>
                        <div class="form-group">
                            <label>الجير </label>
                            <select name="upcar_gear" class="form-control" >
                                <option value="عادي">عادي</option>
                                <option value="أوتوماتيك">أوتوماتيك </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>دبل جير </label>
                            <select name="upcar_gear-type" class="form-control" >
                            <option value="لا">لا</option>
                            <option value="نعم">نعم </option>
                            </select>
                        </div>
                        <div class="clearfix visible-xs "></div>
                        <div class="form-group">
                            <label>عدد الأبواب</label>
                            <select name="cq_doors" id="cq-doors" class="form-control" >
                            </select>
                        </div>
                        <div class="form-group">
                            <label>نوع الوقود </label>
                            <select name="upcar_fuel" class="form-control" >
                            <option value="بنزين">بنزين </option>
                            <option value="مازوت">مازوت  </option>
                            <option value="غاز">غاز </option>
                            <option value="كيروسين">كيروسين  </option>
                            <option value="اخرى">اخرى</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>حالة الإطارات </label>
                                <select name="upcar_wheel" class="form-control" >
                                <option value="جيدة">جيدة </option>
                                <option value="متهالكة">متهالكة </option>
                                <option value="متوسطة">متوسطة </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>القسم  </label>
                            <select name="upcar_category" class="form-control" required>
                                @foreach($cats as $cat)
                                    <option value="{{$cat->id}}">{{$cat->catname}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>شرط الفحص </label>
                            <select name="upcar_check" class="form-control" >
                            <option value="نعم">نعم </option>
                            <option value="لا">لا</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>حجم الماكينة </label>
                            <select name="upcar_csize" class="form-control" >
                            <option value="6 سلندر">6 سلندر</option>
                            <option value="4 سلندر">4 سلندر </option>
                            <option value="5 سلندر">5 سلندر</option>
                            <option value="8 سلندر">8 سلندر</option>
                            <option value="10 سلندر">10 سلندر</option>
                            <option value="12 سلندر">12 سلندر</option>
                            <option value="16 سلندر">16 سلندر</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>تحت الكفالة </label>
                            <select name="upcar_guar" class="form-control" >
                            <option value="لا">لا</option>
                            <option value="نعم">نعم </option>
                            </select>
                        </div>
                        <div class="clearfix visible-xs "></div>
                        <div class="form-group">
                            <label>انتهاء الدفتر </label>
                            <input name="upcar_end" type="text" class="form-control " />
                        </div>
                        <div class="form-group">
                            <label>مواصفات </label>
                            <select name="upcar_spec" class="form-control" >
                                <option value="خليجية">خليجية </option>
                                <option value="أمريكية">أمريكية</option>
                                <option value="أوروبية">أوروبية</option>
                                <option value="أخرى">أخرى</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>الحالة  </label>
                            <select name="upcar_status" class="form-control" >
                            <option value="جيدة">جيدة  </option>
                            <option value="جديدة">جديدة </option>
                            <option value="متهالكة">متهالكة </option>
                            <option value="سكراب">سكراب </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>حساب المستخدم   </label>
                            <select name="user_id" class="form-control" >
                               @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->username}}</option>
                                @endforeach
                            </select>
                        </div>
                        </br></br></br></br>
                        <p class="h3 ad-title form-group">
                           
                        </p></br></br></br>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="رنقات" name="camal9" />
                            رنقات
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="تبريد كراسي" name="camal10" />
                            تبريد كراسي
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="تدفئة كراسي" name="camal11" />
                            تدفئة كراسي
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="شاشات خلفية" name="camal12" />
                            شاشات خلفية
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="كاربون فايبر" name="camal13" />
                            كاربون فايبر
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="تحكم سكان " name="camal14" />
                            تحكم سكان 
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="النقطة العمياء" name="camal15" />
                            النقطة العمياء
                        </div>
                        <div class="form-group col-md-3 col-xs-3">
                            <input type="checkbox" value="حماية زجاج" name="camal16" />
                            حماية زجاج
                        </div>
                        
                    </div>  
                    <div class="form-group col-md-12 col-xs-12">
                        <input type="submit" value="اضافة الاعلان" name="upcar_submit" class="btn btn-success btn-block" id="up-car" />
                    </div>
                </div>
                <div id="english" class="tab-pane fade">
                     
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('public/assets/js/carsQuery.js') }}"></script>
<script type="text/javascript" src="http://www.carqueryapi.com/js/carquery.0.3.4.js"></script>
@endsection