@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">اعلانات محدوفة</span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <table id="demo-dynamic-tables-2" class="table table-middle">
                <thead>
                    <tr>
                    <th>
                        <label class="custom-control custom-control-primary custom-checkbox">
                        <input id="checkAll" class="custom-control-input" type="checkbox">
                        <span class="custom-control-indicator"></span>
                        </label>
                    </th>
                    <th>معاينة</th>
                    <th>العنوان</th>
                    <th>السعر</th>
                    <th>القسم</th>
                    <th>المعلن</th>
                    <th>المحافظة</th>
                    <th>المنطقة</th>
                    <th>الهاتف</th>
                    <th>الاضافة</th>
                    <th>الحالة</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                @if($classcars)
                    @foreach($classcars as $car)
                        <tr>
                            <td></td>
                            <td><img src="{{url('public/assets/img/video')}}/{{$car->upcar_video}}" width="30"/></td>
                            <td>{{$car->upcar_title}}</td>
                            <td>{{$car->upcar_price}}</td>
                            <td>{{$car->getCategory->catname}}</td>
                            <td>{{$car->getuser->username}}</td>
                            <td>{{$car->upcar_gov}}</td>
                            <td>{{$car->upcar_city}}</td>
                            <td>{{$car->upcar_fstphone}}</td>
                            <td>{{ Carbon\Carbon::parse($car->created_at)->format('d-m-Y ') }}</td>
                            <td>
                                @if($car->statys==-3)
                                    <label class="label label-danger label-pill">مرفوضة</label>
                                @endif
                            </td>
                            <td>
                                <div class="btn-group pull-right dropdown">
                                <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                    <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href=""  class="btn-link goldorders" >تفاصيل الاعلان</a></li>
                                    <li><a href="#" id="{{$car->id}}" class="btn-link publierads" title="retoreAds">استعادة الاعلان</a></li>
                                </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection