@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">لوحة التحكم </span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <div class="col-md-6 col-lg-3 col-lg-push-0">
              <div class="card bg-primary">
                <div class="card-body">
                  <div class="media">
                    <div class="media-middle media-left">
                      <span class="bg-primary-inverse circle sq-48">
                        <span class="icon icon-user"></span>
                      </span>
                    </div>
                    <div class="media-middle media-body">
                      <h6 class="media-heading">العملاء الناشطين</h6>
                      <h3 class="media-heading">
                        <span class="fw-l">0.000</span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 col-lg-push-3">
              <div class="card bg-info">
                <div class="card-body">
                  <div class="media">
                    <div class="media-middle media-left">
                      <span class="bg-info-inverse circle sq-48">
                        <span class="icon icon-shopping-bag"></span>
                      </span>
                    </div>
                    <div class="media-middle media-body">
                      <h6 class="media-heading">العملاء الناشطين</h6>
                      <h3 class="media-heading">
                        <span class="fw-l">0.000</span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 col-lg-pull-3">
              <div class="card bg-info">
                <div class="card-body">
                  <div class="media">
                    <div class="media-middle media-left">
                      <span class="bg-info-inverse circle sq-48">
                        <span class="icon icon-clock-o"></span>
                      </span>
                    </div>
                    <div class="media-middle media-body">
                      <h6 class="media-heading">العملاء الناشطين</h6>
                      <h3 class="media-heading">
                        <span class="fw-l">0.000</span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-3 col-lg-pull-0">
              <div class="card bg-warning">
                <div class="card-body">
                  <div class="media">
                    <div class="media-middle media-left">
                      <span class="bg-warning-inverse circle sq-48">
                        <span class="icon icon-usd"></span>
                      </span>
                    </div>
                    <div class="media-middle media-body">
                      <h6 class="media-heading">العملاء الناشطين</h6>
                      <h3 class="media-heading">
                        <span class="fw-l">0.000</span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div> 
        <div class="row gutter-xs">
            <div class="col-xs-12 col-md-6">
              <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                    <table id="demo-dynamic-tables-2" class="table table-middle nowrap">
                      <thead>
                        <tr>
                          <th>الصورة</th>
                          <th>رقم العميل</th>
                          <th>الحالة</th>
                        </tr>
                      </thead>
                        <tr>
                          <td>f</td>
                          <td>0677821730</td>
                          <td>متصل الان</td>
                        </tr>
                      <tbody>
                      </tbody>
                    </table>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"> 1200 حساب مسجل من بينها 1200 حساب نشيط </h4>
                </div>
                <div class="card-body">
                  <div class="card-chart">		
                    <canvas data-chart="bar" data-animation="false" data-labels='["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]' data-values='[{"label": "This week", "backgroundColor": "#d9230f", "borderColor": "#d9230f", "data": [3089, 2132, 1854, 2326, 3274, 3679, 3075]}, {"label": "Last week", "backgroundColor": "#d9831f", "borderColor": "#d9831f", "data": [983, 2232, 3057, 2238, 1613, 2194, 3874]}]' data-tooltips='{"mode": "label"}' data-hide='["gridLinesX", "legend"]' height="150"></canvas>
                  </div>
                </div>
              </div>
            </div>
        </div>
  </div><!--end-->
</div>
@endsection