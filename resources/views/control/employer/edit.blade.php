@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib"> تعديل معلومات الموظف  </span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{URL::to('control/employer')}}" class="btn btn-success pull-right">قائمة الموظفين</a>
            </br>
             @if ($message = Session::get('success'))
                <div class="alert-success success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#arabic"> <i class="flag-icon flag-icon-sa"></i> العربية</a></li>
                <li><a data-toggle="tab" href="#english"> <i class="flag-icon flag-icon-us"></i> الانجليزية</a></li>
            </ul>
            {!! Form::model($work, [
                    'method' => 'PATCH',
                    'route' => ['employer.update', $work->id],
                    'class' => 'form','data-toggle' => 'validator','files' => true
                    ]) !!}
            <div class="tab-content">
                <div id="arabic" class="tab-pane fade in active">
                    <h3></h3>
                        <div class="form-group">
                                {!! Form::label('اسم الموظف') !!}
                                {!! Form::text('empname', null, 
                                    array('required','class'=>'form-control','placeholder'=>'اسم الموظف ')) !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label('رتبة الموظف ') !!}
                                {!! Form::text('emprole', null, 
                                    array('required','class'=>'form-control','placeholder'=>'رتبة الموظف ')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('اسم الشركة') !!}
                             {!! Form::select('company_id',$company,1, ['required','class'=>'form-control']); !!}
                        </div>
                        <div class="form-group">
                                {!! Form::label(' صورة الموظف ') !!}
                                {!! Form::file('empphoto', null, 
                                    array('required','class'=>'form-control','placeholder'=>'صورة الموظف ')) !!}
                        </div>
                </div>
                <div id="english" class="tab-pane fade">
                     <h3></h3>
                    <div class="form-group">
                            {!! Form::label('اسم الموظف') !!}
                            {!! Form::text('empname_en', null, 
                                array('required','class'=>'form-control','placeholder'=>'اسم الموظف ')) !!}
                    </div>
                    <div class="form-group">
                            {!! Form::label('رتبة الموظف ') !!}
                            {!! Form::text('emprole_en', null, 
                                array('required','class'=>'form-control','placeholder'=>'رتبة الموظف ')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::submit('تحديث المعلومات ', 
                        array('class'=>'btn btn-primary btn-block')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection