@extends('control.layouts.navbar')
@section('navbar')
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">كل الموظفين  </span>
            </h1>
        </div>
        <div class="row gutter-xs">
            <a href="{{url('control/employer/create')}}" class="btn btn-success">موظف جديد</a>
            </br>
            <table id="demo-dynamic-tables-2" class="table table-middle">
                <thead>
                    <tr>
                    <th>صورة الموظف</th>
                    <th>اسم الموظف</th>
                    <th>رتبة الموظف</th>
                    <th>اسم الشركة</th>
                    <th>تاريخ الاضافة</th>
                    <th>الحالة</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($workers as $work)
                        <tr>
                            <td>
                             <img class="rounded" width="40" height="40" src="{{url('public/assets/img/')}}/{{$work->empphoto}}" alt=""/>
                            </td>
                            <td>{{ $work->empname}}</td>
                            <td>{{ $work->emprole}}</td>
                            <td>{{$work->companie->compname}}</td>
                            <td>{{ Carbon\Carbon::parse($work->created_at)->format('d-m-Y ') }}</td>
                            <td>
                                @if($work->status==0)
                                    <label class="label label-success label-pill">منشور</label>
                                @else
                                    <label class="label label-primary label-pill">تم ايقافه</label>
                                 @endif
                            </td>
                            <td>
                                <div class="btn-group pull-right dropdown">
                                <button class="btn btn-link link-muted" aria-haspopup="true" data-toggle="dropdown" type="button">
                                    <span class="icon icon-ellipsis-h icon-lg icon-fw"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{route('employer.edit',$work->id)}}"  class="btn-link" >تعديل المعلومات</a></li>
                                    <li><a href="#"  class="btn-link">حدف الموظف</a></li>
                                </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection