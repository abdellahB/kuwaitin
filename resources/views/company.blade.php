@extends('layouts.app')

@section('header')
    @include('includes.sliderads')
    @include('includes.search')
    @include('includes.feature')
@endsection
@section('content')
<div class="col-md-8 col-xs-12 site-content"><!---start site-content-->
    <div class="col-xs-12" id="main-servise"><!---start main-servise-->
        <p class="h1 ad-title">{!! $cats->catname !!}</p>
        
        @foreach($companies as $company)
            <div class="col-md-6 col-xs-12 comp-block">
                <div class="comp-b-img col-xs-5">
                    <img src="{{url('public/assets/img/')}}/{{$company->complogo}}">
                </div>
                <div class="comp-bl-cont col-xs-7">
                    <a href="{{url('companyprofile',$company->compslug)}}">
                        <p class="h5"><strong>{{$company->compname}}</strong></p>
                        <p class="h6">{{$company->compworking}}</p>
                        <p class="h6">
                            <i class="fa fa-map-marker"></i>
                           {{$company->complace}}
                        </p>
                    </a>
                </div>
            </div><!---end comp-block-->   
        @endforeach

        <div class="col-xs-12 comp-ads-more">
            <a href="#" class="cli-btn blue-btn">
                المزيد
            </a>
        </div>
    </div><!---end main-servise-->                    
</div><!---end site-content -->

@include('includes.sidebar')
@endsection