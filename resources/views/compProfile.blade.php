@extends('layouts.app')
@section('header')
    @include('includes.sliderads')
    @include('includes.search')
@endsection
@section('content')
  <div class="col-md-8 col-xs-12 site-content usPro"><!---start site-content , usPro-->
    <div class="col-xs-12 edit-form">
        <form class="col-xs-12 form" action="" method="POST" enctype="multipart/form-data">
            <p class="h2 ad-title">
                <i class="fa fa-edit"></i>
                تعديل الملف الشخصي :
            </p>
            <ul class="edit-cont">
                <li class="col-xs-12 usimg">
                    <div class="form-group">
                        <input type="file" name="photo" id="chUsImg" onchange="loadFile(event)" />
                        <img src="{{url('public/assets/img')}}/{{$companies->photo}}" class="user-img" id="cuUsImg2" />
                        <p class="h4  img-cam">
                            <i class="fa fa-camera"></i>
                        </p>
                        <p class="h5 text-center">الصورة الشخصية</p>
                    </div>
                
                </li>
                    <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">اسم المستخدم :</label>
                        <input type="text" class="form-control col-md-9 col-xs-12" name="username" value="{{$companies->username}}" disabled />
                    </div>
                    </li>
                <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">اسم الدخول :</label>
                        <input type="text" class="form-control col-md-9 col-xs-12" name="phone" value="{{$companies->phone}}" disabled />
                    </div>
                    </li>
                    <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">عن الشركة:</label>
                        <textarea class="form-control col-md-9 col-xs-12" rows="3" name="compabout" placeholder="اكتب نبذة عن الشركة">
                          {{$companies->compabout}}
                        </textarea>
                    </div>
                    </li>
                    <li class="col-xs-12">
                        <div class="form-group">
                            <label class="col-md-3 col-xs-6">كلمة المرور :</label>
                            <label class="col-md-6 col-xs-12 hidden-xs">.............</label>
                            <label class="col-md-3 col-xs-6">
                                <a data-toggle="tab"  href="#chPass">
                                    <i class="fa fa-pencil"></i>
                                تعديل
                                </a>
                            </label>
                        </div>
                    </li>
                <li class="col-xs-12 tab-pane fade" id="chPass">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">كلمة المرور الجديدة :</label>
                        <input type="password" class="form-control col-md-9 col-xs-12" name="password"   />
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">تأكيد كلمة المرور :</label>
                        <input type="password" class="form-control col-md-9 col-xs-12" name="confirm"  />
                    </div>
                    </li>
                <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">العنوان :</label>
                        <input type="text" class="form-control col-md-9 col-xs-12" placeholder="عنوان الشركة" name="comptitle"  value="{{$companies->comptitle}}"/>
                    </div>
                    </li>
                    <li class="col-xs-12">
                    <div class="">
                        <div class="form-group">
                            <label class="col-md-3 col-xs-6">هاتف 1 :</label>
                            <input type="text" class="form-control col-md-3 col-xs-6" value="{{$companies->comphone1}}" name="comphone1" />
                        </div>
                    </div>
                    <div class="">
                        <div class="form-group">
                            <label class="col-md-3 col-xs-6">هاتف 2 :</label>
                            <input type="text" class="form-control col-md-3 col-xs-6" value="{{$companies->comphone2}}" name="comphone2" />
                        </div>
                    </div>
                    </li>
                <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">البريد الإلكتروني :</label>
                        <input type="email" class="form-control col-md-9 col-xs-12" name="compemail" placeholder="xxxxxx@xxxxxxx.com" value="{{$companies->compemail}}" />
                    </div>
                    </li>
                <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">الموقع الإلكتروني :</label>
                        <input type="url" class="form-control col-md-9 col-xs-12" name="compwebsite"  placeholder="www.xxxxxx.com" value="{{$companies->compwebsite}}"  />
                    </div>
                    </li>
                <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">الخدمات:</label>
                        <textarea class="form-control col-md-9 col-xs-12" rows="3" name="" placeholder="اكتب خدمات الشركة"></textarea>
                    </div>
                    </li>
                <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">
                            <i class="fa fa-share-alt"></i>
                            <strong>سوشيال ميديا :</strong>
                        </label>
                        <div class="col-md-9 col-xs-12 offe-block">
                            <div class="form-group">
                                <label class="col-md-5 col-xs-12">
                                    <i class="fa fa-facebook-square"></i>
                                    فيسبوك :</label>
                                <input type="text" class="form-control col-md-7 col-xs-12" name="compfacebook" value="{{$companies->compfacebook}}" />
                            </div>
                            <div class="form-group">
                                <label class="col-md-5 col-xs-12">
                                    <i class="fa fa-twitter-square"></i>
                                    تويتر :</label>
                                <input type="text" class="form-control col-md-7 col-xs-12" name="comptwitter" value="{{$companies->comptwitter}}"/>
                            </div>
                            <div class="form-group">
                                <label class="col-md-5 col-xs-12">
                                    <i class="fa fa-youtube-square"></i>
                                    يوتيوب :</label>
                                <input type="text" class="form-control col-md-7 col-xs-12" name="compyoutube" value="{{$companies->compyoutube}}"/>
                            </div>
                            <div class="form-group">
                                <label class="col-md-5 col-xs-12">
                                    <i class="fa fa-google-plus-square"></i>
                                    جوجل بلس :</label>
                                <input type="text" class="form-control col-md-7 col-xs-12" name="compgoogleplus" value="{{$companies->compgoogleplus}}" />
                            </div>
                            <div class="form-group">
                                <label class="col-md-5 col-xs-12">
                                    <i class="fa fa-instagram"></i>
                                    انستغرام :</label>
                                <input type="text" class="form-control col-md-7 col-xs-12" name="compinstagram" value="{{$companies->compinstagram}}"/>
                            </div>
                        </div>
                    </div>
                
                </li>
                <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">
                            <i class="fa fa-clock-o"></i>
                            <strong>اوقات العمل :</strong>
                        </label>
                        <div class="col-md-9 col-xs-12 offe-block job-days">
                            <div class="form-group job-cont">
                                <div class="col-md-3 col-xs-12">
                                    <input type="checkbox" value="" name="" />
                                    <label>السبت :</label>
                                    </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" class="form-control col-xs-6" name=""  placeholder="من 1-3" />
                                    <div class="col-xs-6">
                                        <select name="" class="form-control">
                                            <option value="">صباحاً</option>
                                            <option value="">مساءاً</option>
                                        
                                        </select>
                                        
                                    </div>
                                </div>
                            </div><!--end job-cont-->
                            <div class="form-group job-cont">
                                <div class="col-md-3 col-xs-12">
                                    <input type="checkbox" value="" name="" />
                                    <label>الأحد :</label>
                                    </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" class="form-control col-xs-6" name=""  placeholder="من 1-3" />
                                    <div class="col-xs-6">
                                        <select name="" class="form-control">
                                            <option value="">صباحاً</option>
                                            <option value="">مساءاً</option>
                                        
                                        </select>
                                        
                                    </div>
                                </div>
                            </div><!--end job-cont-->
                            <div class="form-group job-cont">
                                <div class="col-md-3 col-xs-12">
                                    <input type="checkbox" value="" name="" />
                                    <label>الأثنين :</label>
                                    </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" class="form-control col-xs-6" name=""  placeholder="من 1-3" />
                                    <div class="col-xs-6">
                                        <select name="" class="form-control">
                                            <option value="">صباحاً</option>
                                            <option value="">مساءاً</option>
                                        
                                        </select>
                                        
                                    </div>
                                </div>
                            </div><!--end job-cont-->
                            <div class="form-group job-cont">
                                <div class="col-md-3 col-xs-12">
                                    <input type="checkbox" value="" name="" />
                                    <label>الثلاثاء :</label>
                                    </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" class="form-control col-xs-6" name=""  placeholder="من 1-3" />
                                    <div class="col-xs-6">
                                        <select name="" class="form-control">
                                            <option value="">صباحاً</option>
                                            <option value="">مساءاً</option>
                                        </select>
                                    </div>
                                </div>
                            </div><!--end job-cont-->
                            <div class="form-group job-cont">
                                <div class="col-md-3 col-xs-12">
                                    <input type="checkbox" value="" name="" />
                                    <label>الأربعاء :</label>
                                    </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" class="form-control col-xs-6" name=""  placeholder="من 1-3" />
                                    <div class="col-xs-6">
                                        <select name="" class="form-control">
                                            <option value="">صباحاً</option>
                                            <option value="">مساءاً</option>
                                        </select>
                                    </div>
                                </div>
                            </div><!--end job-cont-->
                            <div class="form-group job-cont">
                                <div class="col-md-3 col-xs-12">
                                    <input type="checkbox" value="" name="" />
                                    <label>الخميس :</label>
                                    </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" class="form-control col-xs-6" name=""  placeholder="من 1-3" />
                                    <div class="col-xs-6">
                                        <select name="" class="form-control">
                                            <option value="">صباحاً</option>
                                            <option value="">مساءاً</option>
                                        </select>
                                    </div>
                                </div>
                            </div><!--end job-cont-->
                            <div class="form-group job-cont">
                                <div class="col-md-3 col-xs-12">
                                    <input type="checkbox" value="" name="" />
                                    <label>الجمعة :</label>
                                    </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" class="form-control col-xs-6" name=""  placeholder="من 1-3" />
                                    <div class="col-xs-6">
                                        <select name="" class="form-control">
                                            <option value="">صباحاً</option>
                                            <option value="">مساءاً</option>
                                        </select>
                                    </div>
                                </div>
                            </div><!--end job-cont-->
                        </div>
                    </div>
            </li>
            <div class="form-group">
                <input type="submit" value="تحديث" name="" class="btn green-btn" />
            </div>
            </ul><!--end edit-cont-->
        </form>
        <ul class="edit-cont">
            <li class="col-xs-12">
                <label class="col-xs-12">
                    <i class="fa fa-users"></i>
                        <strong>الكادر الوظيفي :</strong>
                </label>    
            </li>
            <li class="col-xs-12">
            
                @foreach ($companies->workers as $worker)
                    <div class="col-xs-12 clearfix compslider" id="comp-em-slider2">
                        <div class="com-em-item">
                            <div class="com-em-img">
                                <img src="{{url('public/assets/img')}}/{{ $worker->empphoto}}" />
                            </div>
                            <div class="com-em-info">
                                <p class="h4"><strong>{{ $worker->empname}}</strong></p>
                                <p class="h5">{{ $worker->emprole}}</p>
                            </div>
                            <div class="edit-inf">
                                <a href="{{url('editemployer')}}" class="green-text">
                                    <i class="fa fa-pencil"></i>
                                    تعديل
                                </a>
                                <a href="{{url('deleteemployer')}}" class="red-text">
                                    <i class="fa fa-close"></i>
                                    حذف
                                </a>
                            </div>
                        </div><!---end com-em-item -->
                 @endforeach
            
            </li>
            <li class="col-xs-12">
                <div class="form-group">
                    <label class="col-md-3 col-xs-12">
                        <strong>
                            تنبيهات العروض
                            <span class="badge">4</span>
                            :
                        </strong>
                    </label>
                    <div class="col-md-9 col-xs-12 offe-block">
                        <div class="col-xs-12">
                            <a href="#">هناك عرض سعر جديد عللى إعلانك "عنوان الاعلان يكتب هنا"</a>
                        </div>
                        <div class="col-xs-12">
                            <a href="#">هناك عرض سعر جديد عللى إعلانك "عنوان الاعلان يكتب هنا"</a>
                        </div>
                        <div class="col-xs-12">
                            <a href="#">هناك عرض سعر جديد عللى إعلانك "عنوان الاعلان يكتب هنا"</a>
                        </div>
                        <div class="col-xs-12">
                            <a href="#">هناك عرض سعر جديد عللى إعلانك "عنوان الاعلان يكتب هنا"</a>
                        </div>
                    </div>
                </div>
            </li>
            <!-- favourite-->
            <li class="col-xs-12">
                <div class="form-group">
                    <label class=" col-xs-12 ad-title">
                        <strong>مفضلتي :</strong>
                    </label>
                    <div class="col-xs-12">
                        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
                            <div class="hovereffect featured">
                                <a href="#" class="feat-img">
                                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                                </a>
                                    <div class="overlay col-xs-12">
                                        <div class="col-xs-12 feat-title">
                                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                                        </div>

                                        <div class="col-xs-6">
                                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                                1234
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3" >
                                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="ui share">
                                                <i class="fa fa-share-alt"></i>
                                            </div>
                                            <div class="ui share-menu flowing popup top center transition hidden">
                                                <div class="share-list">
                                                    <div class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-whatsapp"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div><!---end feat-block -->
                        
                        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
                            <div class="hovereffect featured">
                                <a href="#" class="feat-img">
                                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                                </a>
                                    <div class="overlay col-xs-12">
                                        <div class="col-xs-12 feat-title">
                                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                                        </div>

                                        <div class="col-xs-6">
                                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                                1234
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3" >
                                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="ui share">
                                                <i class="fa fa-share-alt"></i>
                                            </div>
                                            <div class="ui share-menu flowing popup top center transition hidden">
                                                <div class="share-list">
                                                    <div class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-whatsapp"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div><!---end feat-block -->
                        
                        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
                            <div class="hovereffect featured">
                                <a href="#" class="feat-img">
                                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                                </a>
                                    <div class="overlay col-xs-12">
                                        <div class="col-xs-12 feat-title">
                                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                                        </div>

                                        <div class="col-xs-6">
                                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                                1234
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3" >
                                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="ui share">
                                                <i class="fa fa-share-alt"></i>
                                            </div>
                                            <div class="ui share-menu flowing popup top center transition hidden">
                                                <div class="share-list">
                                                    <div class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-whatsapp"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div><!---end feat-block -->
                        
                        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
                            <div class="hovereffect featured">
                                <a href="#" class="feat-img">
                                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                                </a>
                                    <div class="overlay col-xs-12">
                                        <div class="col-xs-12 feat-title">
                                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                                        </div>

                                        <div class="col-xs-6">
                                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                                1234
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3" >
                                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="ui share">
                                                <i class="fa fa-share-alt"></i>
                                            </div>
                                            <div class="ui share-menu flowing popup top center transition hidden">
                                                <div class="share-list">
                                                    <div class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-whatsapp"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div><!---end feat-block -->
                        
                        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
                            <div class="hovereffect featured">
                                <a href="#" class="feat-img">
                                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                                </a>
                                    <div class="overlay col-xs-12">
                                        <div class="col-xs-12 feat-title">
                                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                                        </div>

                                        <div class="col-xs-6">
                                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                                1234
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3" >
                                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="ui share">
                                                <i class="fa fa-share-alt"></i>
                                            </div>
                                            <div class="ui share-menu flowing popup top center transition hidden">
                                                <div class="share-list">
                                                    <div class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-whatsapp"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div><!---end feat-block -->
                        
                    
                        
                    
                    </div>
                </div>
            
                <div class="col-xs-12 comp-ads-more">
                    <a href="#" class="cli-btn blue-btn">المزيد</a>
                </div>
            </li>
        <!-- favourite-->
        <!-- my ads-->
        <li class="col-xs-12">
                <div class="form-group">
                    <label class=" col-xs-12 ad-title">
                        <strong>إعلاناتي :</strong>
                    </label>
                    <div class="col-xs-12">
                        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
                            <div class="hovereffect featured">
                                <a href="#" class="feat-img">
                                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                                </a>
                                    <div class="overlay col-xs-12">
                                        <div class="col-xs-12 feat-title">
                                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                                        </div>

                                        <div class="col-xs-6">
                                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                                1234
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3" >
                                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="ui share">
                                                <i class="fa fa-share-alt"></i>
                                            </div>
                                            <div class="ui share-menu flowing popup top center transition hidden">
                                                <div class="share-list">
                                                    <div class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-whatsapp"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div><!---end feat-block -->
                        
                        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
                            <div class="hovereffect featured">
                                <a href="#" class="feat-img">
                                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                                </a>
                                    <div class="overlay col-xs-12">
                                        <div class="col-xs-12 feat-title">
                                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                                        </div>

                                        <div class="col-xs-6">
                                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                                1234
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3" >
                                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="ui share">
                                                <i class="fa fa-share-alt"></i>
                                            </div>
                                            <div class="ui share-menu flowing popup top center transition hidden">
                                                <div class="share-list">
                                                    <div class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-whatsapp"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div><!---end feat-block -->
                        
                        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
                            <div class="hovereffect featured">
                                <a href="#" class="feat-img">
                                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                                </a>
                                    <div class="overlay col-xs-12">
                                        <div class="col-xs-12 feat-title">
                                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                                        </div>

                                        <div class="col-xs-6">
                                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                                1234
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3" >
                                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="ui share">
                                                <i class="fa fa-share-alt"></i>
                                            </div>
                                            <div class="ui share-menu flowing popup top center transition hidden">
                                                <div class="share-list">
                                                    <div class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-whatsapp"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div><!---end feat-block -->
                        
                        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
                            <div class="hovereffect featured">
                                <a href="#" class="feat-img">
                                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                                </a>
                                    <div class="overlay col-xs-12">
                                        <div class="col-xs-12 feat-title">
                                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                                        </div>

                                        <div class="col-xs-6">
                                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                                1234
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3" >
                                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="ui share">
                                                <i class="fa fa-share-alt"></i>
                                            </div>
                                            <div class="ui share-menu flowing popup top center transition hidden">
                                                <div class="share-list">
                                                    <div class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-whatsapp"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div><!---end feat-block -->
                        
                        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
                            <div class="hovereffect featured">
                                <a href="#" class="feat-img">
                                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                                </a>
                                    <div class="overlay col-xs-12">
                                        <div class="col-xs-12 feat-title">
                                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                                        </div>

                                        <div class="col-xs-6">
                                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                                1234
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3" >
                                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="ui share">
                                                <i class="fa fa-share-alt"></i>
                                            </div>
                                            <div class="ui share-menu flowing popup top center transition hidden">
                                                <div class="share-list">
                                                    <div class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-facebook"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-whatsapp"></i>
                                                        </a>
                                                    </div>
                                                    <div  class="share-item">
                                                        <a href="#">
                                                            <i class="fa fa-twitter"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div><!---end feat-block -->
                    </div>
                </div>
            
                <div class="col-xs-12 comp-ads-more">
                    <a href="#" class="cli-btn blue-btn">المزيد</a>
                </div>
            </li>
        <!-- my ads-->
        </ul><!--end edit-cont-->
    </div>
</div><!---end site-content , usPro -->	
 @include('includes.sidebar')
@endsection