@extends('layouts.app')

@section('header')
    @include('includes.sliderads')
    @include('includes.search')
    @include('includes.feature')
@endsection
@section('content')
<div class="col-md-8 col-xs-12 site-content usPro"><!---start site-content , usPro-->
    <div class="col-xs-12 edit-form">
        <div class="col-xs-12 ">
            <p class="h2 ad-title">
                <!--<i class="fa fa-envelope"></i>-->
              الشروط و الأحكام 
            </p>
            <p>
                @foreach($terms as $terms)
                    {!! $terms->terms !!}
                @endforeach
            </p>
        
        </div>
    </div>
</div><!---end site-content , usPro -->
@include('includes.sidebar')
@endsection