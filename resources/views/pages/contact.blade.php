@extends('layouts.app')

@section('header')
    @include('includes.sliderads')
    @include('includes.search')
    @include('includes.feature')
@endsection
@section('content')
<div class="col-md-8 col-xs-12 site-content usPro"><!---start site-content , usPro-->
    <div class="col-xs-12 edit-form">
        <form class="col-xs-12 form" action="" method="" >
            <p class="h2 ad-title">
                <i class="fa fa-envelope"></i>
                اتصل بنا :
            </p>
            <ul class="edit-cont">
                    <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">الاسم  :</label>
                        <input type="text" name="" class="form-control col-md-9 col-xs-12 inputUser" placeholder="من فضلك اكتب اسمك" value="" />
                        <div class="alert alert-danger col-md-12 col-xs-12">
                            الاسم لا يجب ان يقل عن 3 حروف
                        </div>
                    </div>
                    </li>
                    
                    <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">الهاتف / الايميل:</label>
                        <input type="text" name="" class="form-control col-md-9 col-xs-12 inputEmail" placeholder="من فضلك اكتب الهاتف / الايميل" value="" />
                        <div class="alert alert-danger col-md-12 col-xs-12">
                            من فضلك اكتب الايميل / الهاتف
                        </div>
                    </div>
                    </li>
                    
                    <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">الرسالة :</label>
                        <textarea class="form-control col-md-9 col-xs-12 inputEmail" rows="3" placeholder="الرسالة"></textarea>
                        <div class="alert alert-danger col-md-12 col-xs-12">
                            الرسالة قصيرة جدا
                        </div>
                    </div>
                    </li>
                    <div class="form-group">
                        <input type="submit" value="ارسال" name="" class="btn green-btn" />
                    </div>
            </ul><!--end edit-cont-->
        
        </form>
    </div>
</div><!---end site-content , usPro -->
					
    @include('includes.sidebar')
@endsection