@extends('layouts.app')

@section('header')
    @include('includes.sliderads')
@endsection
@section('content')
<div class="col-md-8 col-xs-12 site-content">
    <!---start site-content-->
    <div class="offers">
        @foreach($offers as $offer)
            @if($offer->offertype==2)
                <div class="col-md-3 col-xs-6 feat-block sec"><!---start feat-block -->
                    <div class="hovereffect featured">
                        <a href="{{url('public/assets/img')}}/{{$offer->offerimage}}" class="feat-img ad-view">
                            <img class="img-responsive" src="{{url('public/assets/img')}}/{{$offer->offerimage}}" alt="">
                        </a>
                        <div class="overlay col-xs-12">
                            <div class="col-xs-6">
                                <a href="#">
                                    <span>{!! $offer->countview !!}</span>
                                    <i class="fa fa-eye"></i>
                                </a>
                            </div>
                            <div class="col-xs-3">
                                <a href="{{url('public/assets/img')}}/{{$offer->offerimage}}" class="ad-view">
                                    <i class="fa fa-search-plus"></i>
                                </a>
                            </div>
                            <div class="col-xs-3">
                                <a href="{{$offer->offerurl}}">
                                    <i class="fa fa-link"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-md-3 col-xs-6 feat-block sec">
                    <!--start feat-block -->
                    <div class="hovereffect">
                        <a href="{{url('public/assets/img')}}/{{$offer->offerimage}}" class="feat-img ad-view">
                            <img class="img-responsive" src="{{url('public/assets/img')}}/{{$offer->offerimage}}" alt="">
                        </a>
                        <div class="overlay col-xs-12">
                            <div class="col-xs-6">
                                <a href="#">
                                    <span>{!! $offer->countview !!}</span>
                                    <i class="fa fa-eye"></i>
                                </a>
                            </div>
                            <div class="col-xs-3">
                                <a href="{{url('public/assets/img')}}/{{$offer->offerimage}}" class="ad-view">
                                    <i class="fa fa-search-plus"></i>
                                </a>
                            </div>
                            <div class="col-xs-3">
                                <a href="{{$offer->offerurl}}">
                                    <i class="fa fa-link"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <!---end feat-block -->
            @endif
        @endforeach   
    </div>
</div>
@section('offers')
<div class="col-xs-12 side-block ">
    <form action="" method="" class="col-xs-12">
        <div class="col-xs-6">
            <input type="text" placeholder="ابحث عن" name="" class="form-control" />
        </div>
        <div class="col-xs-6">
            <select name="" class="ui fluid normal dropdown">
                <option value="1">قسم</option>
                <option value="2">قسم</option>
                <option value="3">قسم</option>
                <option value="4">قسم</option>
                <option value="5">قسم</option>
                <option value="6">قسم</option>
                <option value="7">قسم</option>
                <option value="8">قسم</option>
                <option value="9">قسم</option>
                <option value="10">قسم</option>
                <option value="11">قسم</option>
                <option value="12">قسم</option>
                <option value="13">قسم</option>
            </select>
        </div>
        <div class="col-xs-6">
            <select name="" class="ui fluid normal dropdown">
                <option value="1">المكان</option>
                <option value="2">المكان</option>
                <option value="3">المكان</option>
                <option value="4">المكان</option>
                <option value="5">المكان</option>
                <option value="6">المكان</option>
                <option value="7">المكان</option>
                <option value="8">المكان</option>
                <option value="9">المكان</option>
                <option value="10">المكان</option>
                <option value="11">المكان</option>
                <option value="12">المكان</option>
                <option value="13">المكان</option>
            </select>
        </div>
        <div class="col-xs-6">
            <select name="" class="ui fluid normal dropdown">
                <option value="1">اخر اسبوع</option>
                <option value="1">اخر يوم</option>
                <option value="2">اخر شهر</option>
                <option value="3">اخر سنة</option>
            </select>
        </div>
        <div class="col-xs-6">
            <input type="text" placeholder="السعر الادنى" name="" class="form-control" />
        </div>
        <div class="col-xs-6">
            <input type="text" placeholder="السعر الاعلى" name="" class="form-control" />
        </div>
        <div class="col-xs-12 search-btn">
            <button type="submit"  name="" class="side-search side-btn green-btn btn-block" >
                <i class="fa fa-search"></i>
                <span>بحث</span>
            </button>
        </div>
    </form><!---end form -->
</div><!--end side-block-->
@endsection
<!---end site-content -->			
@include('includes.sidebar')
@endsection