@extends('layouts.app')
@section('header')
    @include('includes.sliderads')
@endsection
@section('content')
<div class="col-xs-12 cat-links"><!---start cat-links-->
    <p class="h3 view-ad-ti ">عنوان الاعلان يكتب هنا</p>
    <ul class=" ad-title">
        <li><a href="#" class="h5">الرئيسية</a> <span>> </span> </li>
        <li><a href="#" class="h5">القسم</a> <span>> </span></li>
        <li><a href="#" class="h5">عنوان الاعلان</a></li>
    </ul>
</div><!---end cat-links -->
<div class="col-md-8 col-xs-12 site-content view-ads-content"><!---start site-content , view-ads-content -->
    
    <div id="ad-view-gallery" class="col-xs-12"><!---start ad-view-gallery -->
        <ul class="view-img">
            <li data-thumb="img/person-apple-laptop-notebook.jpg" 
                    data-src="img/person-apple-laptop-notebook.jpg" class="ad-gallery-item">
            <img src="img/person-apple-laptop-notebook.jpg" class="img-responsive" />
            </li>
            <li data-thumb="img/5.jpg" 
                    data-src="img/5.jpg" class="ad-gallery-item">
            <img src="img/5.jpg" class="img-responsive" />
            </li>
            <li data-thumb="img/def-video.jpg"  class=" def-video">
            <video class="lg-video-object lg-html5" controls >
                <source src="img/2.MP4" type="video/mp4">
                HTML5 video متصفحك لا يدعم 
            </video>
            </li>
            
        </ul>
    
    </div><!---end ad-view-gallery--> 
    <div class="col-xs-12 ad-view-contText h4"><!---start  ad-view-contText-->
        هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء
    </div><!---end  ad-view-contText-->
    <div class="col-xs-12 ad-viw-info"><!---start  ad-viw-infoe-->
        <div class="col-xs-12 ad-view-address">
            <p class="h4"><strong>العنوان :</strong></p>
            <p class="h4">هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ</p>
        </div>
    </div><!---end  ad-viw-info-->
    <div class="col-xs-12 ad-view-Finfo"><!---start  ad-view-Finfo-->
        <table class="table table-bordered">
            <tr>
            <td class="view-price">
                <i class="fa fa-tags"></i>
                <span>01233445</span>
                <span>د.ك</span>
            </td> 
            <td>
                <i class="fa fa-clock-o"></i>
                <span>12/03/2017</span>
                <span>15:45</span>
            </td> 
            <td>
                <i class="fa fa-eye"></i>
                <span>012345</span>
            </td>  
            </tr>
        </table>
    </div><!---end  ad-view-Finfo-->
    <div class="col-md-7 col-xs-12 ad-view-author"><!---start  ad-view-author-->
        <div class="ad-view-auth-img">
            <img src="img/def-user.jpg"  />
        </div>
        <div class="ad-viw-auth-info">
            <a href="#">اسم صاحب الاعلان</a>
            <p class="h5">اسم صاحب الاعلان</p>
        </div>
        
    </div><!---end ad-view-author-->
        <div class="col-md-5 col-xs-12 ad-viw-auth-phone">
            <a href="tel:0000000" class="blue-btn">
                <i class="fa fa-phone"></i>
                <span>00000000000</span>
            </a>
        </div><!---end ad-viw-auth-phone-->
    <div class="col-xs-12 add-share">
        <div class="col-xs-3 add-share-fav">
            <a href="#"><i class="fa fa-heart"></i></a>
        </div>
        <div class="col-xs-3 add-share-fac">
            <a href="#"><i class="fa fa-facebook"></i></a>
        </div>
        <div class="col-xs-3 add-share-twit">
            <a href="#"><i class="fa fa-twitter"></i></a>
        </div>
        <div class="col-xs-3 add-share-what">
            <a href="#"><i class="fa fa-whatsapp"></i></a>
        </div>
    
    </div><!---end add-share-->
    <div class="col-xs-12 add-offer">
                <div class="col-md-6 col-xs-12">
            <a class="red-btn add-v" href="#" >
                <i class="fa fa-object-group"></i>
                مقارنة
            </a>
        </div>
            <div class="col-md-6 col-xs-12">
                <a class="blue-btn add-ofer-tab" data-toggle="tab"  href="#add-offer-det">
                    <i class="fa fa-money"></i>
                    قدم عرض
                </a>
            </div>
        <div id="add-offer-det" class="tab-pane fade">
            <form class="form-inline col-md-7 col-xs-12" action="" method="">
                <div class="input-group">
                
                <input type="number" class="form-control" name="offer-cont" placeholder="اضف عرض">
                    <span class="input-group-addon">د.ك</span>
                </div>
                <input type="submit" name="add-new-off" value="اضف"  class="green-btn" />
            </form>
            <div class="col-md-5 col-xs-12 orig-offr">
                <label>السعر المطلوب :</label>
                <label>1123456</label>
                <label>د.ك</label>
            </div>
        </div>
        
        
    </div><!---end add-offer-->
    <div class="col-xs-12">
        <p class="h2 text-center" style="padding: 20px 0;">إعلانات مشابهة :</p>
        <div class="col-xs-12">
            
        <div class="col-md-4 col-xs-12 feat-block same-item"><!---start feat-block -->
            <div class="hovereffect">
                <a href="#" class="feat-img">
                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>

                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                1234
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!---end feat-block -->
            
        <div class="col-md-4 col-xs-12 feat-block same-item"><!---start feat-block -->
            <div class="hovereffect">
                <a href="#" class="feat-img">
                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>

                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                1234
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!---end feat-block -->
            
        <div class="col-md-4 col-xs-12 feat-block same-item"><!---start feat-block -->
            <div class="hovereffect">
                <a href="#" class="feat-img">
                    <img class="img-responsive" src="img/300%20SQR.jpg" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>

                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                1234
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!---end feat-block -->
        
        </div>
    </div>
</div><!---end site-content , view-ads-content --> 
    @include('includes.sidebar')
@endsection
