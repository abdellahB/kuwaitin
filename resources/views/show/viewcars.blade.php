@extends('layouts.app')
@section('header')
    @include('includes.sliderads')
@endsection
@section('content')
<div class="col-xs-12 cat-links"><!---start cat-links-->
    <p class="h3 view-ad-ti ">{{$classcar->upcar_title}}</p>
    <ul class=" ad-title">
        <li><a href="#" class="h5">الرئيسية</a> <span>> </span> </li>
        <li><a href="#" class="h5">{{$classcar->getCategory->catname}}</a> <span>> </span></li>
        <li><a href="#" class="h5">{{$classcar->upcar_title}}</a></li>
    </ul>
</div><!---end cat-links -->
<!---start site-content , view-ads-content -->
<div class="col-md-8 col-xs-12 site-content view-ads-content">
    <div id="ad-view-gallery" class="col-xs-12"><!---start ad-view-gallery -->
        <ul class="view-img">
            <li data-thumb="{{url('public/assets/img/person-apple-laptop-notebook.jpg')}}" 
                    data-src="{{url('public/assets/img/person-apple-laptop-notebook.jpg')}}" class="ad-gallery-item">
            <img src="{{url('public/assets/img/person-apple-laptop-notebook.jpg')}}" class="img-responsive" />
            </li>
            <li data-thumb="{{url('public/assets/img/5.jpg')}}" 
                    data-src="{{url('public/assets/img/5.jpg')}}" class="ad-gallery-item">
            <img src="{{url('public/assets/img/5.jpg')}}" class="img-responsive" />
            </li>
            <li data-thumb="{{url('public/assets/img/def-video.jpg')}}"  class=" def-video">
            <video class="lg-video-object lg-html5" controls >
                <source src="{{url('public/assets/img/2.MP4')}}" type="video/mp4">
                HTML5 video متصفحك لا يدعم 
            </video>
            </li>
        </ul>
    </div><!---end ad-view-gallery--> 
    <div class="col-xs-12 ad-view-contText h4"><!---start  ad-view-contText-->
       {{$classcar->upcar_desc}}
    </div><!---end  ad-view-contText-->
    <div class="col-xs-12 ad-viw-info"><!---start  ad-viw-infoe-->
        <div class="col-xs-12 ad-view-address">
            <p class="h4"><strong>العنوان :</strong></p>
            <p class="h4">
            {{$classcar->upcar_title}}
            </p>
        </div>
        <table class="table">
            <tbody>
                <tr>
                    <th>الماركة :</th>
                    <td> {{$classcar->car_makes}}</td>
                    <th>الموديل :</th>
                    <td> {{$classcar->car_models}}</td>
                </tr>
                <tr>
                    <th>السنة :</th>
                    <td> {{$classcar->car_years}}</td>
                    <th>اللون الخارجي :</th>
                    <td> {{$classcar->upcar_outcolor}}</td>
                </tr>
                <tr>
                    <th>اللون الداخلي :</th>
                    <td> {{$classcar->upcar_incolor}}</td>
                    <th>حجم  الرنقات :</th>
                    <td> {{$classcar->upcar_size}}</td>
                </tr>
                <tr>
                    <th>الجير  :</th>
                    <td> {{$classcar->upcar_gear}}</td>
                    <th>دبل جير :</th>
                    <td> {{ $classcar->upcar_gear}}</td>
                </tr>
                <tr>
                    <th>المسافة المقطوعة :</th>
                    <td> {{$classcar->upcar_dist}}</td>
                    <th>عدد الأبواب :</th>
                    <td> {{$classcar->cq_doors}}</td>
                </tr>
                <tr>
                    <th>نوع الوقود :</th>
                    <td> {{$classcar->upcar_fuel}}</td>
                    <th>حالة الإطارات :</th>
                    <td> {{$classcar->upcar_wheel}}</td>
                </tr>
                <tr>
                    <th>شرط الفحص :</th>
                    <td> {{$classcar->upcar_check}}</td>
                    <th>حجم الماكينة :</th>
                    <td> {{$classcar->upcar_csize}}</td>
                </tr>
                <tr>
                    <th>تحت الكفالة :</th>
                    <td> {{$classcar->upcar_guar}}</td>
                    <th>انتهاء الدفتر :</th>
                    <td> {{$classcar->upcar_end}}</td>
                </tr>
                <tr>
                    <th>مواصفات :</th>
                    <td> {{$classcar->upcar_spec}}</td>
                    <th>الحالة :</th>
                    <td> {{$classcar->upcar_status}}</td>
                </tr>
                </tbody>
        </table>
    </div><!---end  ad-viw-info-->
    <div class="col-xs-12 ad-view-more h5"><!---start  ad-view-more-->
        <div class="col-xs-12 vi-more">
            <ul>
                <li>فتحة</li>
                <li>شاشة</li>
                <li>سي دي</li>
                <li>كاميرا</li>
                <li>زنون</li>
                <li>تدفئة كراسي</li>
                <li>تبريد كراسي</li>
                <li>شاشات خلفية</li>
                <li>نفجيشن</li>
                <li>تكييف</li>
                <li>تحكم سكان</li>
                <li>بانوراما</li>
                <li>النقطة العمياء</li>
                <li>تشغيل بدون مفتاح</li>
                <li>جناح</li>
                <li>شبك حماية خارجي</li>
                <li>كاربون فايبر</li>
                <li>حماية زجاج</li>
                <li>رنقات</li>
            </ul>
        </div>
    </div><!---end  ad-view-more-->
    <div class="col-xs-12 ad-view-Finfo"><!---start  ad-view-Finfo-->
        <table class="table table-bordered">
            <tr>
            <td class="view-price">
                <i class="fa fa-tags"></i>
                <span>{{$classcar->upcar_price}}</span>
                <span>د.ك</span>
            </td> 
            <td>
                <i class="fa fa-clock-o"></i>
                <span>{{$classcar->created_at}}</span>
            </td> 
            <td>
                <i class="fa fa-eye"></i>
                <span>012345</span>
            </td>  
            </tr>
        </table>
    </div><!---end  ad-view-Finfo-->
    <div class="col-md-7 col-xs-12 ad-view-author"><!---start  ad-view-author-->
        <div class="ad-view-auth-img">
            <img src="{{url('public/assets/img')}}/{{$classcar->getuser->photo}}"  />
        </div>
        <div class="ad-viw-auth-info">
            <a href="#">{{$classcar->getuser->username}}</a>
            <p class="h5">{{$classcar->getuser->username}}</p>
        </div>
        
    </div><!---end ad-view-author-->
        <div class="col-md-5 col-xs-12 ad-viw-auth-phone">
            <a href="tel:0000000" class="blue-btn">
                <i class="fa fa-phone"></i>
                <span>{{$classcar->getuser->phone}}</span>
            </a>
        </div><!---end ad-viw-auth-phone-->
    <div class="col-xs-12 add-share">
        <div class="col-xs-3 add-share-fav">
            <a href="#"><i class="fa fa-heart"></i></a>
        </div>
        <div class="col-xs-3 add-share-fac">
            <a href=""><i class="fa fa-facebook"></i></a>
        </div>
        <div class="col-xs-3 add-share-twit">
            <a href="#"><i class="fa fa-twitter"></i></a>
        </div>
        <div class="col-xs-3 add-share-what">
            <a href="#"><i class="fa fa-whatsapp"></i></a>
        </div>
    
    </div><!---end add-share-->
    <div class="col-xs-12 add-offer">
                <div class="col-md-6 col-xs-12">
            <a class="red-btn add-v" href="#" >
                <i class="fa fa-object-group"></i>
                مقارنة
            </a>
        </div>
            <div class="col-md-6 col-xs-12">
                <a class="blue-btn add-ofer-tab" data-toggle="tab"  href="#add-offer-det">
                    <i class="fa fa-money"></i>
                    قدم عرض
                </a>
            </div>
        <div id="add-offer-det" class="tab-pane fade">
            <form class="form-inline col-md-7 col-xs-12" action="" method="">
                <div class="input-group">
                
                <input type="number" class="form-control" name="offer-cont" placeholder="اضف عرض">
                    <span class="input-group-addon">د.ك</span>
                </div>
                <input type="submit" name="add-new-off" value="اضف"  class="green-btn" />
            </form>
            <div class="col-md-5 col-xs-12 orig-offr">
                <label>السعر المطلوب :</label>
                <label>{{$classcar->upcar_price}}</label>
                <label>د.ك</label>
            </div>
        </div>
    </div><!---end add-offer-->
    <div class="col-xs-12">
        <p class="h2 text-center" style="padding:20px 0;">إعلانات مشابهة :</p>
        <div class="col-xs-12">
        <?php $count = 0; ?>
         @foreach($classcar->getCategory as $relatedclasscars)   
            <?php if($count == 3) break; ?>
                <div class="col-md-4 col-xs-12 feat-block same-item"><!---start feat-block -->
                    <div class="hovereffect">
                        <a href="#" class="feat-img">
                            <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                        </a>
                        <div class="overlay col-xs-12">
                            <div class="col-xs-12 feat-title">
                                <a href="#">
                                     *******
                                </a>
                            </div>
                            <div class="col-xs-6">
                                <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                    1234
                                    <i class="fa fa-eye"></i>
                                </a>
                            </div>
                            <div class="col-xs-3" >
                                <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                    <i class="fa fa-heart"></i>
                                </a>
                            </div>
                            <div class="col-xs-3">
                                <div class="ui share">
                                    <i class="fa fa-share-alt"></i>
                                </div>
                                <div class="ui share-menu flowing popup top center transition hidden">
                                    <div class="share-list">
                                        <div class="share-item">
                                            <a href="#">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </div>
                                        <div  class="share-item">
                                            <a href="#">
                                                <i class="fa fa-whatsapp"></i>
                                            </a>
                                        </div>
                                        <div  class="share-item">
                                            <a href="#">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!---end feat-block -->
                <?php $count++; ?>
            @endforeach
        
        </div>
    </div>
</div><!---end site-content , view-ads-content --> 
    @include('includes.sidebar')
@endsection
