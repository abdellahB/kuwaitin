@extends('layouts.app')

@section('header')
    @include('includes.sliderads')
    @include('includes.search')
    @include('includes.feature')
@endsection
@section('content')
<div class="col-md-8 col-xs-12 site-content"><!---start site-content-->
    <div class="col-xs-12" id="main-servise"><!---start main-servise-->
        @foreach($cats as $cat)
            <div class="col-md-3 col-sm-4 col-xs-4 serv-block comp-g"><!---start serv-block -->
                <a href="{{url('companytype',$cat->title)}}" class="serv-cont">
                    <div class="h1 {{$cat->catcolor}}-text">
                         <i class="{{$cat->catflag}}"></i>
                    </div>
                    <div class="serv-title">
                        <p class="h4">{{$cat->catname}}</p>
                    </div>
                </a>
            </div><!---end serv-block -->
        @endforeach  
    </div><!---end main-servise-->                    
</div><!---end site-content -->

@include('includes.sidebar')
@endsection