@extends('layouts.app')

@section('content')
<p class="h1 ad-title text-center">اختر نوع الاعلان</p>
<div class="col-xs-12 site-content"><!---start site-content-->
    <div class="col-xs-12" id="main-servise"><!---start main-servise-->
        <div class="col-md-3 col-sm-4 col-xs-4 serv-block pink-gr"><!---start serv-block -->
            <a href="{{url('addcar/create')}}" class="serv-cont">
                <div class="serv-icon">
                    <img src="{{url('public/assets/img/ic5-02.png')}}"  alt="" >
                </div>
                <div class="serv-title">
                    <p class="h4">حراج السيارات</p>
                </div>
            </a>
        </div><!---end serv-block -->
        <div class="col-md-3 col-sm-4 col-xs-4 serv-block green-gr"><!---start serv-block -->
            <a href="{{url('addbuild/create')}}" class="serv-cont">
                <div class="serv-icon">
                    <img src="{{url('public/assets/img/ic6-01.png')}}"  alt="" >
                </div>
                <div class="serv-title">
                    <p class="h4">عقارات الكويت</p>
                </div>
            </a>
        </div><!---end serv-block -->
        <div class="col-md-3 col-sm-4 col-xs-4 serv-block red-gr"><!---start serv-block -->
            <a href="{{url('addelectronic/create')}}" class="serv-cont">
                <div class="serv-icon">
                    <img src="{{url('public/assets/img/ic11-01.png')}}"  alt="" >
                </div>
                <div class="serv-title">
                    <p class="h4">الكترونيات</p>
                </div>
            </a>
        </div><!---end serv-block -->
            <div class="col-md-3 col-sm-4 col-xs-4 serv-block blue-gr"><!---start serv-block -->
            <a href="{{url('addjobs/create')}}" class="serv-cont">
                <div class="serv-icon">
                    <img src="{{url('public/assets/img/ic12-01.png')}}"  alt="" >
                </div>
                <div class="serv-title">
                    <p class="h4">وظائف الكويت</p>
                </div>
            </a>
        </div><!---end serv-block -->
        <div class="col-md-3 col-sm-4 col-xs-4 serv-block yellow-gr"><!---start serv-block -->
            <a href="{{url('addother/create')}}" class="serv-cont">
                <div class="serv-icon">
                    <img src="{{url('public/assets/img/ic15-01.png')}}"  alt="" >
                </div>
                <div class="serv-title">
                    <p class="h4">خدمات سكراب</p>
                </div>
            </a>
        </div><!---end serv-block -->
        <div class="col-md-3 col-sm-4 col-xs-4 serv-block green-gr"><!---start serv-block -->
            <a href="{{url('addother/create')}}" class="serv-cont">
                <div class="serv-icon">
                    <img src="{{url('public/assets/img/ic14-01.png')}}"  alt="" >
                </div>
                <div class="serv-title">
                    <p class="h4">خدمات منزلية</p>
                </div>
            </a>
        </div><!---end serv-block -->
        <div class="col-md-3 col-sm-4 col-xs-4 serv-block red-gr"><!---start serv-block -->
            <a href="{{url('addother/create')}}" class="serv-cont">
                <div class="serv-icon">
                    <img src="{{url('public/assets/img/ic13-01.png')}}"  alt="" >
                </div>
                <div class="serv-title">
                    <p class="h4">أعمال حرة</p>
                </div>
            </a>
        </div><!---end serv-block -->
        <div class="col-md-3 col-sm-4 col-xs-4 serv-block pink-gr"><!---start serv-block -->
            <a href="{{url('addother/create')}}" class="serv-cont">
                <div class="serv-icon">
                    <img src="{{url('public/assets/img/ic16-01.png')}}"  alt="" >
                </div>
                <div class="serv-title">
                    <p class="h4">اخرى</p>
                </div>
            </a>
        </div><!---end serv-block -->
    </div><!---end main-servise-->
</div><!---end site-content -->
@endsection