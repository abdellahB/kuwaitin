@extends('layouts.app')
@section('header')
    @include('includes.sliderads')
    @include('includes.search')
@endsection
@section('content')
	<div class="col-md-8 col-xs-12 site-content usPro">
    <!---start site-content , usPro-->
    <ul class="edit-cont view-cont">
        <li class="col-xs-12 usimg">
            <div class="form-group">
                <img src="{{url('public/assets/img')}}/{{$user->photo}}" class="user-img" id="cuUsImg" />
                <p class="h4 text-center" style="margin-top:30px; font-weight:700;">{{$user->username}}</p>
                <p class="h4 text-center" style="margin-top:20px;">
                    <i class="fa fa-mobile"></i>
                    <a href="tel:{{$user->phone}}">{{$user->phone}}</a>
                </p>
            </div>
        </li>
        <!-- my ads-->
        <li class="col-xs-12">
            <div class="form-group">
                <label class=" h3 col-xs-12 ad-title text-center">
                    <strong>إعلاناتي :</strong>
                </label>
                <div class="col-xs-12 user-ads">
                @if($classcars)
                @foreach($classcars as $cars)
                     <!---start feat-block -->
                    <div class="col-md-4 col-xs-6 feat-block">
                        <div class="hovereffect @if($cars->statys==2) featured @endif">
                            <a href="{{route('addcar.show',$cars->id)}}" class="feat-img">
                                <img class="img-responsive" src="{{url('public/assets/img/video')}}/{{$cars->upcar_video}}" alt="">
                            </a>
                            <div class="overlay col-xs-12">
                                <div class="col-xs-12 feat-title">
                                    <a href="{{route('addcar.show',$cars->id)}}">
                                     {{$cars->upcar_title}}
                                    </a>
                                </div>

                                <div class="col-xs-6">
                                    <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                        1234
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                        <i class="fa fa-heart"></i>
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <div class="ui share">
                                        <i class="fa fa-share-alt"></i>
                                    </div>
                                    <div class="ui share-menu flowing popup top center transition hidden">
                                        <div class="share-list">
                                            <div class="share-item">
                                                <a href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </div>
                                            <div class="share-item">
                                                <a href="#">
                                                    <i class="fa fa-whatsapp"></i>
                                                </a>
                                            </div>
                                            <div class="share-item">
                                                <a href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xs-12">
                                    <a class="red-btn cancel btn-block sold-btn" disabled>مباع</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach 
                @endif
                    <!---end feat-block -->
                    <!--<div class="col-md-4 col-xs-6 feat-block">
                        
                        <div class="hovereffect featured sold">
                            <a href="#" class="feat-img">
                                <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                            </a>
                            <div class="overlay col-xs-12">
                                <div class="col-xs-12 feat-title">
                                    <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                                </div>

                                <div class="col-xs-6">
                                    <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                        1234
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                        <i class="fa fa-heart"></i>
                                    </a>
                                </div>
                                <div class="col-xs-3">
                                    <div class="ui share">
                                        <i class="fa fa-share-alt"></i>
                                    </div>
                                    <div class="ui share-menu flowing popup top center transition hidden">
                                        <div class="share-list">
                                            <div class="share-item">
                                                <a href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </div>
                                            <div class="share-item">
                                                <a href="#">
                                                    <i class="fa fa-whatsapp"></i>
                                                </a>
                                            </div>
                                            <div class="share-item">
                                                <a href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xs-12">
                                    <a href="#" class="red-btn cancel btn-block">تم البيع</a>
                                </div>
                            </div>
                        </div>
                    </div>-->
                   
                </div>
            </div>
            <div class="col-xs-12 comp-ads-more">
                <a href="#" class="cli-btn blue-btn">المزيد</a>
            </div>
        </li>
        <!-- my ads-->
    </ul>
    <!--end edit-cont-->
</div>
<!---end site-content , usPro -->		
 @include('includes.sidebar')
@endsection