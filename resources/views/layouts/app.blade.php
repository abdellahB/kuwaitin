<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'kuwaitin') }}</title>
    <!-- Styles -->
    @if( LaravelLocalization::getCurrentLocaleDirection()=='rtl')
        <link href="{{asset('public/assets/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet" />
        <link href="{{asset('public/assets/css/bootstrap-rtl.min.css')}}" type="text/css" rel="stylesheet" />
    @else
        <link href="{{asset('public/assets/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet" />
        <link href="{{ asset('public/css/all_en.css') }}" rel="stylesheet">
    @endif
    <link href="{{asset('public/assets/css/font-awesome.min.css')}}" type="text/css" rel="stylesheet" />
    <link href="{{ asset('public/css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/css/uploadfile.css') }}" type="text/css" rel="stylesheet" />
    @yield('styles')
</head>
<body>
        <section id="header"><!---Start header section -->
            <div class="container">
                <div class="row topbar"><!---Start topbar-->
                    <div class="col-lg-3 col-sm-4 col-xs-12 logo">
                        <a href="{{url('/')}}">
                             <img src="{{url('public/assets/img/')}}/{{$setup->sitelogo}}" class="img-responsive" />
                        </a>
                    </div><!---end logo-->
                    <div class="col-lg-9 col-sm-8 col-xs-12 log">
                        <ul class="@if( LaravelLocalization::getCurrentLocaleDirection()=='rtl') navbar-left @else navbar-right @endif top-links h5">
                            <div class="colored-limks">
                                <li class="ad">
                                    <a href="{{url('addads')}}" class="blue-btn">
                                        <i class="fa fa-plus"></i>
                                        @lang('homepage.addAds')
                                    </a>
                                </li>
                                 @if (Auth::guest())
                                <div class="lang-links">
                                <li>
                                    <a href="{{url('login')}}" class="">
                                        <i class="fa fa-compress green-btn"></i>
                                        <span class="hidden-xs"> @lang('homepage.login')</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('register')}}" class="">
                                        <i class="fa fa-user-plus red-btn"></i>
                                        <span class="hidden-xs"> @lang('homepage.register')</span>
                                    </a>
                                </li>
                                @else
                                <li>
                                    <a href="" class="">
                                    </a>
                                </li>
                                <li class="profile">
                                <div class="dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                      {{Auth::user()->username}}  <i class="fa fa-user"></i>
                                    </div>
                                    <div class="dropdown-menu">
                                        @if(Auth::user()->usertype =="user")
                                            <div class="item">
                                                <a href="{{url('profile')}}">
                                                    <i class="fa fa-gear"></i>
                                                    <span> @lang('homepage.profile')</span>
                                                </a>
                                            </div>
                                        @else
                                            <div class="item">
                                                <a href="{{url('compProfile')}}">
                                                    <i class="fa fa-gear"></i>
                                                    <span> @lang('homepage.profile')</span>
                                                </a>
                                            </div>
                                            <div class="item">
                                                <a href="{{url('employer')}}">
                                                    <i class="fa fa-plus"></i>
                                                    <span> @lang('homepage.employer')</span>
                                                </a>
                                            </div>
                                        @endif
                                        <div class="item">
                                            <a href="{{url('myads')}}">
                                                <i class="fa fa-mail-forward"></i>
                                                <span>@lang('homepage.myads')</span>
                                            </a>
                                        </div>
                                        <!--<div class="item">
                                            <a href="#">
                                                <i class="fa fa-envelope"></i>
                                                <span>الرسائل</span>
                                            </a>
                                        </div>-->
                                        <div class="item">
                                            <a href="{{URL::to('logout')}}">
                                                <i class="fa fa-unlink"></i>
                                                <span>@lang('homepage.logout')</span>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </li>
                            @endif
                             @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <li>
                                    <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                       @if($properties['native']=="English")
                                            <img src="{{url('public/assets/img/usa.jpg')}}" />
                                       @else
                                            <img src="{{url('public/assets/img/sa.png')}}" />
                                       @endif
                                    </a>
                                </li>
                                @endforeach
                            </div>
                            </div>
                        </ul>
                    </div><!---end log -->
                </div> <!---end topbar -->
            </div>
        </section><!---end header section -->
        <section id="navi-bar"><!---start navi-bar section -->
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-bar" aria-expanded="false">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>


                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="main-nav-bar">
                      <ul class="nav navbar-nav">
                        <li><a href="{{url('/')}}">@lang('homepage.home')</a></li>
                        <li><a href="#">عروض الكويت</a></li>
                        <li><a href="#">أخبار </a></li>
                        <li><a href="{{url('addcar')}}">حراج السيارات</a></li>
                        <li><a href="{{url('addbuild')}}">عقارات الكويت</a></li>
                        <li><a href="{{url('addelectronic')}}">الكترونيات</a></li>
                        <li><a href="{{url('addjobs')}}">وظائف الكويت</a></li>
                        <li><a href="{{url('scrap')}}">خدمات سكراب</a></li>
                        <li><a href="{{url('house')}}">خدمات منزلية</a></li>
                        <li><a href="{{url('others')}}">خدمات أخرى</a></li>
                      </ul>

                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
        </section><!---end navi-bar section -->
        @yield('header')
        <section class="main-content"><!---start main-content section -->
            <div class="container">
                <div class="row">
                    @yield("content")
                </div>
            </div>
        </section><!---end main-content section -->

        <footer><!---start footer section -->
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-xs-12 footer-block">
                        <ul>
                            <li><a href="{{url('terms')}}">@lang('homepage.terms')</a></li>
                            <li><a href="{{url('privacy')}}">@lang('homepage.privacy')</a></li>
                            <li><a href="{{url('about')}}">@lang('homepage.about')</a></li>
                            <li><a href="{{url('contact')}}">@lang('homepage.contact')</a></li>
                            <li><a class="top" href="#header">@lang('homepage.top')</a></li>
                        </ul>
                    </div><!---end footer-block -->
                    <div class="col-md-4 col-xs-12 footer-block foot-ser">
                        <ul>
                            <li><a href="#">قسم</a></li>
                            <li><a href="#">قسم</a></li>
                            <li><a href="#">قسم</a></li>
                            <li><a href="#">قسم</a></li>
                            <li><a href="#">قسم</a></li>
                            <li><a href="#">قسم</a></li>
                            <li><a href="#">قسم</a></li>
                            <li><a href="#">قسم</a></li>
                            <li><a href="#">قسم</a></li>
                            <li><a href="#">قسم</a></li>
                        </ul>
                    </div><!--end footer-block -->
                    <div class="col-md-4 col-xs-12 footer-block">
                        <ul class="social">
                            <li class="btn-block">
                                <a>
                                   @lang('homepage.followus')
                                </a>
                            </li>
                            <li>
                                <a href="{{$setup->site_facebook}}">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$setup->site_instagram}}">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$setup->site_twitter}}">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$setup->site_youtube}}">
                                    <i class="fa fa-youtube"></i>
                                </a>
                            </li>
                            <li class="btn-block">
                                <a>
                                    تصميم : ----------
                                </a>
                            </li>
                            <li class="btn-block">
                                <a>
                                    برمجة : ----------
                                </a>
                            </li>
                        </ul>
                    </div><!---end footer-block -->
                </div>
            </div>
            <p class="h5 rights text-center">
                @lang('homepage.rights')
                &copy;
                2017
            </p>
        </footer><!---end footer section -->
    <!-- Scripts -->
    <script src="{{ asset('public/js/all.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/js/jquery.uploadfile.min.js') }}"></script>
    @yield('scripts')
</body>
</html>
