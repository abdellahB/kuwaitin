@extends('layouts.app')

@section('header')
    @include('includes.sliderads')
    @include('includes.search')
    @include('includes.feature')
@endsection
@section('content')
<div class="col-md-8 col-xs-12 site-content"><!---start site-content-->
    <div class="col-xs-12 adsIc" id="main-servise"><!---start main-servise-->
        <div class="col-xs-12 main-signup">
            <p class="h1 red-text">
                <i class="fa fa-user-plus"></i>
                انشاء حساب
            </p>
            <div class="divid"></div>
            <form action="{{ route('register') }}" method="POST" class="form">
                 {{ csrf_field() }}
                <div class="col-xs-12">
                    <div class="form-group ori{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <input type="text" name="phone" class="form-control input-lg inputPhone" placeholder="رقم الجوال (اجباري)" value="{{ old('phone') }}" requird />
                        <i class="fa fa-mobile"></i>
                        @if ($errors->has('phone'))
                            <div class="alert-danger">
                                <strong> من فضلك ادخل رقم جوال صحيح</strong>
                            </div>
                         @endif
                    </div>
                    <div class="form-group ori{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" name="password" class="form-control input-lg inputPass" placeholder="كلمة المرور (اجباري)"
                                value="{{ old('password') }}" required/>
                        <i class="fa fa-lock "></i>
                        @if ($errors->has('password'))
                            <div class="alert-danger">
                               <strong> من فضلك ادخل كلمة المرور</strong>
                            </div>
                        @endif
                    </div>
                    <div class="form-group ori{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" name="password_confirmation" class="form-control input-lg inputCPass" placeholder="تأكيد كلمة المرور (اجباري)"
                                value="{{ old('password') }}" required/>
                        <i class="fa fa-lock "></i>
                        @if ($errors->has('password'))
                        <div class="alert-danger">
                          <strong>من فضلك أعد إدخال كلمة المرور للتأكيد </strong>
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="text" name="username" class="form-control input-lg " placeholder="اسم المستخدم (اختياري)" value="{{ old('username') }}" />
                        <i class="fa fa-user "></i>
                    </div>
                    <div class="form-group">
                        <input type="email" name="emailuser" class="form-control input-lg " value="{{ old('emailuser') }}" placeholder=" الايميل (اختياري)" required/>
                        <i class="fa fa-envelope "></i>
                        @if ($errors->has('emailuser'))
                            <div class="alert-danger">
                               <strong>هدا الايميل مستخدم مسبقا</strong>
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>صورة شخصية</label>
                        <input type="file" name="photo" class="form-control input-lg" required />
                    </div>
                    <div class="form-group">
                        <label>النوع</label>
                        <select name="sexe" class="form-control">
                            <option value="m">ذكر</option>
                            <option value="f">انثى</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <input type="submit" name="" class="btn green-btn" value="تسجيل" />
                    </div>
                </div>
                <div class="col-xs-12 h5">
                    <a href="{{url('login')}}" class=" blue-text">امتلك حساب بالفعل </a>
                </div>
            </form>
        
        </div><!---end main-signup-->     
        
        
    </div><!---end main-servise-->                    
</div><!---end site-content -->
 @include('includes.sidebar')
@endsection
