@extends('layouts.app')

@section('header')
    @include('includes.sliderads')
    @include('includes.search')
    @include('includes.feature')
@endsection
@section('content')
    <div class="col-md-8 col-xs-12 site-content"><!---start site-content-->
        <div class="col-xs-12 adsIc" id="main-servise"><!---start main-servise-->
            <div class="col-xs-12 main-signup">
             @if ($message = Session::get('error'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
              @endif
                <form action="{{ route('login') }}" method="POST" class="tab-pane fade in active form" id="main-signIn">
                    <p class="h1 green-text">
                        <i class="fa fa-sign-in"></i>
                        تسجيل الدخول
                    </p>
                     {{ csrf_field() }}
                    <div class="col-xs-12">
                        <div class="form-group ori{{ $errors->has('phone') ? ' has-error' : '' }} ">
                            <input type="text" name="phone" class="form-control input-lg inputPhone" placeholder="رقم الجوال " value="{{ old('phone') }}" required/>
                            <i class="fa fa-mobile"></i>
                            @if($errors->has('phone') OR $errors->has('password') )
                            <div class="alert-danger">
                                من فضلك ادخل رقم جوال صحيح
                            </div>
                            @endif
                        </div>
                        <div class="form-group ori{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" name="password" class="form-control input-lg inputPass" placeholder="كلمة المرور "
                                    value="{{ old('password') }}" required/>
                            <i class="fa fa-lock "></i>
                            @if ($errors->has('phone') OR $errors->has('password') )
                            <div class="alert-danger">
                                من فضلك ادخل كلمة المرور
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <input type="submit" name="" class="btn blue-btn" value="تسجيل" />
                        </div>
                    </div>
                    <div class="col-xs-12 h5">
                        <p class="h5">
                            <a data-toggle="tab" href="#recover"  class=" red-text">هل نسيت كلمة المرور ؟ (استعادة) </a>
                        </p>
                        <p class="h5">
                            <a href="{{url('register')}}" class=" blue-text">لا أمتلك حساب </a>
                        </p>
                    </div>
                </form>
                <form action="" method="" class=" tab-pane fade form" id="recover">
                    <p class="h1 green-text">
                        <i class="fa fa-rotate-right"></i>
                        استعادة حساب
                    </p>
                    <p class="h5 alert alert-info">
                        ادخل البريد الالكتروني لاستعادة الحساب - ان توفر - أو يمكنك مراسلة السوبر أدمن لاستعادة الحساب
                    </p>
                    <div class="col-xs-12">
                        <div class="form-group ori ">
                            <input type="text" name="" class="form-control input-lg inputPhone" placeholder="الايميل " value="" />
                            <i class="fa fa-mobile"></i>
                            <div class="alert alert-danger">
                                من فضلك ادخل ايميل صحيح
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <input type="submit" name="" class="btn blue-btn" value="استعادة" />
                            <a class="btn red-btn cancel" data-toggle="tab" href="#main-signIn" >إلغاء</a> 
                        </div>
                    </div>
                </form>
            </div><!---end main-signup-->     
        </div><!---end main-servise-->                    
    </div><!---end site-content -->
     @include('includes.sidebar')
@endsection
