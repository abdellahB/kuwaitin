@extends('layouts.app')
@section('header')
    @include('includes.sliderads')
    @include('includes.search')
    @include('includes.feature')
@endsection
@section('content')
    <div class="col-md-8 col-xs-12 site-content"><!---start site-content-->
        <div class="col-xs-12 cat-links categ"><!---start cat-links-->
            <p class="h3 view-ad-ti ">خدمات منزلية
             </p>
            <ul class=" ad-title">
                <li><a href="#" class="h5">الرئيسية</a> <span>> </span> </li>
                <li><a href="#" class="h5">الاقسام</a> <span>> </span></li>
            </ul>
        </div><!---end cat-links -->
        <div class="col-xs-12 adsIc" id="main-servise"><!---start main-servise-->
            @if(!empty($houses))
                @foreach($houses as $hous)
                    <div class="col-md-3 col-sm-4 col-xs-4 serv-block comp-g "><!---start serv-block -->
                        <a href="#" class="serv-cont">
                            @if($hous->catflag)
                                <div class="h1 {{$hous->catcolor}}-text">
                                    <i class="{{$hous->catflag}}"></i>
                                </div>
                            @else
                                <div class="h1 green-text">
                                    <img class="img-resonsive" src="img/adsIc/eur.png" >
                                </div>
                            @endif
                            <div class="serv-title">
                                <p class="h4">{{$hous->catname}}</p>
                            </div>
                        </a>
                    </div><!---end serv-block -->
                @endforeach
            @endif
        </div><!---end main-servise-->                    
    </div><!---end site-content -->
    @include('includes.sidebar')
@endsection
