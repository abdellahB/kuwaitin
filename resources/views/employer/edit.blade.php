@extends('layouts.app')

@section('header')
    @include('includes.sliderads')
    @include('includes.search')
@endsection
@section('content')
<div class="col-md-8 col-xs-12 site-content usPro"><!---start site-content , usPro-->
    <div class="col-xs-12 edit-form">
        <form class="col-xs-12 form" action="" method="PATH" enctype="multipart/form-data">
            <p class="h2 ad-title">
                <i class="fa fa-edit"></i>
                تعديل موظف :
            </p>
            <div class="resault">
                <div class="alert alert-success" >تم التحديث</div>
                <div class="alert alert-danger" >بعض الحقول فارغة</div>
            </div>
            <ul class="edit-cont">
                <li class="col-xs-12 usimg">
                    <div class="form-group">
                        <input type="file" name="" id="chUsImg" onchange="loadFile(event)" />
                        <img src="img/def-user.jpg" class="user-img" id="cuUsImg3" />
                        <p class="h4  img-cam">
                            <i class="fa fa-camera"></i>
                        </p>
                        <p class="h5 text-center">الصورة الشخصية</p>
                    </div>
                </li>
                    <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">أسم الموظف :</label>
                        <input type="text" class="form-control col-md-9 col-xs-12" name="" maxlength="30" value="اسم الموظف بالكامل هنا" placeholder="اسم الموظف بالكامل هنا" />
                    </div>
                    </li>
                <li class="col-xs-12">
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12">رتبة الموظف:</label>
                        <input type="text" class="form-control col-md-9 col-xs-12" name="" maxlength="30" value="رتبة الموظف" placeholder="رتبة الموظف"  />
                    </div>
                    </li>
                    
                    <div class="form-group">
                        <input type="submit" value="تحديث" name="" class="btn green-btn" />
                        <a href="{{url('employer')}}" class="btn cancel  blue-btn">
                        <i class="fa fa-plus"></i>
                            اضافة موظف
                        </a>    
                </div>
            </ul><!--end edit-cont-->
        </form>
    </div>
</div><!---end site-content , usPro -->
    @include('includes.sidebar')
@endsection
