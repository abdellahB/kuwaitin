@extends('layouts.app')
@section('content')
<p class="h1 ad-title">
    <i class="fa fa-bank"></i>
    اضافة اعلان عقارات
</p>
<div class="">
    @if(!$user)
    <span class="alert alert-warning col-md-12">
        لكي تتمكن من نشر اعلانك يجب أولا <a href="{{url('login')}}">تسجيل الدخول</a> الى حسابك أو فتح <a href="{{url('register')}}">حساب جديد</a> ان كنت لا تتوفر على حساب معنا
    </span>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success success col-md-12">
            <p>{{ $message }}</p>
        </div>
    @endif
    <!--upload photos-->
    <div class="clearfix"></div>
    <!--create data-->
    <form id="build" method="post" action="{{route('addbuild.store')}}"
         enctype="multipart/form-data" class="col-xs-12">
        <div class="dropzone" id="mydropzone" name="mainFileUploader">
            <div class="fallback">
                <input name="file" type="file" multiple />
            </div>
        </div>
        <div class="form-group col-md-6  col-xs-12">
            <label>اختر فيديو</label>
            <input name="upbuild_video" type="file" class="form-control car-upvid" />
        </div>
        <div class="form-group col-md-6 col-xs-12">
            <label>عنوان الاعلان</label>
            <input name="upbuild_title" type="text" class="form-control" placeholder="عنوان الاعلان" />
        </div>
        <div class="form-group col-md-12 col-xs-12">
            <label>الوصف</label>
            <textarea class="form-control" name="upbuild_desc" placeholder="الوصف"></textarea>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>السعر</label>
            <input name="upbuild_price" type="number" class="form-control" placeholder="السعر"/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>رقم الهاتف 1</label>
            <input name="upbuild_fstphon" type="number" class="form-control" placeholder="رقم الهاتف 1" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>رقم الهاتف 2</label>
            <input name="upbuild_secphone" type="number" class="form-control" placeholder="رقم الهاتف 2"/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>الدور</label>
            <input name="upbuild_floor" type="number" class="form-control" placeholder="الدور" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المحافظة </label>
            <select name="upbuild_gov" class="form-control upcargov">
            <option value="">اختر المحافظة</option>
            <option value="الكويت">الكويت</option>
            <option value="حولي">حولي</option>
            <option value="الفروانية">الفروانية</option>
            <option value="مبارك الكبير">مبارك الكبير</option>
            <option value="الجهراء">الجهراء</option>
            <option value="الأحمدي">الأحمدي</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المنطقة </label>
            <select name="upbuild_city" class="form-control domains_table" required>
                  @include('includes.city')
            </select>
        </div>
        <div class="form-group col-md-6  col-xs-12">
            <label>اكمال العنوان </label>
            <input name="upbuild_adress" type="text" class="form-control" placeholder="اكتب تفاصيل العنوان : القطعة , الشارع , رقم المنزل / البناية , الدور , رقم الشقة / المكتب , "/>
        </div>
        <div class="form-group col-md-3  col-xs-12">
            <label>المساحة</label>
            <input name="upbuild_size" type="text" class="form-control" placeholder="المساحة" />
        </div>
        <div class="form-group col-md-3  col-xs-12">
            <label>عدد الغرف</label>
            <input name="upbuild_room" type="number"  class="form-control" placeholder="عدد الغرف" />
        </div>
        <div class="form-group col-md-3 col-xs-12">
            <label>عدد الحمامات</label>
            <input name="upbuild_bath" type="text" class="form-control" placeholder=">عدد الحمامات" />
        </div>
        
        <div class="form-group  col-md-3 col-xs-6">
            <label>الحالة </label>
            <select name="upbuild_status" class="form-control" >
            <option value="للإيجار">للإيجار </option>
            <option value="للبيع">للبيع </option>
            <option value="تمليك">تمليك </option>
            <option value="ضمان">ضمان</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>النوع</label>
            <select name="upbuild_kind" class="form-control" >
            <option value="بناية">بناية </option>
            <option value="مجمع تجاري">مجمع تجاري </option>
            <option value="أرض">أرض </option>
            <option value="قسيمة">قسيمة </option>
            <option value="شقة">شقة </option>
            <option value="فيلا">فيلا </option>
            <option value="بيت">بيت </option>
            <option value="محل">محل </option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-12">
            <label>اطلالة على:</label>
            <select name="upbuild_pos" class="form-control" >
            <option value="شارع">شارع </option>
            <option value="زاوية">زاوية</option>
            <option value="ساحة">ساحة </option>
            <option value="البحر">البحر </option>
            <option value="حديقة">حديقة </option>
            <option value="اخرى">اخرى</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-12">
            <label>القسم </label>
            <select name="bluidcategory" class="form-control" >
                @foreach($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->catname}}</option>
                @endforeach
            </select>
        </div>        
        <p class="h3 ad-title col-xs-12">
        معلومات اضافية: 
    </p>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="1" name="upbuild_cars" />
            مواقف سيارات
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="radio" value="1" name="upbuild_upstairs" checked />
            درج 
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="radio" value="2" name="upbuild_upstairs" />
            أصنصير
        </div>
        {{ csrf_field() }}
        <div class="form-group col-md-12 col-xs-12">
            @if($user)
                <input type="hidden" name="user_id" value="{{$user->id}}">
             
            <!--write inside hidden input's value attr. the today date & time-->
                <input type="hidden" value="date & time Now"  name="upbuild_todate" />
                <input type="submit" value="اضافة الاعلان" name="upbuild_submit" class="btn btn-block blue-btn" id="up-build" />
            @endif
        </div>
    </form>
</div>
@endsection
@section('scripts')
    <!--<script type="text/javascript">
        $(document).ready(function(){
            $("#mydropzone").dropzone({
                maxFiles:5,
                url: "{{route('addbuild.store')}}",
                addRemoveLinks: true,
                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 20,    
	            maxFilesize: 2,
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                success: function (file, response) {
                    console.log(response);
                }
            });
        });
    </script>-->
@endsection