@extends('layouts.app')
@section('content')
<p class="h1 ad-title">
    <i class="fa fa-tv"></i>
    اضافة اعلان إلكترونيات 
</p>
<div class="">
    @if(!$user)
    <span class="alert alert-warning col-md-12">
        لكي تتمكن من نشر اعلانك يجب أولا <a href="{{url('login')}}">تسجيل الدخول</a> الى حسابك أو فتح <a href="{{url('register')}}">حساب جديد</a> ان كنت لا تتوفر على حساب معنا
    </span>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success success col-md-12">
            {{ $message }}
        </div>
    @endif
    <!--upload photos-->
    <div class="clearfix"></div>
    <!--create data-->
    <form id="data-elc" method="post" action="{{route('addelectronic.store')}}" enctype="multipart/form-data" class="col-xs-12">
        <div class="dropzone" id="myawesomedropzone"></div>
        <div class="form-group col-md-6  col-xs-12">
            <label>اختر فيديو</label>
            <input name="upelc_video" type="file" class="form-control car-upvid" />
        </div>
        {{ csrf_field() }}
        <div class="form-group col-md-6 col-xs-12">
            <label>عنوان الاعلان</label>
            <input name="upelc_title" type="text" class="form-control" placeholder="عنوان الاعلان" />
        </div>
        <div class="form-group col-md-12 col-xs-12">
            <label>الوصف</label>
            <textarea class="form-control" name="upelc_desc" placeholder="الوصف"></textarea>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>السعر</label>
            <input name="upelc_price" type="number" class="form-control"  placeholder="السعر"/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>رقم الهاتف 1</label>
            <input name="upelc_fstphone" type="number" class="form-control" placeholder="رقم الهاتف 1" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>رقم الهاتف 2</label>
            <input name="upelc_secphone" type="number" class="form-control" placeholder="رقم الهاتف 2" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>ماركة الجهاز</label>
            <input name="upelc_brand" type="text" class="form-control" placeholder="ماركة الجهاز" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المحافظة </label>
            <select name="upelc_gov" class="form-control upcargov">
            <option value="">اختر المحافظة</option>
            <option value="الكويت">الكويت</option>
            <option value="حولي">حولي</option>
            <option value="الفروانية">الفروانية</option>
            <option value="مبارك الكبير">مبارك الكبير</option>
            <option value="الجهراء">الجهراء</option>
            <option value="الأحمدي">الأحمدي</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المنطقة </label>
            <select name="upelc_city" class="form-control domains_table" required>
                  @include('includes.city')
            </select>
        </div>
        <div class="form-group col-md-6  col-xs-12">
            <label>اكمال العنوان </label>
            <input name="upelc_adress" type="text" class="form-control" placeholder="اكتب تفاصيل العنوان : القطعة , الشارع , رقم المنزل / البناية , الدور , رقم الشقة / المكتب , "/>
        </div>
        <div class="form-group col-md-3  col-xs-12">
            <label>موديل الجهاز</label>
            <input name="upelc_model" type="text" class="form-control" placeholder="موديل الجهاز"/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>بلد الصنع</label>
            <input name="upelc_country" type="text"  class="form-control" placeholder="بلد الصنع"/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>سنة الصنع</label>
            <input name="upelc_year" type="number"  class="form-control" placeholder="سنة الصنع"/>
        </div>
        
        <div class="form-group  col-md-3 col-xs-6">
            <label>الحالة </label>
            <select name="upelc_status" class="form-control" >
            <option value="جديد">جديد </option>
            <option value="مستعمل">مستعمل </option>
            <option value="متهالك">متهالك </option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>تحت الكفالة </label>
            <select name="upelc_guar" class="form-control" >
            <option value="نعم">نعم </option>
            <option value="لا">لا</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>القسم  </label>
            <select name="upelccategory" class="form-control" >
                @foreach($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->catname}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-6 col-xs-12">
            <label>الملحقات </label>
            <textarea class="form-control" name="upelc_acc" placeholder="الملحقات"></textarea>
        </div>
        <div class="form-group col-md-12 col-xs-12">
             @if($user)
                <input type="hidden" name="user_id" value="{{$user->id}}">
            <!--write inside hidden input's value attr. the today date & time-->
            <input type="hidden" value="date & time Now"  name="upelc_todate" />
            <input type="submit" value="اضافة الاعلان " name="upelc_submit" class="btn btn-block blue-btn" id="up-elc" />
             @endif
        </div>
    </form>
</div>
@endsection