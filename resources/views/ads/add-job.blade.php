@extends('layouts.app')
@section('content')
<p class="h1 ad-title">
    <i class="fa fa-shopping-bag"></i>
    اضافة اعلان وظائف
</p>
<div class="">
     @if(!$user)
    <span class="alert alert-warning col-md-12">
        لكي تتمكن من نشر اعلانك يجب أولا <a href="{{url('login')}}">تسجيل الدخول</a> الى حسابك أو فتح <a href="{{url('register')}}">حساب جديد</a> ان كنت لا تتوفر على حساب معنا
    </span>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success success col-md-12">
            <p>{{ $message }}</p>
        </div>
    @endif
    <!--upload photos-->
    <div class="clearfix"></div>
    <!--create data-->
    <form id="data-jobs" method="post" action="{{route('addjobs.store')}}" enctype="multipart/form-data" class="col-xs-12">
        <div class="dropzone" id="myawesomedropzone"></div>
        <div class="form-group col-md-6  col-xs-12">
            <label>اختر فيديو</label>
            <input name="upjob_video" type="file" class="form-control car-upvid" />
        </div>
        {{ csrf_field() }}
        <div class="form-group col-md-6 col-xs-12">
            <label>عنوان الاعلان</label>
            <input name="upjob_title" type="text" class="form-control" placeholder="عنوان الاعلان" />
        </div>
        <div class="form-group col-md-12 col-xs-12">
            <label>الوصف</label>
            <textarea class="form-control" name="upjob_desc" placeholder="الوصف"></textarea>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>الراتب </label>
            <input name="upjob_salary" type="number" class="form-control" placeholder="الراتب"/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>رقم الهاتف 1</label>
            <input name="upjob_fstphone" type="number" class="form-control" placeholder="رقم الهاتف 1" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>رقم الهاتف 2</label>
            <input name="upjob_secphone" type="number" class="form-control" placeholder="رقم الهاتف 2" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>مجال العمل</label>
            <input name="upjob_comp" type="number" class="form-control" placeholder="مجال العمل" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المحافظة </label>
            <select name="upjob_gov" class="form-control upcargov">
            <option value="">اختر المحافظة</option>
            <option value="الكويت">الكويت</option>
            <option value="حولي">حولي</option>
            <option value="الفروانية">الفروانية</option>
            <option value="مبارك الكبير">مبارك الكبير</option>
            <option value="الجهراء">الجهراء</option>
            <option value="الأحمدي">الأحمدي</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المنطقة </label>
            <select name="upjob_city" class="form-control domains_table" required>
                  @include('includes.city')
            </select>
        </div>
        <div class="form-group col-md-6  col-xs-12">
            <label>اكمال العنوان </label>
            <input name="upjob_adress" type="text" class="form-control" placeholder="اكتب تفاصيل العنوان : القطعة , الشارع , رقم المنزل / البناية , الدور , رقم الشقة / المكتب , "/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>الخبرة السابقة  </label>
            <select name="upjob_exp" class="form-control" >
                <option value="خريج">خريج</option>
                <option value="مبتدئ">مبتدئ</option>
                <option value="متوسط">متوسط</option>
                <option value="محترف">محترف</option>
                <option value="مدير">مدير</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>الإجازة الأسبوعية  </label>
            <select name="upjob_Wholiday" class="form-control" >
            <option value="يوم الجمعة">يوم الجمعة</option>
            <option value="يوم السبت">يوم السبت</option>
            <option value="يوم الاحد">يوم الاحد</option>
            <option value="يوم الاثنين">يوم الاثنين</option>
            <option value="يوم الثلاثاء">يوم الثلاثاء</option>
            <option value="يوم الاربعاء">يوم الاربعاء</option>
            <option value="يوم الخميس">يوم الخميس</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>إجازة إضافية   </label>
            <select name="upjob_Aholiday" class="form-control" >
            <option value="لايوجد">لايوجد</option>
            <option value="يوم الجمعة">يوم الجمعة</option>
            <option value="يوم السبت">يوم السبت</option>
            <option value="يوم الاحد">يوم الاحد</option>
            <option value="يوم الاثنين">يوم الاثنين</option>
            <option value=">يوم الثلاثاء">يوم الثلاثاء</option>
            <option value="يوم الاربعاء">يوم الاربعاء</option>
            <option value="يوم الخميس">يوم الخميس</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>عدد الشواغر </label>
            <input name="upjob_Jnum" type="number" class="form-control" placeholder="عدد الشواغر" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>نوع العمل </label>
            <select name="upjob_Jkind" class="form-control" >
            <option value="دوام كامل">دوام كامل</option>
            <option value="نص دوام">نص دوام</option>
            <option value="عمل حر">عمل حر</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>الجنس المطلوب  </label>
            <select name="upjob_gen" class="form-control" >
            <option value="ذكور">ذكور</option>
            <option value="إناث">إناث</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>الجنسية  </label>
             <input name="upjob_nation" type="text" class="form-control" placeholder="الجنسية"/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>القسم</label>
            <select name="upjobcategory" class="form-control" >
                @foreach($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->catname}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>العمر</label>
            <input name="upjob_age" type="number" class="form-control" placeholder="العمر" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>الدرجة الدراسية </label>
            <input name="upjob_edu" type="text" class="form-control" placeholder="الدرجة الدراسية" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>التخصص العلمي </label>
            <input name="upjob_spec" type="text" class="form-control" placeholder="التخصص العلمي" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>أوقات الدوام - من :</label>
            <input name="upjob_fromTime" type="text" class="form-control" placeholder="أوقات الدوام - من"/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>الى:</label>
            <input name="upjob_toTime" type="text" class="form-control" placeholder="الى" />
        </div>
        
        <div class="form-group col-md-3  col-xs-12">
            <label>المنصب </label>
            <input name="upbuild-pos" type="text" class="form-control" placeholder="المنصب" />
        </div>
        <div class="form-group col-md-12 col-xs-12">
             @if($user)
                <input type="hidden" name="user_id" value="{{$user->id}}">
            <!--write inside hidden input's value attr. the today date & time-->
            <input type="hidden" value="date & time Now"  name="upjob_todate" />
            <input type="submit" value="اضافة الاعلان" name="upjob_submit" class="btn btn-block blue-btn" id="up-job" />
            @endif
        </div>
    </form>
</div>
@endsection