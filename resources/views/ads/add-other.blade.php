@extends('layouts.app')
@section('content')
<p class="h1 ad-title">
    <i class="fa fa-star"></i>
    اضافة اعلان أخرى 
</p>
<div class="">
    @if(!$user)
    <span class="alert alert-warning col-md-12">
        لكي تتمكن من نشر اعلانك يجب أولا <a href="{{url('login')}}">تسجيل الدخول</a> الى حسابك أو فتح <a href="{{url('register')}}">حساب جديد</a> ان كنت لا تتوفر على حساب معنا
    </span>
    @endif
    <!--upload photos-->
    <div class="clearfix"></div>
    <!--create data-->
    <form id="data-oth" method="POST" action="{{route('addother.store')}}" enctype="multipart/form-data" class="col-xs-12">
       <div class="dropzone" id="myawesomedropzone"></div>
        <div class="form-group col-md-6  col-xs-12">
            <label>اختر فيديو</label>
            <input name="upoth-video" type="file" class="form-control car-upvid" />
        </div>
        <div class="form-group col-md-6 col-xs-12">
            <label>عنوان الاعلان</label>
            <input name="upoth-title" type="text" class="form-control" />
        </div>
         {{ csrf_field() }}
        <div class="form-group col-md-6 col-xs-12">
            <label>القسم الرئيسي</label>
           <select name="supcategory" class="form-control supcategory" >
                <option value="1">اعلان سكراب</option>
                <option value="2">اعلان خدمات منزلية</option>
                <option value="3">اعلان اعمال حرة</option>
                <option value="4">اعلان اخرى</option>
            </select>
        </div>
        <div class="form-group col-md-6 col-xs-12">
            <label>القسم الفرعي</label>

            <select name="scrapcategory" class="form-control scrapcategory" >
               @foreach($scraps as $cat)
                    <option value="{{$cat->id}}">{{$cat->catname}}</option>
                @endforeach
            </select>

            <select name="housecategory" class="form-control housecategory" style="display:none" >
               @foreach($houses as $cat)
                    <option value="{{$cat->id}}">{{$cat->catname}}</option>
                @endforeach
            </select>

            <select name="lancecategory" class="form-control lancecategory" style="display:none" >
               @foreach($lances as $cat)
                    <option value="{{$cat->id}}">{{$cat->catname}}</option>
                @endforeach
            </select>

            <select name="otherscategory" class="form-control otherscategory" style="display:none" >
               @foreach($others as $cat)
                    <option value="{{$cat->id}}">{{$cat->catname}}</option>
                @endforeach
            </select>

        </div>
        <div class="form-group col-md-12 col-xs-12">
            <label>الوصف</label>
            <textarea class="form-control" name="upoth-desc"></textarea>
        </div>
        <div class="form-group col-md-3  col-xs-12">
            <label>السعر</label>
            <input name="upoth-price" type="number" class="form-control" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>رقم الهاتف 1</label>
            <input name="upoth-fstphone" type="number" class="form-control" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>رقم الهاتف 2</label>
            <input name="upoth-secphone" type="number" class="form-control" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المحافظة </label>
            <select name="upcar-gov" class="form-control upcargov">
            <option value="">اختر المحافظة</option>
            <option value="الكويت">الكويت</option>
            <option value="حولي">حولي</option>
            <option value="الفروانية">الفروانية</option>
            <option value="مبارك الكبير">مبارك الكبير</option>
            <option value="الجهراء">الجهراء</option>
            <option value="الأحمدي">الأحمدي</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المنطقة </label>
            <select name="upcar-city" class="form-control domains_table" required>
                  @include('includes.city')
            </select>
        </div>
        <div class="form-group col-md-9  col-xs-12">
            <label>اكمال العنوان </label>
            <input name="upoth-adress" type="text" class="form-control" placeholder="اكتب تفاصيل العنوان : القطعة , الشارع , رقم المنزل / البناية , الدور , رقم الشقة / المكتب , "/>
        </div>
        <div class="form-group col-md-12 col-xs-12">
             @if($user)
                <input type="hidden" name="user_id" value="{{$user->id}}">
              @endif
            <!--write inside hidden input's value attr. the today date & time-->
            <input type="hidden" value="date & time Now"  name="upoth-todate" />
            <input type="submit" value="اضافة الاعلان" name="upoth-submit" class="btn btn-block blue-btn" id="up-oth" />
        </div>
        
    </form>
    
</div>

@endsection