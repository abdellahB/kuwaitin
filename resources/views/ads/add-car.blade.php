@extends('layouts.app')

@section('content')
<p class="h1 ad-title">
    <i class="fa fa-car"></i>
    اضافة اعلان سيارات
    @if($user && $user->usertype !="user")
        <span class="pull-left" id="myForm">
            <label class="radio-inline">
            <input type="radio" name="optradio" value="1" checked> <b>اعلان سيارات </b>
            </label>
            <label class="radio-inline">
            <input type="radio" name="optradio" value="2"> <b>تأجير سيارات </b>
            </label>
        </span>
   @endif
</p>
<div class="">
    @if(!$user)
    <span class="alert alert-warning col-md-12">
        لكي تتمكن من نشر اعلانك يجب أولا <a href="{{url('login')}}">تسجيل الدخول</a> الى حسابك أو فتح <a href="{{url('register')}}">حساب جديد</a> ان كنت لا تتوفر على حساب معنا
    </span>
    @endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success success col-md-12">
            <p>{{ $message }}</p>
        </div>
    @endif
    <!--upload photos-->
    <div class="clearfix"></div>
    <!--create data-->
    <form id="data-cars" method="POST" action="{{Route('addcar.store')}}" 
        enctype="multipart/form-data" class="col-xs-12 ">
        <div id="upcar_img"></div>
        {{ csrf_field() }}
        <div class="form-group col-md-6  col-xs-12">
            <label>صورة الاعلان</label>
            <input name="upcar_video" type="file" class="form-control car-upvid" />
        </div>
        <div class="form-group col-md-6 col-xs-12">
            <label>عنوان الاعلان</label>
            <input name="upcar_title" type="text" class="form-control" placeholder="عنوان الاعلان" required />
        </div>
        <div class="form-group col-md-12 col-xs-12">
            <label>الوصف</label>
            <textarea class="form-control" name="upcar_desc" placeholder="وصف الاعلان " required></textarea>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>السعر</label>
            <input name="upcar_price" type="text" class="form-control" placeholder="السعر" required/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>رقم الهاتف 1</label>
            <input name="upcar_fstphone" type="number" class="form-control" placeholder="رقم الهاتف 1" required/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>رقم الهاتف 2</label>
            <input name="upcar_secphone" type="number" class="form-control" placeholder="رقم الهاتف 2" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>الماركة </label>
            <select name="car_makes" id="car-makes" class="form-control" >
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>حجم  الرنقات</label>
            <input name="upcar_size" type="text" class="form-control" placeholder="حجم  الرنقات"/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المسافة المقطوعة</label>
            <input name="upcar_dist" type="text" class="form-control" placeholder="المسافة المقطوعة"/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المحافظة </label>
            <select name="upcar_gov" class="form-control upcargov">
            <option value="">اختر المحافظة</option>
            <option value="الكويت">الكويت</option>
            <option value="حولي">حولي</option>
            <option value="الفروانية">الفروانية</option>
            <option value="مبارك الكبير">مبارك الكبير</option>
            <option value="الجهراء">الجهراء</option>
            <option value="الأحمدي">الأحمدي</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المنطقة </label>
            <select name="upcar_city" class="form-control domains_table" required>
                  @include('includes.city')
            </select>
        </div>
        <div class="form-group col-md-6  col-xs-12">
            <label>اكمال العنوان </label>
            <input name="upcar_adress" type="text" class="form-control" placeholder="اكتب تفاصيل العنوان : القطعة , الشارع , رقم المنزل / البناية , الدور , رقم الشقة / المكتب , " required/>
        </div>
        
        <div class="form-group  col-md-3 col-xs-4">
            <label>الموديل</label>
            <select name="car_models" id="car-models" class="form-control" >
            
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>السنة</label>
            <select name="car_years" id="car-years" class="form-control" >
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>اللون الخارجي </label>
            <input type="text" name="upcar_outcolor" class="form-control" placeholder="اللون الخارجي" />
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>اللون الداخلي </label>
            <input type="text" name="upcar_incolor" class="form-control"  placeholder="اللون الداخلي" />
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>الجير </label>
            <select name="upcar_gear" class="form-control" >
                <option value="عادي">عادي</option>
                <option value="أوتوماتيك">أوتوماتيك </option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>دبل جير </label>
            <select name="upcar_gear_type" class="form-control" >
            <option value="لا">لا</option>
            <option value="نعم">نعم </option>
            </select>
        </div>
        <div class="clearfix visible-xs "></div>
        <div class="form-group col-md-3  col-xs-4">
            <label>عدد الأبواب</label>
            <select name="cq_doors" id="cq-doors" class="form-control" >
            </select>
        </div>
        <div class="form-group  col-md-3 col-xs-4">
            <label>نوع الوقود </label>
            <select name="upcar_fuel" class="form-control" >
            <option value="بنزين">بنزين </option>
            <option value="مازوت">مازوت  </option>
            <option value="غاز">غاز </option>
            <option value="كيروسين">كيروسين  </option>
            <option value="اخرى">اخرى</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>حالة الإطارات </label>
                <select name="upcar_wheel" class="form-control" >
                <option value="جيدة">جيدة </option>
                <option value="متهالكة">متهالكة </option>
                <option value="متوسطة">متوسطة </option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>القسم  </label>
            <select name="upcar_category" class="form-control" required>
                @foreach($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->catname}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>شرط الفحص </label>
            <select name="upcar_check" class="form-control" >
            <option value="نعم">نعم </option>
            <option value="لا">لا</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>حجم الماكينة </label>
            <select name="upcar_csize" class="form-control" >
            <option value="6 سلندر">6 سلندر</option>
            <option value="4 سلندر">4 سلندر </option>
            <option value="5 سلندر">5 سلندر</option>
            <option value="8 سلندر">8 سلندر</option>
            <option value="10 سلندر">10 سلندر</option>
            <option value="12 سلندر">12 سلندر</option>
            <option value="16 سلندر">16 سلندر</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>تحت الكفالة </label>
            <select name="upcar_guar" class="form-control" >
            <option value="لا">لا</option>
            <option value="نعم">نعم </option>
            </select>
        </div>
        <div class="clearfix visible-xs "></div>
        <div class="form-group col-md-3  col-xs-4">
            <label>انتهاء الدفتر </label>
            <input name="upcar_end" type="text" class="form-control " />
        </div>
        <div class="form-group  col-md-3 col-xs-4">
            <label>مواصفات </label>
            <select name="upcar_spec" class="form-control" >
                <option value="خليجية">خليجية </option>
                <option value="أمريكية">أمريكية</option>
                <option value="أوروبية">أوروبية</option>
                <option value="أخرى">أخرى</option>
            </select>
        </div>
        
        <div class="form-group col-md-3  col-xs-4">
            <label>الحالة  </label>
            <select name="upcar_status" class="form-control" >
            <option value="جيدة">جيدة  </option>
            <option value="جديدة">جديدة </option>
            <option value="متهالكة">متهالكة </option>
            <option value="سكراب">سكراب </option>
            </select>
        </div>
        <p class="h3 ad-title col-xs-12">
        الكماليات 
    </p>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="فتحة" name="camal" />
            فتحة
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="شاشة" name="camal1" />
            شاشة
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="سي دي" name="camal2" />
            سي دي
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="كاميرا" name="camal3" />
            كاميرا
        </div>
        <div class="clearfix visible-xs "></div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="زنون" name="camal4" />
            زنون
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="نفجيشن" name="camal5" />
            نفجيشن
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="تكييف" name="camal6" />
            تكييف
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="بانوراما" name="camal7" />
            بانوراما
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="جناح" name="camal8" />
            جناح
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="رنقات" name="camal9" />
            رنقات
        </div>
        
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="تبريد كراسي" name="camal10" />
            تبريد كراسي
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="تدفئة كراسي" name="camal11" />
            تدفئة كراسي
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="شاشات خلفية" name="camal12" />
            شاشات خلفية
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="كاربون فايبر" name="camal13" />
            كاربون فايبر
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="تحكم سكان " name="camal14" />
            تحكم سكان 
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="النقطة العمياء" name="camal15" />
            النقطة العمياء
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="حماية زجاج" name="camal16" />
            حماية زجاج
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value=" شبك حماية خارجي" name="camal17" />
            شبك حماية خارجي
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value=" تشغيل بدون مفتاح" name="camal18" />
            تشغيل بدون مفتاح
        </div>
        <div class="form-group col-md-12 col-xs-12">
        @if($user)
            <input type="hidden" name="user_id" value="{{$user->id}}">
           
            <!--write inside hidden input's value attr. the today date & time-->
            <input type="hidden" value="date & time Now"  name="upcar_todate" />
            <input type="submit" value="اضافة الاعلان" name="upcar_submit" class="btn btn-block blue-btn" id="up_car" />
         @endif
        </div>
    </form>

    <!-- تاجير السيارات -->
    <form id="data-rentcar" method="" action="/upload" enctype="multipart/form-data" class="col-xs-12" style="display:none">
        <div class="dropzone" id="myawesomedropzone">
        </div>
        <div class="form-group col-md-6  col-xs-12">
            <label>اختر فيديو</label>
            <input name="uprentcar_video" type="file" class="form-control car-upvid" />
        </div>
        <div class="form-group col-md-6 col-xs-12">
            <label>عنوان الاعلان</label>
            <input name="uprentcar_title" type="text" class="form-control" placeholder="عنوان الاعلان"/>
        </div>
        <div class="form-group col-md-12 col-xs-12">
            <label>الوصف</label>
            <textarea class="form-control" name="uprentcar_desc" placeholder="الوصف"></textarea>
        </div>
        <div class="form-group col-md-2  col-xs-6">
            <label>السعر - يومي</label>
            <input name="uprentcar_Dprice" type="text" class="form-control" placeholder="السعر - يومي"/>
        </div>
        <div class="form-group col-md-2  col-xs-6">
            <label>أسبوعي </label>
            <input name="uprentcar_Wprice" type="text" class="form-control" placeholder="أسبوعي" />
        </div>
        <div class="form-group col-md-2  col-xs-6">
            <label>شهري</label>
            <input name="uprentcar_Mprice" type="text" class="form-control" placeholder="شهري" />
        </div>
        <div class="form-group col-md-2  col-xs-6">
            <label>ربع سنوي </label>
            <input name="uprentcar_QYprice" type="text" class="form-control" placeholder="ربع سنوي" />
        </div>
        <div class="form-group col-md-2  col-xs-6">
            <label>نصف سنوي </label>
            <input name="uprentcar_HYprice" type="text" class="form-control" placeholder="نصف سنوي" />
        </div>
        <div class="form-group col-md-2  col-xs-6">
            <label>سنوي </label>
            <input name="uprentcar_Yprice" type="text" class="form-control" placeholder="سنوي" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>رقم الهاتف 1</label>
            <input name="uprentcar_fstphone" type="number" class="form-control" placeholder="رقم الهاتف 1" />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>رقم الهاتف 2</label>
            <input name="uprentcar_secphone" type="number" class="form-control" placeholder="رقم الهاتف 2"/>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>الماركة </label>
            <select name="uprentcar_company" id="car-makes" class="form-control">
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>حجم الرنقات</label>
            <input name="uprentcar_size" type="text" class="form-control" placeholder="حجم الرنقات" />
        </div>

        <div class="form-group col-md-3  col-xs-6">
            <label>المحافظة </label>
            <select name="uprentcar_gov" class="form-control upcargov">
                <option value="">اختر المحافظة</option>
                <option value="الكويت">الكويت</option>
                <option value="حولي">حولي</option>
                <option value="الفروانية">الفروانية</option>
                <option value="مبارك الكبير">مبارك الكبير</option>
                <option value="الجهراء">الجهراء</option>
                <option value="الأحمدي">الأحمدي</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المنطقة </label>
            <select name="uprentcar_city" class="form-control domains_table">
                  @include('includes.city')
            </select>
        </div>
        <div class="form-group col-md-6  col-xs-12">
            <label>اكمال العنوان </label>
            <input name="uprentcar_adress" type="text" class="form-control" placeholder="اكتب تفاصيل العنوان : القطعة , الشارع , رقم المنزل / البناية , الدور , رقم الشقة / المكتب , " />
        </div>
        <div class="form-group col-md-3  col-xs-6">
            <label>المسافة المقطوعة</label>
            <input name="uprentcar_dist" type="text" class="form-control" placeholder="المسافة المقطوعة"/>
        </div>
        <div class="form-group  col-md-3 col-xs-6">
            <label>الموديل</label>
            <select name="uprentcar_mod" id="car-models" class="form-control">
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>السنة</label>
            <select name="uprentcar_year" id="car-years" class="form-control">
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>اللون الخارجي </label>
            <input type="text" name="uprentcar_outcolor" class="form-control" placeholder="اللون الخارجي">
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>اللون الداخلي </label>
            <input type="text" name="uprentcar_incolor" class="form-control" placeholder="اللون الداخلي">
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>الجير </label>
            <select name="uprentcar_gear" class="form-control">
                <option value="عادي">عادي</option>
                <option value="أوتوماتيك">أوتوماتيك </option>

            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>دبل جير </label>
            <select name="uprentcar_gear" class="form-control">
                <option value="لا">لا</option>
                <option value="نعم">نعم </option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>عدد الأبواب</label>
            <select name="uprentcar_door" id="cq-doors" class="form-control">
            </select>
        </div>
        <div class="form-group  col-md-3 col-xs-4">
            <label>نوع الوقود </label>
            <select name="uprentcar_fuel" class="form-control">
                <option value="بنزين">بنزين </option>
                <option value="مازوت">مازوت </option>
                <option value="غاز">غاز </option>
                <option value="كيروسين">كيروسين </option>
                <option value="اخرى">اخرى</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>حالة الإطارات </label>
            <select name="uprentcar_wheel" class="form-control">
                <option value="جيدة">جيدة </option>
                <option value="متهالكة">متهالكة </option>
                <option value="متوسطة">متوسطة </option>

            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>شرط الفحص </label>
            <select name="uprentcar_check" class="form-control">
                <option value="نعم">نعم </option>
                <option value="لا">لا</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>حجم الماكينة </label>
            <select name="uprentcar_csize" class="form-control">
                 <option value="6 سلندر">6 سلندر</option>
                <option value="4 سلندر">4 سلندر </option>
                <option value="5 سلندر">5 سلندر</option>
                <option value="8 سلندر">8 سلندر</option>
                <option value="10 سلندر">10 سلندر</option>
                <option value="12 سلندر">12 سلندر</option>
                <option value="16 سلندر">16 سلندر</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>تحت الكفالة </label>
            <select name="uprentcar_guar" class="form-control">
                <option value="لا">لا</option>
                <option value="نعم">نعم </option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>انتهاء الدفتر </label>
            <input name="uprentcar_end" type="text" class="text-data form-control " placeholder="انتهاء الدفتر "/>
        </div>
        <div class="clearfix visible-xs "></div>
        <div class="form-group  col-md-3 col-xs-4">
            <label>مواصفات </label>
            <select name="uprentcar_spec" class="form-control">
                <option value="خليجية">خليجية </option>
                <option value="أمريكية">أمريكية</option>
                <option value="أوروبية">أوروبية</option>
                <option value="أخرى">أخرى</option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>الحالة </label>
            <select name="uprentcar_status" class="form-control">
                <option value="جيدة">جيدة </option>
                <option value="جديدة">جديدة </option>
                <option value="متهالكة">متهالكة </option>
                <option value="سكراب">سكراب </option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label> نوع الكفالة </label>
            <select name="uprentcar_Gtype" class="form-control">
                <option value="لايوجد">لايوجد </option>
                <option value="ضد الغير">ضد الغير </option>
                <option value="شاملة">شاملة </option>
                <option value="أخرى">أخرى </option>
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-4">
            <label>القسم  </label>
            <select name="upcar_category" class="form-control" required>
                @foreach($cats as $cat)
                    <option value="{{$cat->id}}">{{$cat->catname}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-3  col-xs-12">
            <label>دفعة مقدمة (د.ك) </label>
            <input name="uprentcar_Pprice" type="text" class="form-control" placeholder="دفعة مقدمة (د.ك)"/>
        </div>
        <div class="form-group col-md-3 col-xs-6">
            <label>متوفرة </label>
            <select name="uprentcar_avai" class="form-control">
                <option value="نعم">نعم </option>
                <option value="لا">لا</option>
                <option value="قريبا">قريبا</option>
                <option value="معروضة للبيع">معروضة للبيع</option>
            </select>
        </div>
        <div class="form-group col-md-3 col-xs-6">
            <label>أخرى </label>
            <input type="text" name="uprentcar_other" class="form-control" placeholder="أخرى" />
        </div>
        <p class="h3 ad-title col-xs-12">
            الكماليات
        </p>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="فتحة" name="" /> فتحة
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="شاشة" name="" /> شاشة
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value=" سي دي" name="" /> سي دي
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="كاميرا" name="" /> كاميرا
        </div>
        <div class="clearfix visible-xs "></div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="زنون" name="" /> زنون
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="نفجيشن" name="" /> نفجيشن
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="تكييف" name="" /> تكييف
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="بانوراما" name="" /> بانوراما
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="جناح" name="" /> جناح
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="رنقات" name="" /> رنقات
        </div>

        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="تبريد كراسي" name="" /> تبريد كراسي
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value=" تدفئة كراسي" name="" /> تدفئة كراسي
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="شاشات خلفية" name="" /> شاشات خلفية
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="كاربون فايبر" name="" /> كاربون فايبر
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="تحكم سكان" name="" /> تحكم سكان
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="النقطة العمياء" name="" /> النقطة العمياء
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="حماية زجاج" name="" /> حماية زجاج
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="شبك حماية خارجي" name="" /> شبك حماية خارجي
        </div>
        <div class="form-group col-md-2 col-xs-3">
            <input type="checkbox" value="تشغيل بدون مفتاح" name="" /> تشغيل بدون مفتاح
        </div>
        <p class="h3 ad-title col-xs-12">
            مزايا إضافية
        </p>
        <div class="form-group col-md-3 col-xs-6">
            <input type="checkbox" value="تامين شامل ذهبي" name="" /> تامين شامل ذهبي
        </div>
        <div class="form-group col-md-3 col-xs-6">
            <input type="checkbox" value=" صيانه مجانيه طوال فتره العقد" name="" /> صيانه مجانيه طوال فتره العقد
        </div>
        <div class="form-group col-md-3 col-xs-6">
            <input type="checkbox" value=" سياره بديله للأعطال والحوادث" name="" /> سياره بديله للأعطال والحوادث
        </div>
        <div class="form-group col-md-3 col-xs-6">
            <input type="checkbox" value=" خدمه المساعده على الطريق" name="" /> خدمه المساعده على الطريق
        </div>
        <p class="h3 ad-title col-xs-12">
            الأوراق المطلوبة
        </p>
        <div class="form-group col-md-3 col-xs-6">
            <input type="checkbox" value=" صوره البطاقه المدنيه" name="" /> صوره البطاقه المدنيه
        </div>
        <div class="form-group col-md-3 col-xs-6">
            <input type="checkbox" value="صوره رخصه القياده ساريه المفعول" name="" /> صوره رخصه القياده ساريه المفعول
        </div>
        <div class="form-group col-md-3 col-xs-6">
            <input type="checkbox" value="شهاده راتب حديثه" name="" /> شهاده راتب حديثه
        </div>
        <div class="form-group col-md-3 col-xs-6">
            <input type="checkbox" value="كشف حساب من البنك أخر 6 شهور" name="" /> كشف حساب من البنك أخر 6 شهور
        </div>
        <div class="form-group col-md-3 col-xs-6">
            <input type="checkbox" value="صوره جواز السفر ( للمقيم )" name="" /> صوره جواز السفر ( للمقيم )
        </div>
        <div class="form-group col-md-3 col-xs-6">
            <input type="checkbox" value="اعتماد توقيع لموظفي القطاع الخاص" name="" /> اعتماد توقيع لموظفي القطاع الخاص
        </div>
        <div class="form-group col-md-6 col-xs-12">
            <input type="checkbox" value=" شهاده دعم العماله للكويتي" name="" /> شهاده دعم العماله للكويتي
        </div>
        
        <div class="form-group col-md-12 col-xs-12">
           @if($user)
            <input type="hidden" name="user_id" value="{{$user->id}}">
            <input type="submit" value="اضافة الاعلان" name="uprentcar_submit" class="btn btn-block blue-btn" id="up_car" />
           @endif
        </div>
    </form>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('public/assets/js/carsQuery.js') }}"></script>
<script type="text/javascript" src="http://www.carqueryapi.com/js/carquery.0.3.4.js"></script>
<script type="text/javascript">
    $(function() {

        var uploadObj = $("#upcar_img").uploadFile({
            url:"{{Route('addcar.store')}}", // upload page url
            fileName: "upcar_img[]" ,// file input name
            multiple:true,
            dragDrop:true,
            acceptFiles:"image/*",
            showPreview:true,
            maxFileCount:4,
            autoSubmit: false,
            sequential:true,
            sequentialCount:4,
            previewHeight: "100px",
            previewWidth: "100px",
            enctype: "multipart/form-data",
            dragDropStr:"<span class='h5'>اختر صور لرفعها او ادرج الصور هنا</span>",
            uploadStr: "اختر"
        });

        $("#up_car").click(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            uploadObj.startUpload();

            alert("ok");
        });

    });
</script>
@endsection
