@extends('layouts.app')
@section('header')
    @include('includes.sliderads')
    @include('includes.search')
@endsection
@section('content')
<div class="col-md-8 col-xs-12 site-content usPro">
   <!---start site-content , usPro-->
   <div class="col-xs-12 edit-form">
      <form class="col-xs-12 form" action="{{url('profile')}}" method="POST" enctype="multipart/form-data">
         <p class="h2 ad-title">
            <i class="fa fa-edit"></i>
            تعديل الملف الشخصي :
         </p>
         <ul class="edit-cont">
            <li class="col-xs-12 usimg">
               <div class="form-group">
                  <input type="file" name="" id="chUsImg" onchange="loadFile(event)" />
                  <img src="{{url('public/assets/img')}}/{{$user->photo}}" class="user-img" id="cuUsImg" />
                  <p class="h4  img-cam">
                     <i class="fa fa-camera"></i>
                  </p>
                  <p class="h5 text-center">الصورة الشخصية</p>
               </div>
            </li>
              {{ csrf_field() }}
            <li class="col-xs-12">
               <div class="form-group">
                  <label class="col-md-3 col-xs-12">اسم المستخدم :</label>
                  <input type="text" class="form-control col-md-9 col-xs-12" value="{{$user->username}}" disabled />
               </div>
            </li>
            <li class="col-xs-12">
               <div class="form-group">
                  <label class="col-md-3 col-xs-12">رقم الهاتف :</label>
                  <input type="text" class="form-control col-md-9 col-xs-12" value="{{$user->phone}}" disabled />
               </div>
            </li>
            <li class="col-xs-12">
               <div class="form-group">
                  <label class="col-md-3 col-xs-6">كلمة المرور :</label>
                  <label class="col-md-6 col-xs-12 hidden-xs">.............</label>
                  <label class="col-md-3 col-xs-6">
                  <a data-toggle="tab"  href="#chPass">
                  <i class="fa fa-pencil"></i>
                  تعديل
                  </a>
                  </label>
               </div>
            </li>
            <li class="col-xs-12 tab-pane fade" id="chPass">
               <div class="form-group">
                  <label class="col-md-3 col-xs-12">كلمة المرور الجديدة :</label>
                  <input type="password" class="form-control col-md-9 col-xs-12" name="password"   />
               </div>
               <div class="form-group">
                  <label class="col-md-3 col-xs-12">تأكيد كلمة المرور :</label>
                  <input type="password" class="form-control col-md-9 col-xs-12" name="confirm"  />
               </div>
            </li>
            <li class="col-xs-12">
               <div class="form-group">
                  <label class="col-md-3 col-xs-12">البريد الإلكتروني :</label>
                  <input type="email" class="form-control col-md-9 col-xs-12" name="emailuser" placeholder="xxxxxx@xxxxxxx.com" value="{{$user->emailuser}}"  />
               </div>
            </li>
            <div class="form-group">
               <input type="submit" value="تحديث" name="" class="btn green-btn" />
            </div>
         </ul>
         <!--end edit-cont-->
      </form>
      <ul class="edit-cont">
         <li class="col-xs-12">
            <div class="form-group">
               <label class="col-md-3 col-xs-6">
               تنبيهات العروض
               <span class="badge">4</span>
               :
               </label>
               <div class="col-md-9 col-xs-12 offe-block">
                  <div class="col-xs-12">
                     <a href="#">هناك عرض سعر جديد عللى إعلانك "عنوان الاعلان يكتب هنا"</a>
                  </div>
                  <div class="col-xs-12">
                     <a href="#">هناك عرض سعر جديد عللى إعلانك "عنوان الاعلان يكتب هنا"</a>
                  </div>
                  <div class="col-xs-12">
                     <a href="#">هناك عرض سعر جديد عللى إعلانك "عنوان الاعلان يكتب هنا"</a>
                  </div>
                  <div class="col-xs-12">
                     <a href="#">هناك عرض سعر جديد عللى إعلانك "عنوان الاعلان يكتب هنا"</a>
                  </div>
               </div>
            </div>
         </li>
         <!-- favourite-->
         <li class="col-xs-12">
            <div class="form-group">
               <label class=" col-xs-12 ad-title">
               <strong>مفضلتي :</strong>
               </label>
               <div class="col-xs-12">
                  <div class="col-md-4 col-xs-6 feat-block">
                     <!---start feat-block -->
                     <div class="hovereffect featured">
                        <a href="#" class="feat-img">
                        <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                        </a>
                        <div class="overlay col-xs-12">
                           <div class="col-xs-12 feat-title">
                              <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                           </div>
                           <div class="col-xs-6">
                              <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                              1234
                              <i class="fa fa-eye"></i>
                              </a>
                           </div>
                           <div class="col-xs-3" >
                              <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                              <i class="fa fa-heart"></i>
                              </a>
                           </div>
                           <div class="col-xs-3">
                              <div class="ui share">
                                 <i class="fa fa-share-alt"></i>
                              </div>
                              <div class="ui share-menu flowing popup top center transition hidden">
                                 <div class="share-list">
                                    <div class="share-item">
                                       <a href="#">
                                       <i class="fa fa-facebook"></i>
                                       </a>
                                    </div>
                                    <div  class="share-item">
                                       <a href="#">
                                       <i class="fa fa-whatsapp"></i>
                                       </a>
                                    </div>
                                    <div  class="share-item">
                                       <a href="#">
                                       <i class="fa fa-twitter"></i>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!---end feat-block -->
                  <div class="col-md-4 col-xs-6 feat-block">
                     <!---start feat-block -->
                     <div class="hovereffect featured">
                        <a href="#" class="feat-img">
                        <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                        </a>
                        <div class="overlay col-xs-12">
                           <div class="col-xs-12 feat-title">
                              <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                           </div>
                           <div class="col-xs-6">
                              <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                              1234
                              <i class="fa fa-eye"></i>
                              </a>
                           </div>
                           <div class="col-xs-3" >
                              <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                              <i class="fa fa-heart"></i>
                              </a>
                           </div>
                           <div class="col-xs-3">
                              <div class="ui share">
                                 <i class="fa fa-share-alt"></i>
                              </div>
                              <div class="ui share-menu flowing popup top center transition hidden">
                                 <div class="share-list">
                                    <div class="share-item">
                                       <a href="#">
                                       <i class="fa fa-facebook"></i>
                                       </a>
                                    </div>
                                    <div  class="share-item">
                                       <a href="#">
                                       <i class="fa fa-whatsapp"></i>
                                       </a>
                                    </div>
                                    <div  class="share-item">
                                       <a href="#">
                                       <i class="fa fa-twitter"></i>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!---end feat-block -->
                  <div class="col-md-4 col-xs-6 feat-block">
                     <!---start feat-block -->
                     <div class="hovereffect featured">
                        <a href="#" class="feat-img">
                        <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                        </a>
                        <div class="overlay col-xs-12">
                           <div class="col-xs-12 feat-title">
                              <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                           </div>
                           <div class="col-xs-6">
                              <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                              1234
                              <i class="fa fa-eye"></i>
                              </a>
                           </div>
                           <div class="col-xs-3" >
                              <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                              <i class="fa fa-heart"></i>
                              </a>
                           </div>
                           <div class="col-xs-3">
                              <div class="ui share">
                                 <i class="fa fa-share-alt"></i>
                              </div>
                              <div class="ui share-menu flowing popup top center transition hidden">
                                 <div class="share-list">
                                    <div class="share-item">
                                       <a href="#">
                                       <i class="fa fa-facebook"></i>
                                       </a>
                                    </div>
                                    <div  class="share-item">
                                       <a href="#">
                                       <i class="fa fa-whatsapp"></i>
                                       </a>
                                    </div>
                                    <div  class="share-item">
                                       <a href="#">
                                       <i class="fa fa-twitter"></i>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!---end feat-block -->
                  <div class="col-md-4 col-xs-6 feat-block">
                     <!---start feat-block -->
                     <div class="hovereffect featured">
                        <a href="#" class="feat-img">
                        <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                        </a>
                        <div class="overlay col-xs-12">
                           <div class="col-xs-12 feat-title">
                              <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                           </div>
                           <div class="col-xs-6">
                              <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                              1234
                              <i class="fa fa-eye"></i>
                              </a>
                           </div>
                           <div class="col-xs-3" >
                              <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                              <i class="fa fa-heart"></i>
                              </a>
                           </div>
                           <div class="col-xs-3">
                              <div class="ui share">
                                 <i class="fa fa-share-alt"></i>
                              </div>
                              <div class="ui share-menu flowing popup top center transition hidden">
                                 <div class="share-list">
                                    <div class="share-item">
                                       <a href="#">
                                       <i class="fa fa-facebook"></i>
                                       </a>
                                    </div>
                                    <div  class="share-item">
                                       <a href="#">
                                       <i class="fa fa-whatsapp"></i>
                                       </a>
                                    </div>
                                    <div  class="share-item">
                                       <a href="#">
                                       <i class="fa fa-twitter"></i>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!---end feat-block -->
                  <div class="col-md-4 col-xs-6 feat-block">
                     <!---start feat-block -->
                     <div class="hovereffect featured">
                        <a href="#" class="feat-img">
                        <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                        </a>
                        <div class="overlay col-xs-12">
                           <div class="col-xs-12 feat-title">
                              <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                           </div>
                           <div class="col-xs-6">
                              <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                              1234
                              <i class="fa fa-eye"></i>
                              </a>
                           </div>
                           <div class="col-xs-3" >
                              <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                              <i class="fa fa-heart"></i>
                              </a>
                           </div>
                           <div class="col-xs-3">
                              <div class="ui share">
                                 <i class="fa fa-share-alt"></i>
                              </div>
                              <div class="ui share-menu flowing popup top center transition hidden">
                                 <div class="share-list">
                                    <div class="share-item">
                                       <a href="#">
                                       <i class="fa fa-facebook"></i>
                                       </a>
                                    </div>
                                    <div  class="share-item">
                                       <a href="#">
                                       <i class="fa fa-whatsapp"></i>
                                       </a>
                                    </div>
                                    <div  class="share-item">
                                       <a href="#">
                                       <i class="fa fa-twitter"></i>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!---end feat-block -->
               </div>
            </div>
            <div class="col-xs-12 comp-ads-more">
               <a href="#" class="cli-btn blue-btn">المزيد</a>
            </div>
         </li>
         <!-- favourite-->
         <!-- my ads-->
         <li class="col-xs-12">
            <div class="form-group">
               <label class=" col-xs-12 ad-title">
               <strong>إعلاناتي :</strong>
               </label>
               <div class="col-xs-12">
               @if($classcars)
                @foreach($classcars as $cars)
                    <div class="col-md-4 col-xs-6 feat-block">
                     <!---start feat-block -->
                     <div class="hovereffect @if($cars->statys==2) featured @endif">
                        <a href="{{route('addcar.show',$cars->id)}}" class="feat-img">
                        <img class="img-responsive" src="{{url('public/assets/img/video')}}/{{$cars->upcar_video}}" alt="">
                        </a>
                        <div class="overlay col-xs-12">
                           <div class="col-xs-12 feat-title">
                              <a href="{{route('addcar.show',$cars->id)}}">
                              {{$cars->upcar_title}}
                              </a>
                           </div>
                           <div class="col-xs-6">
                              <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                              1234
                              <i class="fa fa-eye"></i>
                              </a>
                           </div>
                           <div class="col-xs-3" >
                              <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                              <i class="fa fa-heart"></i>
                              </a>
                           </div>
                           <div class="col-xs-3">
                              <div class="ui share">
                                 <i class="fa fa-share-alt"></i>
                              </div>
                              <div class="ui share-menu flowing popup top center transition hidden">
                                 <div class="share-list">
                                    <div class="share-item">
                                       <a href="#">
                                       <i class="fa fa-facebook"></i>
                                       </a>
                                    </div>
                                    <div  class="share-item">
                                       <a href="#">
                                       <i class="fa fa-whatsapp"></i>
                                       </a>
                                    </div>
                                    <div  class="share-item">
                                       <a href="#">
                                       <i class="fa fa-twitter"></i>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                       </div>
                    </div>
                @endforeach  
                @endif
            </div>
            <div class="col-xs-12 comp-ads-more">
               <a href="#" class="cli-btn blue-btn">المزيد</a>
            </div>
         </li>
         <!-- my ads-->
      </ul>
      <!--end edit-cont-->
   </div>
</div>
<!---end site-content , usPro -->
    @include('includes.sidebar')
@endsection
