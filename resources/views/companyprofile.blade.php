<?php
$dt = new DateTime();
$date = $dt->format('m/d/Y');
?>
@extends('layouts.app')
@section('header')
<section id="header-comp"><!---Start header-comp section -->
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-12 comp-ar-ti">
                <p class="h3">{{$companies->compname}}</p>
                <p class="h4">{{$companies->compworking}}</p>
            </div><!---end comp-ar-ti -->
            <div class="col-md-4 col-xs-12 comp-logo">
                <img src="{{url('public/assets/img')}}/{{$companies->complogo}}" />
                <div class="comp-rate-info col-xs-12">
                    <div class="col-xs-6 comp-watch h4">
                        <span><i class="fa fa-eye"></i></span>
                        <span>123455</span>
                    </div><!---end comp-watch -->
                    <div class="col-xs-6 comp-rating">
                        <div class="ui star rating" data-rating="5" data-max-rating="5"></div>
                    </div><!---end comp-rateing -->
                </div>
            </div>
            <div class="col-md-4 col-xs-12 comp-en-ti">
            <p class="h3">{{$companies->compname_en}}</p>
                <p class="h4">
                <span>Member since:</span>
                <span>29/12/2017</span>
                </p>
            </div><!---end comp-en-ti -->
        </div>
    </div>
</section><!---end header-comp section -->    
@endsection
@section('content')
<div class="col-md-8 col-xs-12 site-content"><!---start site-content-->
    <div class="col-xs-12 comp-view-desc">
    <p class="h3"><strong>عن الشركة :</strong></p>
    <p class="h5">
    {{$companies->compabout}}
    </p>
    </div><!---end comp-view-desc -->
    <div class="col-xs-12 comp-view-desc">
        <p class="h3"><strong>العنوان :</strong></p>
        <p class="h5">
            {{$companies->comptitle}}
        </p>
    </div><!---end comp-view-desc -->
    
    <div class="col-xs-12 comp-view-desc ">
        <div class="col-xs-12 comp-view-info">
            <div class="col-md-4 col-xs-12 comp-v-ti">أوقات العمل:</div>
            <div class="col-md-8 col-xs-12">
                <div class="col-md-3 col-xs-4">
                    <p><b>السبت</b></p>
                    <p>6-8 ص</p>
                </div>
                <div class="col-md-3 col-xs-4">
                    <p><b>الاحد</b></p>
                    <p>6-8 ص</p>
                </div>
                <div class="col-md-3 col-xs-4">
                    <p><b>الاثنين</b></p>
                    <p>6-8 ص</p>
                </div>
                <div class="col-md-3 col-xs-4">
                    <p><b>الثلاثاء</b></p>
                    <p>6-8 ص</p>
                </div>
                <div class="col-md-3 col-xs-4">
                    <p><b>الاربعاء</b></p>
                    <p>6-8 ص</p>
                </div>
                <div class="col-md-3 col-xs-4">
                    <p><b>الخميس</b></p>
                    <p>6-8 ص</p>
                </div>
                <div class="col-md-3 col-xs-4">
                    <p><b>الجمعة</b></p>
                    <p>6-8 ص</p>
                </div>
            </div>
        </div>
    </div><!---end comp-view-desc -->
    <div class="col-xs-12 comp-view-desc">
            <div class="col-md-3 col-xs-6 comp-v-ti">الموقع الالكتروني:</div>
            <div class="col-md-3 col-xs-6 comp-v-val">{{$companies->compwebsite}}</div>
            <div class="col-md-3 col-xs-6 comp-v-ti">البريد:</div>
            <div class="col-md-3 col-xs-6  comp-v-val">{{$companies->compemail}}</div>
        </div><!---end comp-view-desc -->
        <div class="col-xs-12 comp-view-desc">
            <div class="col-md-3 col-xs-5 comp-v-ti">سوشيال ميديا:</div>
            <div class="col-md-3 col-xs-7 comp-v-val">
                <ul class="comp-v-soc">
                    <li>
                        <a href="https://facebook.com/{{$companies->compfacebook}}" class="add-share-fac">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://facebook.com/{{$companies->compfacebook}}" class="add-share-twit">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    
                    <li>
                        <a href="https://facebook.com/{{$companies->compfacebook}}" class="add-share-fav">
                            <i class="fa fa-youtube"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://facebook.com/{{$companies->compfacebook}}" class="add-share-what">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </li>
                    
                    <li>
                        <a href="https://facebook.com/{{$companies->compfacebook}}" class="add-share-inst">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-xs-6 comp-v-ti">هاتف:</div>
            <div class="col-md-3 col-xs-6  comp-v-val">
                <p>{{$companies->comphone1}}</p>
                <p>{{$companies->comphone2}}</p>
            </div>
        </div><!---end comp-view-desc -->
        <div class="col-xs-12 comp-view-desc">
            <div class="col-md-3 col-xs-12 comp-v-ti">الخدمات:</div>
            <div class="col-md-9 col-xs-12 comp-v-ti">
                <ul class="comp-v-serv">
                    @foreach ($companies->tags as $tag)
                        <li>
                            <a href="#">{{$tag->tagname}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div><!---end comp-view-desc -->
        <div class="col-xs-12 comp-view-desc">
            <p class="h3"><strong>العروض الحالية :</strong></p>
            <div class="row" id="comp-l-slider">
                @foreach ($homeads as $ads)
                    @if($ads->adsplace==1 && $ads->status==0 && $ads->enddate>=$date)
                        @if($ads->adstype==1)
                        <div class="ads-block">
                            <div>
                                <div class="source-links">
                                    <div class="go-link">
                                        <a href="{{$ads->adsurl}}"><i class="fa fa-link"></i></a>
                                    </div>
                                    <div class="go-view">
                                        <a href="{{url('public/assets/img/')}}/{{$ads->image}}"><i class="fa fa-search-plus"></i></a>
                                    </div>
                                </div>
                                <img src="{{url('public/assets/img')}}/{{$ads->image}}" class="img-responsive" />
                            </div>
                        </div><!---end ads-block -->
                        @elseif($ads->adstype==2)
                        <div class="ads-block">
                                <div >
                                    <!--youtube video block with iframe-->
                                <div class="source-links">
                                    <div class="go-link">
                                        <a href="#"><i class="fa fa-link"></i></a>
                                    </div>
                                    <div class="go-view">
                                        <a  data-src="https://www.youtube.com/embed/{{$ads->youtube}}" data-iframe="true"><i class="fa fa-search-plus"></i></a>
                                    </div>
                                </div>
                                <iframe width="300" height="250" src="https://www.youtube.com/embed/{{$ads->youtube}}" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div><!---end ads-block -->
                        @elseif($ads->adstype==3)
                        <div class="ads-block">
                            <div >
                                <div class="source-links">
                                    <div class="go-link">
                                        <a href="#"><i class="fa fa-link"></i></a>
                                    </div>
                                    <div class="go-view">
                                        <a href="#"><i class="fa fa-search-plus"></i></a>
                                    </div>
                                </div>
                                {!! $ads->adsense !!}
                                <!--<iframe id="google_ads_frame1" name="google_ads_frame1" width="336" height="280" frameborder="0" src="{{$ads->adsense}}" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true"></iframe>
                                -->
                            </div>
                        </div><!---end ads-block -->
                        @endif
                    @endif
                @endforeach
            </div>
        </div><!---end comp-view-desc -->
        <div class="col-xs-12 comp-view-desc ">
            <p class="h3"><strong>الكادر الوظيفي :</strong></p>
            <div class="col-xs-12" id="comp-em-slider">
                @foreach ($companies->workers as $worker)
                    <div class="com-em-item">
                        <div class="com-em-img">
                            <img src="{{url('public/assets/img')}}/{{ $worker->empphoto}}" />
                        </div>
                        <div class="com-em-info">
                            <p class="h4"><strong>{{ $worker->empname}}</strong></p>
                            <p class="h5">{{ $worker->emprole}}</p>
                        </div>
                    </div><!---end com-em-item -->
                @endforeach
            </div><!---end comp-em-slider -->
        </div><!---end comp-view-desc -->
    <div class="col-xs-12 comp-view-desc">
        <p class="h3"><strong>إعلانات الشركة :</strong></p>
        <div class="col-xs-12" id="center-ads"><!---start center-ads -->
        <p class="div-title h2 blue-border">
            احدث الاعلانات
        </p>
        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
            <div class="hovereffect featured">
                <a href="#" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>

                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                1234
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!---end feat-block -->
        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
            <div class="hovereffect">
                <a href="#" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>

                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                1234
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!---end feat-block -->
        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
            <div class="hovereffect">
                <a href="#" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>

                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                1234
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!---end feat-block -->
        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
            <div class="hovereffect featured">
                <a href="#" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>

                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                12345
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!---end feat-block -->
        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
            <div class="hovereffect">
                <a href="#" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>

                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                1234
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!---end feat-block -->
        <div class="col-md-4 col-xs-6 feat-block"><!---start feat-block -->
            <div class="hovereffect">
                <a href="#" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>

                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                1234
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div><!---end feat-block -->
    </div><!---end center-ads -->
        <div class="col-xs-12 comp-ads-more">
            <a href="#" class="cli-btn blue-btn">المزيد</a>
        </div>
    </div><!---end comp-view-desc -->
</div><!---end site-content -->

@include('includes.sidebar')
@endsection