<section id="feature-sli"><!---start feature-sli section -->
<div class="container-fluid">
    <div class="row" id="feat-slider">
        <!---start feat-block -->
        @if($featureclasscars)
        @foreach($featureclasscars as $classcars)
        <div class="feat-block">
            <div class="hovereffect featured">
                <a href="{{route('addcar.show',$classcars->id)}}" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/video')}}/{{$classcars->upcar_video}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="{{route('addcar.show',$classcars->id)}}">{{$classcars->upcar_title}}</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                <span>1234</span>
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div><!---end feat-block -->
        @endforeach
        @endif

                <!---start feat-block -->
        @if($featureclassbuilds)
        @foreach($featureclassbuilds as $classbuilds)
        <div class="feat-block">
            <div class="hovereffect featured">
                <a href="#" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                <span>1234</span>
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div><!---end feat-block -->
        @endforeach
        @endif

                <!---start feat-block -->
        @if($featureclasselectros)
        @foreach($featureclasselectros as $classelectros)
        <div class="feat-block">
            <div class="hovereffect featured">
                <a href="#" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                <span>1234</span>
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div><!---end feat-block -->
        @endforeach
        @endif

                <!---start feat-block -->
        @if($featureclassjobs)
        @foreach($featureclassjobs as $classjobs)
        <div class="feat-block">
            <div class="hovereffect featured">
                <a href="#" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="#">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                <span>1234</span>
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div><!---end feat-block -->
        @endforeach
        @endif
    </div>
</div>
<div class="adsli-btn">
    <div class="adsli-next blue-btn" id="adsli2-next">
        <i class="fa fa-angle-double-right"></i>
    </div>
    <div class="adsli-prev blue-btn" id="adsli2-prev">
        <i class="fa fa-angle-double-left"></i>
</div>
</div>
</section><!---end feature-sli section -->