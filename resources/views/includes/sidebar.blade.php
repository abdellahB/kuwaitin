<?php
$dt = new DateTime();
$date = $dt->format('m/d/Y');
?>
<!---start sidebar -->
<div class="col-md-4 col-xs-12 sidebar">
<div class="col-xs-12 side-block ">
    <a href="{{url('addads')}}" class="side-btn blue-btn btn-block text-center">
        <i class="fa fa-plus"></i>
        @lang('homepage.addAds')
    </a>
</div><!--end side-block-->
<div class="col-xs-12 side-block ">
    <a href="{{url('favorit')}}" class=" side-btn red-btn btn-block text-center">
        <i class="fa fa-heart"></i>
         @lang('homepage.favorit')
    </a>
</div><!--end side-block-->
@yield("offers")
<div class="col-xs-12 side-block">
    <p class="div-title h2 blue-border">
         @lang('homepage.offers')
    </p>
    <div  id="side-slider" class="sds">
        @foreach ($homeads as $ads)
            @if($ads->adsplace==2 && $ads->status==0 && $ads->enddate>=$date)
                @if($ads->adstype==1 && $ads->adsfeature==2 )
                    <div class="col-lg-3 col-md-4 col-xs-12 ads-block">
                        <a href="#">
                            <img src="{{url('public/assets/img/')}}/{{$ads->image}}" class="img-responsive" />
                        </a>
                    </div><!---end ads-block -->
                @elseif($ads->adstype==2 && $ads->adsfeature==2)
                    <div class="col-lg-3 col-md-4 col-xs-12 ads-block">
                        <iframe width="300" height="250" src="https://www.youtube.com/embed/{{$ads->youtube}}" frameborder="0" allowfullscreen></iframe>
                    </div><!---end ads-block -->
                @elseif($ads->adstype==3 && $ads->adsfeature==2)
                    <div class="col-lg-3 col-md-4 col-xs-12 ads-block">
                        {!! $ads->adsense !!}
                    </div><!---end ads-block -->
                @endif
            @endif
        @endforeach
    </div>
</div> <!--end side-block-->
<div class="col-xs-12 side-block ">
    <p class="div-title h2 blue-border">
        @lang('homepage.featureAds')
    </p>
    <div  id="fslider" class="sds">
        @foreach ($homeads as $ads)
            @if($ads->adsplace==2 && $ads->status==0 && $ads->enddate>=$date)
                @if($ads->adstype==1 && $ads->adsfeature==1 )
                    <div class="col-lg-3 col-md-4 col-xs-12 ads-block">
                        <a href="#">
                            <img src="{{url('public/assets/img/')}}/{{$ads->image}}" class="img-responsive" />
                        </a>
                    </div><!---end ads-block -->
                @elseif($ads->adstype==2 && $ads->adsfeature==1)
                    <div class="col-lg-3 col-md-4 col-xs-12 ads-block">
                        <iframe width="300" height="250" src="https://www.youtube.com/embed/{{$ads->youtube}}" frameborder="0" allowfullscreen></iframe>
                    </div><!---end ads-block -->
                @elseif($ads->adstype==3 && $ads->adsfeature==1)
                    <div class="col-lg-3 col-md-4 col-xs-12 ads-block">
                        {!! $ads->adsense !!}
                    </div><!---end ads-block -->
                @endif
            @endif
        @endforeach
    </div>
</div> <!--end side-block-->
<div class="col-xs-12 side-block info">
    <a href="mailto: {{$setup->site_email}}" class="side-btn green-btn btn-block text-center">
                <i class="fa fa-envelope"></i>
               {{$setup->site_email}}
    </a>
    <a href="tel: {{$setup->site_phone}}" class="side-btn green-btn btn-block text-center">
                <i class="fa fa-phone"></i>
                {{$setup->site_phone}}
    </a>
</div><!--end side-block-->
<div class="col-xs-12 side-block info">
    <a href="{{$setup->site_facebook}}" class="">
        <img src="{{url('public/assets/img/facebook.jpg')}}" class="img-responsive" alt="" />
                
    </a>
</div><!--end side-block-->
<div class="col-xs-12 side-block info">
    <a href="{{$setup->site_twitter}}" class="">
        <img src="{{url('public/assets/img/twitter.jpg')}}" class="img-responsive" alt="" />
                
    </a>
</div><!--end side-block-->
<div class="col-xs-12 side-block info">
    <a href="{{$setup->site_instagram}}" class="">
        <img src="{{url('public/assets/img/instagram.jpg')}}" class="img-responsive" alt="" />
                
    </a>
</div><!--end side-block-->
<div class="col-xs-12 side-block ">
    <p class="div-title h2 blue-border">
         @lang('homepage.featurecompany')
    </p>
    <div  id="coslider" class="sds">
        @foreach ($homeads as $ads)
            @if($ads->adsplace==4 && $ads->status==0 && $ads->enddate>=$date)
                @if($ads->adstype==1)
                    <div class="col-lg-3 col-md-4 col-xs-12 ads-block">
                        <a href="#">
                            <img src="{{url('public/assets/img/')}}/{{$ads->image}}" class="img-responsive" />
                        </a>
                    </div><!---end ads-block -->
                @elseif($ads->adstype==2)
                    <div class="col-lg-3 col-md-4 col-xs-12 ads-block">
                        <iframe width="300" height="250" src="https://www.youtube.com/embed/{{$ads->youtube}}" frameborder="0" allowfullscreen></iframe>
                    </div><!---end ads-block -->
                @elseif($ads->adstype==3)
                    <div class="col-lg-3 col-md-4 col-xs-12 ads-block">
                        {!! $ads->adsense !!}
                    </div><!---end ads-block -->
                @endif
            @endif
        @endforeach
    </div>
</div> <!--end side-block-->
</div><!---end sidebar -->