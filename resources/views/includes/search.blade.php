<section id="search-bar"><!---start search-bar section -->
            <div class="container">
                <div class="row">
                    <form action="" method="" class="col-xs-12">
                        <div class="col-lg-2 col-md-6 col-xs-6">
                            <input type="text" placeholder="ابحث عن" name="" class="form-control" />
                        </div>
                        <div class="col-lg-2 col-md-6 col-xs-6">
                            <select name="" class="ui fluid normal dropdown">
                                <option value="1">قسم</option>
                                <option value="2">قسم</option>
                                <option value="3">قسم</option>
                                <option value="4">قسم</option>
                                <option value="5">قسم</option>
                                <option value="6">قسم</option>
                                <option value="7">قسم</option>
                                <option value="8">قسم</option>
                                <option value="9">قسم</option>
                                <option value="10">قسم</option>
                                <option value="11">قسم</option>
                                <option value="12">قسم</option>
                                <option value="13">قسم</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-6 col-xs-6">
                            <select name="" class="ui fluid normal dropdown">
                                <option value="1">المكان</option>
                                <option value="2">المكان</option>
                                <option value="3">المكان</option>
                                <option value="4">المكان</option>
                                <option value="5">المكان</option>
                                <option value="6">المكان</option>
                                <option value="7">المكان</option>
                                <option value="8">المكان</option>
                                <option value="9">المكان</option>
                                <option value="10">المكان</option>
                                <option value="11">المكان</option>
                                <option value="12">المكان</option>
                                <option value="13">المكان</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-6 col-xs-6">
                            <select name="" class="ui fluid normal dropdown">
                                <option value="1">اخر اسبوع</option>
                                <option value="1">اخر يوم</option>
                                <option value="2">اخر شهر</option>
                                <option value="3">اخر سنة</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-6 col-xs-6">
                            <input type="text" placeholder="السعر الادنى" name="" class="form-control" />
                        </div>
                        <div class="col-lg-2 col-md-6 col-xs-6">
                            <input type="text" placeholder="السعر الاعلى" name="" class="form-control" />
                        </div>
                        <div class="col-lg-12 col-md-12 col-xs-12 search-btn">
                            <button type="submit"  name="" class="red-btn" >
                                <i class="fa fa-search"></i>
                                <span> @lang('homepage.search')</span>
                            </button>
                        </div>
                    </form><!---end form -->
                </div>
            </div>
        </section><!---end search-bar section -->