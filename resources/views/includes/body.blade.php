<?php
$dt = new DateTime();
$date = $dt->format('m/d/Y');
?>
<div class="col-md-8 col-xs-12 site-content"><!---start site-content-->
<div class="col-xs-12" id="main-servise"><!---start main-servise-->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block red-gr"><!---start serv-block -->
        <a href="{{url('offers')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic17-01.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">عروض الكويت</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block green-gr"><!---start serv-block -->
        <a href="{{url('offers')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic1-02.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">مواقع هامة</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block blue-gr"><!---start serv-block -->
        <a href="{{url('company')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic2-02.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">دليل الشركات</p>
            </div>
        </a>
    </div><!---end serv-block -->
    
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block red-gr"><!---start serv-block -->
        <a href="{{url('offers')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic3-02.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">دليل انستغرام</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block blue-gr"><!---start serv-block -->
        <a href="{{url('offers')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic18-01.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">اخبار الكويت</p>
            </div>
        </a>
    </div><!---end serv-block -->
    
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block purple-gr"><!---start serv-block -->
        <a href="{{url('companytype')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic4-02.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">خدمات التعليم</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block pink-gr"><!---start serv-block -->
        <a href="{{url('addcar')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic5-02.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">حراج السيارات</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block green-gr"><!---start serv-block -->
        <a href="{{url('addbuild')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic6-01.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">عقارات الكويت</p>
            </div>
        </a>
    </div><!---end serv-block -->
        <div class="col-md-3 col-sm-4 col-xs-4 serv-block yellow-gr"><!---start serv-block -->
            <a href="{{url('car4rent')}}" class="serv-cont">
                <div class="serv-icon">
                    <img src="{{url('public/assets/img/ic7-01.png')}}"  alt="" >
                </div>
                <div class="serv-title">
                    <p class="h4">تأجير سيارات</p>
                </div>
            </a>
        </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block red-gr"><!---start serv-block -->
        <a href="{{url('companytype')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic8-01.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">سفريات وفنادق</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block blue-gr"><!---start serv-block -->
        <a href="{{url('companytype')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic9-01.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">عيادات وتجميل</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block purple-gr"><!---start serv-block -->
        <a href="{{url('companytype')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic10-01.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">مطاعم الكويت</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block pink-gr"><!---start serv-block -->
        <a href="{{url('addelectronic')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic11-01.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">الكترونيات</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block green-gr"><!---start serv-block -->
        <a href="{{url('addjobs')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic12-01.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">وظائف الكويت</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block red-gr"><!---start serv-block -->
        <a href="{{url('addother')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic13-01.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">أعمال حرة</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block green-gr"><!---start serv-block -->
        <a href="{{url('house')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic14-01.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">خدمات منزلية</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block yellow-gr"><!---start serv-block -->
        <a href="{{url('scrap')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic15-01.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">خدمات سكراب</p>
            </div>
        </a>
    </div><!---end serv-block -->
    <div class="col-md-3 col-sm-4 col-xs-4 serv-block pink-gr"><!---start serv-block -->
        <a href="{{url('others')}}" class="serv-cont">
            <div class="serv-icon">
                <img src="{{url('public/assets/img/ic16-01.png')}}"  alt="" >
            </div>
            <div class="serv-title">
                <p class="h4">اخرى</p>
            </div>
        </a>
    </div><!---end serv-block -->
</div><!---end main-servise-->
<div class="col-xs-12" id="center-ads"><!---start center-ads -->
    <p class="div-title h2 blue-border">
         @lang('homepage.latestAds')
    </p>
    <!---start feat-block -->
    @if($classcars)
    @foreach($classcars as $cars)
        <div class="col-md-4 col-xs-6 feat-block">
            <div class="hovereffect @if($cars->statys==2) featured @endif">
                <a href="{{route('addcar.show',$cars->id)}}" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/video')}}/{{$cars->upcar_video}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="{{route('addcar.show',$cars->id)}}"> 
                            {{$cars->upcar_title}}
                            </a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                1234
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        @endif
        <!--end feat-block -->
        @if($classbuilds)
        @foreach($classbuilds as $build)
        <div class="col-md-4 col-xs-6 feat-block">
            <div class="hovereffect featured">
                <a href="{{route('addbuild.show',$build->id)}}" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="{{route('addbuild.show',$build->id)}}">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                1234
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        @endif

        <!--ads n 3-->
        @if($classelectros)
        @foreach($classelectros as $electro)
        <div class="col-md-4 col-xs-6 feat-block">
            <div class="hovereffect featured">
                <a href="{{route('addelectronic.show',$electro->id)}}" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="{{route('addelectronic.show',$electro->id)}}">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                1234
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        @endif

        <!--class jobs-->
        @if($classjobs)
        @foreach($classjobs as $jobs)
        <div class="col-md-4 col-xs-6 feat-block">
            <div class="hovereffect featured">
                <a href="{{route('addjobs.show',$jobs->id)}}" class="feat-img">
                    <img class="img-responsive" src="{{url('public/assets/img/300%20SQR.jpg')}}" alt="">
                </a>
                    <div class="overlay col-xs-12">
                        <div class="col-xs-12 feat-title">
                            <a href="{{route('addjobs.show',$jobs->id)}}">العنوان يكتب هنا العنوان يكتب هنا العنوان يكتب هنا</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#" data-tooltip="2 مشاهدة" data-position="top center">
                                1234
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                        <div class="col-xs-3" >
                            <a href="#" data-tooltip="اضافة للمفضلة" data-position="top center">
                                <i class="fa fa-heart"></i>
                            </a>
                        </div>
                        <div class="col-xs-3">
                            <div class="ui share">
                                <i class="fa fa-share-alt"></i>
                            </div>
                            <div class="ui share-menu flowing popup top center transition hidden">
                                <div class="share-list">
                                    <div class="share-item">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                    </div>
                                    <div  class="share-item">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        @endif
</div><!---end center-ads -->
<div class="col-xs-12 bottom-ads">
    @foreach ($homeads as $ads)
        @if($ads->adsplace==3 && $ads->status==0 && $ads->enddate>=$date)
            @if($ads->adstype==1)
                <div class="col-md-6 col-xs-12 bottom-ad">
                    <a href="#" class="btn-block">
                        <img src="{{url('public/assets/img/')}}/{{$ads->image}}" class="img-responsive" alt="" />
                    </a>
                </div><!---end bottom-ad -->
            @elseif($ads->adstype==3)
                <div class="col-md-6 col-xs-12 bottom-ad">
                      {!! $ads->adsense !!}
                </div><!---end bottom-ad -->
            @endif
        @endif
    @endforeach
</div><!---end bottom-ads -->
</div><!---end site-content -->