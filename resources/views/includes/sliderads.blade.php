<?php
$dt = new DateTime();
$date = $dt->format('m/d/Y');
?>
<section id="ads-sli"><!---start ads-sli section -->
<div class="container-fluid">
    <div class="row" id="light-slider">
        @foreach ($homeads as $ads)
        @if($ads->adsplace==1 && $ads->status==0 && $ads->enddate>=$date)
            @if($ads->adstype==1)
            <div class="ads-block">
                <div>
                    <div class="source-links">
                        <div class="go-link">
                            <a href="{{$ads->adsurl}}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="go-view">
                            <a href="{{url('public/assets/img/')}}/{{$ads->image}}"><i class="fa fa-search-plus"></i></a>
                        </div>
                    </div>
                    <img src="{{url('public/assets/img')}}/{{$ads->image}}" class="img-responsive" />
                </div>
            </div><!---end ads-block -->
            @elseif($ads->adstype==2)
            <div class="ads-block">
                    <div >
                        <!--youtube video block with iframe-->
                    <div class="source-links">
                        <div class="go-link">
                            <a href="#"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="go-view">
                            <a  data-src="https://www.youtube.com/embed/{{$ads->youtube}}" data-iframe="true"><i class="fa fa-search-plus"></i></a>
                        </div>
                    </div>
                    <iframe width="300" height="250" src="https://www.youtube.com/embed/{{$ads->youtube}}" frameborder="0" allowfullscreen></iframe>
                </div>
            </div><!---end ads-block -->
            @elseif($ads->adstype==3)
            <div class="ads-block">
                <div >
                    <div class="source-links">
                        <div class="go-link">
                            <a href="#"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="go-view">
                            <a href="#"><i class="fa fa-search-plus"></i></a>
                        </div>
                    </div>
                    {!! $ads->adsense !!}
                    <!--<iframe id="google_ads_frame1" name="google_ads_frame1" width="336" height="280" frameborder="0" src="{{$ads->adsense}}" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true"></iframe>
                    -->
                </div>
            </div><!---end ads-block -->
            @endif
        @endif
        @endforeach
    </div>
</div>
    <div class="adsli-btn">
        <div class="adsli-next red-btn" id="adsli-next">
            <i class="fa fa-angle-double-right"></i>
        </div>
        <div class="adsli-prev red-btn" id="adsli-prev">
            <i class="fa fa-angle-double-left"></i>
    </div>
</div>
</section><!---end ads-slisection -->
