<?php

return [

    'addAds' => "أضف اعلانك ",
    'login' => "دخول ",
    'register' => "تسجيل  ",
    'profile' => "اعدادت الحساب ",
    'employer' => "اضافة موظف ",
    'myads' => "اعلاناتي ",
    'logout' => "تسديل الخروج",
    'home' => "الرئسية ",
    'terms' => "الشروط والاحكام ",
    'privacy' => "سياسة الخصوصية ",
    'about' => "من نحن ",
    'contact' => "تواصل معنا ",
    'top' => "الاعلى ",
    'followus' => " تابعنا على ",
    'rights' => "  جميع الحقوق محفوظة لـ ",

    'favorit' => " مفضلاتي ",
    'offers' => "  احدث الاعلانات ",
    'featurecompany' => " شركات مميزة ",
    'featureAds' => " اعلانات مميزة",
    'search' => " البحث  ",
    'latestAds' => "أحدث الاعلانات",
    
];
