$(document).ready(function(){

		
//select ads types
	$(".adstype").on("change",function(){ 
		var idds = $(this).val();
		if(idds==1){
			$('.adsense').hide();
			$('.youtube').hide();
			$('.image').show();
		}else if(idds==2){
			$('.adsense').hide();
			$('.image').hide();
			$('.youtube').show();
		}else if(idds==3){
			$('.image').hide();
			$('.youtube').hide();
			$('.adsense').show();
		}
		
	}); 	

	if($(".adstype").val()==1){
		$('.adsense').hide();
		$('.youtube').hide();
		$('.image').show();
	}else if($(".adstype").val()==2){
		$('.adsense').hide();
		$('.image').hide();
		$('.youtube').show();
	}else if($(".adstype").val()==3){
		$('.image').hide();
		$('.youtube').hide();
		$('.adsense').show();
	}

	//select ads place
	$(".adsplace").on("change",function(){ 
		var adsplace = $(this).val();
		if(adsplace==2){
			$('.adsfeature').show();
		}else{
			$('.adsfeature').hide();
		}
		
	}); 	

	if($(".adsplace").val()==2){
		$('.adsfeature').show();
	}else{
		$('.adsfeature').hide();
	}

	//mode
	$('.isAgeSelected').on('click',function(){
		var mode = $(this).is(':checked');
		if(mode){
			$('.catphoto').show();
			$('.catflag').hide();
		}else{
			$('.catphoto').hide();
			$('.catflag').show();
		}
	});


	//disable account admins
	$(".table").on("click",".removeadmins",function(){ 
		var idadmin=$(this).attr("id");
		var admins ="removeadmins";
		var data ="idadmin="+idadmin+"&admins="+admins;
		////alert(data);
			swal({
			title: "",
			text: "هل تريد حقا ايقاف هدا الحساب ؟",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "ايقاف الحساب",
			closeOnConfirm: false
			},
		function(){
			$.ajax({
				url: 'status',
				type: "post",
				data: data,
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				success: function(response){ // What to do if we succeed
					if(response.status == "success"){
						location.reload();
					}
				}
			});
		});		
	});	
	//activate account of admns
	$(".table").on("click",".activeadmins",function(){ 
		var idadmin=$(this).attr("id");
		var admins ="activeadmins";
		var data ="idadmin="+idadmin+"&admins="+admins;
		////alert(data);
			swal({
			title: "",
			text: "هل تريد حقا تفعيل هدا الحساب ؟",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "تفعيل الحساب",
			closeOnConfirm: false
			},
		function(){
			$.ajax({
				url: 'status',
				type: "post",
				data: data,
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				success: function(response){ // What to do if we succeed
					if(response.status == "success"){
						location.reload();
					}
				}
			});
		});		
	});	


	//class cars 
	$(".table").on("click",".publierads",function(){ 
		var idads  = $(this).attr("id");
		var status = $(this).attr("title");
		swal({
			title: "هل أنت متـأكد ؟",
			text: "تريد القيام بهده العملية لهدا الاعلان ...",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "الموافقة",
			closeOnConfirm: false
			},
		function(){ 
			$.ajax({
				url: 'getAjax',
				method: "post",
				data: {idads,status},
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				success: function(response){ // What to do if we succeed
					if(response.status == "success"){
						location.reload();
					}
				}
			});
		});		
	});	

	
	//
	$(".upcargov").on("change",function(){ 
		var upcargov = $(this).val();
        $.get( 'create' , { "upcargov" : upcargov } , function(htmlCode){
            $(".domains_table").html(htmlCode);
        });
    });	

});