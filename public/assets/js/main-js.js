$(document).ready(function() {


    $('.ui.dropdown').dropdown();
        var lightslider = $("#light-slider").lightSlider({
            item: 4,
            rtl:true ,
            loop: true,
            slideMargin:5,
            pager: false,
            controls: false, 
            auto: true,
            pause: 3000,
            autoWidth:true,
            pauseOnHover:true,
            responsive : [
                 {
                breakpoint:991,
                settings: {
                    item: 3,
                    slideMove:1,
                    slideMargin:0,
                  }
            },
            {
                breakpoint:767,
                settings: {
                    item: 1,
                    slideMove:1,
                    slideMargin:2,
                    autoWidth:true
                  }
            }
                 
             ]
        });
    $('#adsli-prev').click(function(){
        lightslider.goToPrevSlide();    
    });
    $('#adsli-next').click(function(){
        lightslider.goToNextSlide();    
    });
    var featslider = $("#feat-slider").lightSlider({
            item: 4,
            rtl:true ,
        autoWidth:false,
            loop: true,
            slideMargin:0,
            pager: false,
            controls: false, 
            auto: true,
            pause: 3000,
            pauseOnHover:true,
             responsive : [
                 {
                breakpoint:991,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:0,
                    
                  }
            },
            {
                breakpoint:767,
                settings: {
                    item:2,
                    slideMove:1,
                    slideMargin:0,
                    autoWidth:false
                  }
            }
                 
             ]
        });
$('#adsli2-prev').click(function(){
        featslider.goToPrevSlide();    
    });
    $('#adsli2-next').click(function(){
        featslider.goToNextSlide();    
    });
    
    var sds = $(".sds").lightSlider({
            item: 1,
            rtl:true ,
            loop: true,
            slideMargin:0,
            pager: false,
            auto: true,
            pause: 3000,
            pauseOnHover:true,
        });

        $("#light-slider").lightGallery({
            selector:".go-view a" ,
            videojs: true
        }); 
        $(".offers").lightGallery({
            selector:".ad-view" ,
            videojs: true
        }); 
    $(".share").popup({
        inline     : false,
        hoverable  : true,
        position   : 'top right',
      });

    $(".top").smoothScroll();
    var viewads = $("#ad-view-gallery .view-img").lightSlider({
            gallery:true,
            item:1,
            loop:true,
            thumbItem:8,
            slideMargin:0,
            enableDrag: true,
            adaptiveHeight: true,
            rtl: true,
            currentPagerPosition:'right',
            onSliderLoad: function(el) {
                el.lightGallery({
                    selector: '#ad-view-gallery .view-img .ad-gallery-item'
                });
            }   
        });

    var sameslider = $(".same-slider").lightSlider({
            item: 5,
            rtl:true ,
        autoWidth:false,
            loop: true,
            slideMargin:2,
            pager: true,
            controls: true, 
            auto: true,
            pause: 3000,
            pauseOnHover:true,
             responsive : [
                 {
                breakpoint:991,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:2,
                    
                  }
            },
            {
                breakpoint:767,
                settings: {
                    item:2,
                    slideMove:1,
                    slideMargin:2,
                    autoWidth:false
                  }
            }
                 
             ]
        });


});
//compare slider
 var compare = $(".compare").lightSlider({
            item: 3,
            rtl:true ,
            autoWidth:false,
            loop: true,
            slideMargin:0,
            pager: true,
            auto: false,
            pauseOnHover:false,
            enableDrag: true,
            enableTouch: true,
             responsive : [
                 {
                breakpoint:991,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:0,
                    controls: false
                    
                  }
            },
            {
                breakpoint:767,
                settings: {
                    item:2,
                    slideMove:1,
                    slideMargin:0,
                    autoWidth:false,
                    controls: true
                  }
            }
                 
             ]
        });