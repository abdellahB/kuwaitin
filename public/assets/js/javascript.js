$(document).ready(function(){

    $(".supcategory").on("change",function(){ 
		var idds = $(this).val();
		if(idds==1){
			$('.scrapcategory').show();

            $('.housecategory').hide();
            $('.lancecategory').hide();
            $('.otherscategory').hide();

		}else if(idds==2){

			$('.housecategory').show();

            $('.scrapcategory').hide();
            $('.lancecategory').hide();
            $('.otherscategory').hide();

		}else if(idds==3){

			$('.lancecategory').show();

            $('.scrapcategory').hide();
            $('.housecategory').hide();
            $('.otherscategory').hide();

		}else if(idds==4){

			$('.otherscategory').show();

            $('.scrapcategory').hide();
            $('.lancecategory').hide();
            $('.housecategory').hide();
		}
	}); 	

    
    //get all city
    $(".upcargov").on("change",function(){ 
		var upcargov = $(this).val();
		var data ="upcargov="+upcargov;
        $.get( 'create' , { "upcargov" : upcargov } , function(htmlCode){
            $(".domains_table").html(htmlCode);
        });
    });	
    
    $('#myForm input[name=optradio]').on('change', function() {
        var value = $('input[name=optradio]:checked', '#myForm').val(); 
        if(value == 1){
            $("#data-cars").show();
            $("#data-rentcar").hide();
        }else{
            $("#data-cars").hide();
            $("#data-rentcar").show();
        }
    });



    $('.text-data').pickadate({
        format: 'd/mm/yyyy',
        monthsFull: ['يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو', 'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر'],
        monthsShort: ['يناير', 'فبراير', 'مارس', 'ابريل', 'مايو', 'يونيو', 'يوليو', 'اغسطس', 'سبتمبر', 'اكتوبر', 'نوفمبر', 'ديسمبر'],
        weekdaysFull: ['الاحد', 'الاتنين', 'الثلاثاء', 'الاربعاء', 'الخميس', 'الجمعة', 'السبت'],
        weekdaysShort: ['الاحد', 'الاتنين', 'الثلاثاء', 'الاربعاء', 'الخميس', 'الجمعة', 'السبت'],
        today: 'اليوم',
        clear: 'افراغ',
        close: 'اغلاق',
    });

    $('.ui.rating').rating('disable');
    $(".compslider").lightSlider({
        item: 4,
        rtl:true ,
        loop: true,
        slideMargin:5,
        pager: false,
        controls: true,
        auto: true,
        pause: 3000,
        autoWidth:true,
        pauseOnHover:true,
        responsive : [
             {
            breakpoint:991,
            settings: {
                item: 3,
                slideMove:1,
                slideMargin:0,
              }
        },
        {
            breakpoint:767,
            settings: {
                item: 1,
                slideMove:1,
                slideMargin:2,
                autoWidth:true
              }
        }

         ]
    });

    $(".compslider").lightGallery({
        selector:".go-view a" ,
         videojs: true
    });
});