<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],
function(){

	/** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
  Auth::routes();
  Route::get('/logout','Auth\LoginController@userLogout')->name('userLogout');
	Route::get('/', 'HomeController@index')->name('home');
  Route::get('/addads', 'AdsController@index')->name('ads');

  Route::resource('/addcar', 'HomeCarscontroller');
  Route::resource('/addbuild', 'HomeBuildController');
  Route::resource('/addelectronic','HomeElectroController');
  Route::resource('/addjobs', 'homeJobsController');
  Route::resource('/addother', 'HomeOtherController');

  Route::get('/scrap', 'HomeOtherController@scrap');
  Route::get('/house', 'HomeOtherController@house');
  Route::get('/others', 'HomeOtherController@other');

  
  //profile
  Route::get('/profile', 'AdsController@profile')->name('profile');
	Route::post('/profile','AdsController@updateprofile')->name('updateprofile');
  Route::get('/myads', 'AdsController@myads')->name('myads');

 //profile company ...
  Route::get('/employer', 'CompanyController@employer')->name('add_employer');
  Route::get('/compProfile', 'CompanyController@compprofile')->name('compprofile');
  Route::get('/editemployer', 'CompanyController@editemployer')->name('editemployer');
  //pages
  Route::get('/offers', 'HomeController@offers')->name('offers');
  Route::get('/contact','HomeController@contact');
  Route::get('/terms','HomeController@terms');
  Route::get('/privacy','HomeController@privacy');
  Route::get('/about','HomeController@about');
  Route::get('/company','HomeController@company');
  Route::get('/companytype/{cat}','HomeController@companytype');
  Route::get('/companyprofile/{slug}','HomeController@companyprofile');
});

//route admin
Route::prefix('control')->group(function() {

  Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('control.auth.login');
  Route::post('/login', 'Auth\AdminLoginController@login')->name('control.auth.login.submit');
  Route::get('/logout', 'Auth\AdminLoginController@logout')->name('control.auth.logout');
  Route::get('/', 'AdminController@index')->name('control.index');

  //register 
  Route::resource('/role', 'AdminRegisterController');
  Route::post('/status','AdminRegisterController@status');

  // Password reset routes
  Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('control.auth.password.email');
  Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('control.auth.password.request');
  Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
  Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('control.auth.password.reset');

  Route::get('/page/privacy', 'AdminPageController@showprivacy')->name('control.page.showprivacy');
  Route::post('/page/privacy', 'AdminPageController@saveprivacy')->name('control.page.saveprivacy');

  Route::get('/page/terms', 'AdminPageController@showterms')->name('control.page.showterms');
  Route::post('/page/terms', 'AdminPageController@saveterms')->name('control.page.saveterms');

  Route::get('/page/about', 'AdminPageController@showabout')->name('control.page.showabout');
  Route::post('/page/about', 'AdminPageController@saveabout')->name('control.page.saveabout');

  Route::get('/city', 'AdminPageController@showcity')->name('control.city.showcity');
  Route::post('/city', 'AdminPageController@savecity')->name('control.city.savecity');

  Route::get('/settings', 'AdminPageController@showsettings')->name('control.settings.showsettings');
  Route::post('/settings', 'AdminPageController@savesettings')->name('control.settings.savesettings');

  //ads
  Route::get('/adsStep/new', 'AdminAdsCarsController@newadsCars');
  Route::get('/adsStep/published', 'AdminAdsCarsController@publishedCars');
  Route::get('/adsStep/feature', 'AdminAdsCarsController@featureCars');
  Route::get('/adsStep/trashed', 'AdminAdsCarsController@trashedCars');

  Route::get('/adsStep/trashed', 'AdminAdsCarsController@trashedCars');
  
  //menu
  Route::resource('/adsStep', 'AdminAdsCarsController');
  Route::post('/adsStep/getAjax','AdminAdsCarsController@getAjax');

  //Route::get('/wmenuindex', 'AdminMenucontroller@wmenuindex');
  //route resource
  Route::resource('/category/cars', 'AdminCarsController');
  Route::resource('/category/builds', 'AdminBuildController');
  Route::resource('/category/electro', 'AdminElectroController');
  Route::resource('/category/jobs', 'AdminJobsController');
  Route::resource('/category/others', 'AdminOthersController');

  Route::resource('/category/lance', 'AdminLanceController');
  Route::resource('/category/scrap', 'AdminScrapController');
  Route::resource('/category/runt', 'AdminRuntController');
  Route::resource('/category/homes', 'AdminHomesController');

  Route::resource('/ads', 'AdminAdscontroller');
  Route::resource('/offer', 'AdminOffersController');
  Route::resource('/client', 'AdminClientController');
  Route::resource('/cat', 'AdminCatController');
  Route::resource('/company', 'AdminCompanyController');
  Route::resource('/tags', 'AdminTagcontroller');
  Route::resource('/employer', 'AdminEmployerController');

});
